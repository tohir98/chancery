<fieldset ng-if="personnel_type == 1">
    <legend>
        Health Info
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 20%">Genotype</td>
            <td style="width: 30%">
                <select name="genotype_id" id="genotype_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($genotypes)):
                        foreach ($genotypes as $type):
                            ?>
                            <option value="<?= $type->genotype_id ?>"><?= trim($type->genotype) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td style="width: 20%">Blood Group</td>
            <td>
                <select name="blood_group_id" id="blood_group_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($blood_groups)):
                        foreach ($blood_groups as $group):
                            ?>
                            <option value="<?= $group->blood_group_id ?>"><?= trim($group->blood_group) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Health History</td>
            <td>
                <textarea name="health_history" id="health_history" rows="4" style="width: 80%"></textarea>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</fieldset>