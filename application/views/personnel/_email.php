<div class="modal" id="email-modal">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Compose New Message</h4>
            </div>
            <form class="form-horizontal" method="post" action="<?= site_url('/personnel/sendEmail'); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <input required name="email_subject" id="email_subject" class="form-control input-xlarge" placeholder="Subject:"/>
                    </div>
                    <div class="form-group">
                        <textarea name="email_message" id="email_message" class="form-control" style="height: 300px; width: 80%">
                      
                        </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_ids" value="{{buildIdList(selectedPersonnels, 'personnel_id', ',')}}" />
                    <button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat">Send Email</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>