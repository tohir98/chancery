<fieldset ng-if="personnel_type != 2 && personnel_type > 0">
    <legend>
        Educational Background
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="ed in educations">
        <tr>
            <td style="width: 15%">Education Type</td>
            <td style="width: 35%">
                <select name="education[{{$index}}][education_type_id]"  class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($education_types)):
                        foreach ($education_types as $type):
                            ?>
                            <option value="<?= $type->education_type_id ?>"><?= ucfirst(trim($type->education_type)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td style="width: 20%">Institution Name</td>
            <td><input required type="text" class="form-control" id="further_institution" name="education[{{$index}}][institution]"></td>
        </tr>
        <tr>
            <td>Period Year</td>
            <td>
                From: 
                <input type="text" class="" name="education[{{$index}}][from]" id="education[{{$index}}][from]" placeholder="2001" >
               
                To 
                <input type="text" class="" name="education[{{$index}}][to]" id="education[{{$index}}][to]" placeholder="2005" >
            </td>
            <td>Degree</td>
            <td><input required type="text" class="form-control" id="degree_further" name="education[{{$index}}][degree]"></td>
        </tr>
        <tr>
            <td colspan="4">
                <a href="#" onclick="return false;" ng-click="removeEducation($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
    <a href="#" onclick="return false;" ng-click="addNewEducation()" class="btn btn-success btn-sm btn-flat pull-right">
        <i class="fa fa-plus-circle"></i> Add Further Education
    </a>
</fieldset>