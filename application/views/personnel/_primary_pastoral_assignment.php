<fieldset ng-if="personnel_type == 1">
    <legend>
        Pastoral Assignments Till Date
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="ed in passignments">
        <tr>
            <td style="width: 15%">Name of Assignment</td>
            <td style="width: 35%">
                <input required type="text" class="form-control" id="nameof_assignment_primary" name="primary[{{$index}}][nameof_assignment]" value="">
            </td>
            <td style="width: 20%">Position</td>
            <td>
                <select required name="primary[{{$index}}][position_primary]" id="position_primary" class="form-control">
                    <option value="">Select position</option>
                    <?php
                    if (!empty($pastoral_positions)):
                        foreach ($pastoral_positions as $position):
                            ?>
                            <option value="<?= $position->pastoral_position_id ?>"><?= ucfirst(trim($position->pastoral_position)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Period Year</td>
            <td>
                From: <input required type="text" placeholder="Jan. 2016" name="primary[{{$index}}][from]" > To <input type="text" name="primary[{{$index}}][to]" placeholder="Feb. 2017"  ></td>
            <td style="width: 20%">&nbsp;</td>
            <td>
                <a href="#" onclick="return false;" ng-click="removePrimaryAssignment($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
    <a href="#" onclick="return false;" ng-click="addNewPrimaryAssignment()" class="btn btn-success btn-sm btn-flat pull-right">
        <i class="fa fa-plus-circle"></i> Add Pastoral Assignment
    </a>
</fieldset>