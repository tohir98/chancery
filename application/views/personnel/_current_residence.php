<fieldset ng-if="personnel_type > 0">
    <legend>Current Residence Information</legend>
    <table class="table table-bordered table-condensed table-striped">
        <tr>
            <td style="width: 20%">Place of Residence</td>
            <td>
                <input required type="text" class="form-control" id="residence" name="residence">
            </td>
            <td style="width: 20%">Address</td>
            <td>
                <input required type="text" class="form-control" id="residence_address" name="residence_address" value="">
            </td>
        </tr>
        <tr>
            <td>City</td>
            <td>
                <input required type="text" class="form-control" id="residence_city" name="residence_city" value="">
            </td>
            <td>State</td>
            <td>
                <select required class="form-control" id="residence_state_id" name="residence_state_id">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        foreach ($states as $state):
                            ?>
                            <option value="<?= $state->state_id ?>" ><?= ucfirst(trim($state->state)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr ng-if="personnel_type != 3">
            <td>Region</td>
            <td>
                <select required class="form-control" id="region_id" name="region_id"  ng-model="personal.region_id" ng-change="loadDeanary()">
                    <option value="">Select Region</option>
                    <?php
                    if (!empty($regions)):
                        foreach ($regions as $region):
                            ?>
                            <option value="<?= $region->region_id ?>" ><?= ucfirst(trim($region->region)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>Deanery</td>
            <td>
                <div ng-if="personal.deanaryStatus !== ''">
                    {{personal.deanaryStatus}}
                </div>
                <div ng-if="personal.deanaryStatus === ''">
                    <select name="deanery_id" id="deanery_id" class="form-control">
                        <option ng-repeat="lg in personal.deanarys" value="{{lg.id}}">
                            {{lg.name}}
                        </option>
                    </select>

                </div>
            </td>
        </tr>

    </table>
</fieldset>