<fieldset ng-if="personnel_type == 1">
    <legend>
        Current Assignment
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 15%">Name of Current Assignment</td>
            <td style="width: 35%">
                <input required type="text" class="form-control" id="nameof_assignment_primary" name="current_assignment" value="">
            </td>
            <td style="width: 20%">Position</td>
            <td>
                <select required name="current_position_id" id="position_primary" class="form-control">
                    <option value="">Select position</option>
                    <?php
                    if (!empty($current_positions)):
                        foreach ($current_positions as $position):
                            ?>
                            <option value="<?= $position->current_position_id ?>"><?= ucfirst(trim($position->position)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
    </table>
</fieldset>