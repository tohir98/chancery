<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script src="/js/staff-directory.js" type="text/javascript"></script>

<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Profile</li>
    </ol>
</section>

<section class="content" ng-app="personnel" ng-controller="profilesCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <form method="get">
                    Status:
                    <select id="status" class="form-control input-medium select2" ng-model="status" >
                        <option value="0">All Status</option>
                        <?php
                        if (!empty($personnel_statuses)):
                            $sell = '';
                            foreach ($personnel_statuses as $value => $statuss):
                                if ($status == $value):
                                    echo $sell = 'selected';
                                else:
                                    $sell = '';
                                endif;
                                ?>
                                <option value="<?= $value; ?>" <?= $sell; ?>><?= $statuss; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>

                    </select>
                    &nbsp;
                    Type: <select class="form-control input-medium select2" id="personnel_type_id" ng-model="personnel_type_id">
                        <option value="0">All</option>
                        <?php
                        if (!empty($personnel_types)):
                            $sel = '';
                            foreach ($personnel_types as $value => $type):
                                ?>
                                <option value="<?= $value ?>"><?= $type; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    &nbsp;
                    Current Assignment: <select class="form-control input-medium select2" id="current_assignment" ng-model="current_assignment">
                        <option value="">All</option>
                        <?php
                        if (!empty($current_assignments)):
                            foreach ($current_assignments as $assignment):
                                ?>
                                <option value="<?= $assignment ?>"><?= $assignment; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    &nbsp;
                    <button type="button" class="btn btn-primary btn-sm btn-flat" ng-click="filterPersonnel()"><i class="fa fa-search"></i> Filter</button>
                </form>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                        Bulk Action <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#" onclick="return false;" ng-click="bulkAction('', '#sms-status')" class="send_message">Send SMS</a></li>
                                        <li><a href="#" onclick="return false;" ng-click="bulkActionEmail('')" class="send_message">Send Email</a></li>
                                    </ul>
                                </div>
                                <a class="btn btn-success btn-flat pull-right" href="<?= site_url('/personnel/add_profile'); ?>" > 
                                    <i class="fa fa-plus-circle"></i> Add Profile
                                </a>
                            </div>
                            <div class="box-body" ng-init='personnelType = <?= json_encode($personnel_types) ?>;
                                            personnelStatuses = <?= json_encode($personnel_statuses) ?>' cg-busy="personnelPromise" ng-cloak="">
                                <?php if (!empty($profiles)): ?>
                                    <table class="table table-condensed table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" ng-click="toggleAll()"></th>
                                                <th style="width: 80px">&nbsp;</th>
                                                <th>Type</th>
                                                <th>Name</th>
                                                <th>Date Of Birth</th>
                                                <th>Contact</th>
                                                <th>Current Assignment</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="personnel in personnels">
                                                <td>
                                                    <input type="checkbox" value="{{personnel.personnel_id}}" name="personnels[]" ng-model="personnel.is_selected"/>
                                                </td>
                                                <td>
                                                    <img src="{{personnel.picture_url}}" style="width: 40px; height: 40px" />
                                                </td>
                                                <td>{{personnelType[personnel.personnel_type]}}
                                                </td>
                                                <td>
                                                    <a href="/personnel/{{personnel.personnel_id}}/view_record">
                                                        {{personnel.surname}} {{personnel.other_names}} 
                                                    </a> <br>
                                                    <span class="label label-{{personnel.status == 0 ? 'danger' : 'warning' }}" >
                                                        {{personnelStatuses[personnel.status]}}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{personnel.dob}}
                                                </td>
                                                <td>
                                                    <i class="fa fa-envelope"></i> {{personnel.email}} <br>
                                                    <i class="fa fa-phone"></i> {{personnel.phone_number_1}} 
                                                </td>
                                                <td>{{personnel.current_assignment}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                            Action <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#" onclick="return false;" data-personnel_id="{{personnel.personnel_id}}" class="change_status">Change Status</a></li>
                                                            <li><a href="/personnel/deletePersonnel/{{personnel.personnel_id}}" class="deletePersonnel">Delete</a></li>
                                                            <li><a href="/personnel/{{personnel.personnel_id}}/view_record">View Record</a></li>
                                                            <li><a href="/personnel/{{personnel.personnel_id}}/print_pdf" target="_blank">Print Record</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data("No personnel profile has been added.");
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '_change_status.php'; ?>
    <?php include '_sms.php'; ?>
    <?php include '_email.php'; ?>
</section>



<script src="/js/personnel.js"></script>