<fieldset ng-if="personnel_type == 1 && personnel_type > 0">
    <legend>
        Honorary Title
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="hon in honouraries">
        <tr>
            <td style="width: 20%">Type</td>
            <td style="width: 30%">
                <select name="honorary[{{$index}}][honorary_title_id]" ng-model="honorary_title_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($honourary_titles)):
                        foreach ($honourary_titles as $title):
                            ?>
                            <option value="<?= $title->honorary_title_id ?>"><?= ucfirst(trim($title->honourary_title)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td style="width: 20%">Place</td>
            <td><input type="text" class="form-control" ng-model="honorary_place" name="honorary[{{$index}}][honorary_place]" value=""></td>
        </tr>
        <tr>
            <td>Date</td>
            <td><input type="text" class="input-medium datepicker" ng-model="honorary_date" name="honorary[{{$index}}][honorary_date]" ></td>
            <td>Prelate</td>
            <td><input type="text" class="form-control" ng-model="honorary_prelate" name="honorary[{{$index}}][honorary_prelate]"></td>
        </tr>
        <tr>
            <td colspan="4">
                <a href="#" onclick="return false;" ng-click="removeHonour($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
    <a href="#" onclick="return false;" ng-click="addNewHonour()" class="btn btn-success btn-sm btn-flat pull-right"><i class="fa fa-plus-circle"></i> Add More</a>
</fieldset>