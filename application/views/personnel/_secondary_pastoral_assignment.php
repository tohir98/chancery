<fieldset ng-if="personnel_type == 1 && personnel_type > 0 ">
    <legend>
        Administrative Offices/Assignments Till Date
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="ed in sassignments">
        <tr>
            <td style="width: 15%">Name of Administrative Office/Assignment</td>
            <td style="width: 35%">
                <input type="text" class="form-control" id="nameof_secondary_assignment" name="secondary[{{$index}}][nameof_secondary_assignment]" value="">
            </td>
            <td style="width: 20%">Position</td>
            <td>
                <select name="secondary[{{$index}}][position_secondary]" id="position_secondary" class="form-control">
                    <option value="">Select Admin Office</option>
                    <?php
                    if (!empty($admin_offices)):
                        foreach ($admin_offices as $office):
                            ?>
                            <option value="<?= $office->admin_office_id ?>"><?= ucfirst(trim($office->admin_office)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Period Year</td>
            <td>
                From: <input type="text" name="secondary[{{$index}}][from]" placeholder="Jan. 2016" > To <input type="text" name="secondary[{{$index}}][to]" placeholder="Feb. 2017" >
            </td>
            <td>&nbsp;</td>
            <td>
                <a href="#" onclick="return false;" ng-click="removeSecondaryAssignment($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
     <a href="#" onclick="return false;" ng-click="addNewSecondaryAssignment()" class="btn btn-success btn-sm btn-flat pull-right">
        <i class="fa fa-plus-circle"></i> Add Administrative Office
    </a>
</fieldset>