<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('/personnel/profiles') ?>" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
        <a href="<?= site_url('/personnel/'. $this->uri->segment(2) . '/print_pdf') ?>" target="_blank" class="btn btn-flat btn-success btn-sm">
            <i class="fa fa-file-pdf-o"></i> Print
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Personnel</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <select class="form-control institutions select2" id="cboStudents">

                <?php
                if (!empty($personnels)):
                    foreach ($personnels as $person):
                        $sel = '';
                        if ($person->personnel_id == $this->uri->segment(2)):
                            $sel = 'selected';
                        else:
                            $sel = '';
                        endif;
                        ?>
                        <option value="<?= $person->personnel_id; ?>" <?= $sel; ?>><?= ucfirst($person->surname . ' ' . $person->other_names); ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                    <div class="row-fluid" style="height:180px;overflow:hidden">

                        <?php
                        $pic_src = "/img/avatar5.png";
                        if (strlen($profile_picture) > 0):
                            $pic_src = $profile_picture;
                        endif;
                        ?>
                        <img src="<?= $pic_src ?>" style="max-width: 150px">
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <div class="btn-group" style="width: 100%">
                        <button type="button" style="width: 100%" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit
                            <span class="caret"></span> 
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= site_url('/personnel/change_emp_picture/' . $this->uri->segment(2)) ?>">Change Picture</a></li>
                            <?php if ($profile_picture): ?>
                                <li><a class="remove_pic" href="<?= site_url('/personnel/remove_emp_picture/' . $this->uri->segment(2)) ?>">Remove Picture</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body no-padding" style="margin-top: 10px">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a class="left_menu_button" href="#profile_container"> Profile</a></li>
                                <li> <a class="left_menu_button" href="#current_residence_container">Residence Details</a></li>
                                <?php if ($personnel_type == PERSONNEL_PRIST) : ?>
                                    <li> <a class="left_menu_button" href="#home_parish_container">Home Parish</a></li>
                                    <li> <a class="left_menu_button" href="#family_contact_container">Family Contact</a></li>
                                    <li> <a class="left_menu_button" href="#health_info_container">Health Info</a></li>
                                    <li> <a class="left_menu_button" href="#ordination_container">Ordination</a></li>
                                    <li> <a class="left_menu_button" href="#honorary_title_container">Honorary Title</a></li>
                                    <li> <a class="left_menu_button" href="#current_assignment_container">Current Assignment</a></li>
                                    <li> <a class="left_menu_button" href="#pastoral_assignment_container">Pastoral Assignment</a></li>
                                    <li> <a class="left_menu_button" href="#admin_office_container">Administrative Offices</a></li>
                                <?php endif; ?>
                                <?php if ($personnel_type == PERSONNEL_REV_SISTER) : ?>
                                    <li> <a class="left_menu_button" href="#apostolate_container">Work Assigned/Apostolate</a></li>
                                <?php endif; ?>
                                <?php if ($personnel_type != PERSONNEL_REV_SISTER) : ?>
                                    <li> <a class="left_menu_button" href="#education_container">Educational Background</a></li>
                                <?php endif; ?>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php
            echo $profile_content;
            echo $current_residence_content;
            echo $family_contact_content;
            if ($personnel_type == PERSONNEL_PRIST) :
                echo $home_parish_content;
                echo $health_info_content;
                echo $ordination_content;
                echo $honorary_title_content;
                echo $current_assignment_content;
                echo $pastoral_assignment_content;
                echo $admin_office_content;
            endif;
            if ($personnel_type == PERSONNEL_REV_SISTER) :
                echo $apostolate_content;
            endif;
            if ($personnel_type != PERSONNEL_REV_SISTER) :
                echo $education_content;
            endif;
            ?>
        </div>
    </div>
</section>

<div class="modal" id="modal_edit_user">
    
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
            $('.remove_pic').click(function (e) {
                e.preventDefault();
                var h = this.href;
                var message = 'Are you sure you want to remove this profile picture ?';
                OaaStudy.doConfirm({
                    title: 'Confirm ',
                    message: message,
                    onAccept: function () {
                        window.location = h;
                    }
                });
            });
        });
        
        $(function () {
            $('.delete').click(function (e) {
                e.preventDefault();
                var h = this.href;
                var message = 'Are you sure you want to remove this item ?';
                OaaStudy.doConfirm({
                    title: 'Confirm ',
                    message: message,
                    onAccept: function () {
                        window.location = h;
                    }
                });
            });
        });


        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.employee_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.institutions').change(function () {
            var ref_id_ = $(this).val();

            window.location.href = '<?php echo site_url("/personnel"); ?>/' + ref_id_ + '/view_record';
        });

        $('body').delegate('.edit', 'click', function (evt) {
            evt.preventDefault();

            $('#modal_edit_user').modal('show');
            $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

            var page = $(this).attr("href");
            $.get(page, function (html) {

                $('#modal_edit_user').html('');
                $('#modal_edit_user').html(html);
                $('#modal_edit_user').modal('show').fadeIn();
            });
            return false;
        });




    });


</script>
