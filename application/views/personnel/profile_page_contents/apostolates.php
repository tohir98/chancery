<div class="box box-solid box-primary employee_info_box" id="apostolate_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Work(s) Assigned/Apostolate

        </h3>
    </div>
    <div class="box-body">
        <?php if ($apostolates): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name of Assignment</th>
                        <th>Position</th>
                        <th>Period</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php foreach ($apostolates as $apostolate): ?>
                    <tr>
                        <td><?= $apostolate->name_of_assignment; ?></td>
                        <td><?= $apostolate->position; ?></td>
                        <td><?= $apostolate->year_from; ?> to <?= $apostolate->year_to; ?></td>
                        <td>
                            <a href="/personnel/editApostolate/<?= $apostolate->personnel_apostolate_id; ?>/<?= $this->uri->segment(2); ?>" class="edit">
                                <i class="fa fa-edit"></i> Edit</a> |
                            <a href="/personnel/deleteApostolate/<?= $apostolate->personnel_apostolate_id; ?>" class="delete">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
