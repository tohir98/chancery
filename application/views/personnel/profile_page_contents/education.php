<div class="box box-solid box-primary employee_info_box" id="education_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Educational Background
            <a href="#addNewEducation" data-toggle="modal" class="btn btn-warning pull-right" style="margin-right: 5px;">Add New</a>

        </h3>
    </div>
    <div class="box-body">
        <?php if ($educations): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Education Type</th>
                        <th>Institution</th>
                        <th>Period</th>
                        <th>Certificate</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php foreach ($educations as $education): ?>
                    <tr>
                        <td><?= $education->education_type; ?></td>
                        <td><?= $education->institution_name; ?></td>
                        <td><?= $education->year_from; ?> to <?= $education->year_to; ?></td>
                        <td><?= $education->degree; ?></td>
                        <td>
                            <a title="Edit" href="/personnel/editEducation/<?= $education->personnel_education_background_id; ?>/<?= $this->uri->segment(2); ?>" class="edit">
                                <i class="fa fa-edit"></i> Edit</a> |
                            <a title="Delete" href="/personnel/deleteEducation/<?= $education->personnel_education_background_id; ?>" class="delete">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="modal" id="addNewEducation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Education</h4>
            </div>
            <form role="form" method="post" action="<?= site_url("/personnel/addEducation"); ?>">
                <?php include APPPATH . 'views/personnel/edit/_education_form.php'; ?>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" value="<?= $this->uri->segment(2); ?>" />
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>