<div class="box box-solid box-primary employee_info_box" id="home_parish_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Home Parish
            <a href="/personnel/<?= $this->uri->segment(2); ?>/edit_home_parish" class="edit btn btn-warning pull-right" style="margin-right: 5px;">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php 
        if($record): ?>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    Parish:
                </td>
                <td>
                    <?= $record->parish ?>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <?= $record->city; ?>
                </td>
            </tr>
            <tr>
                <td>
                    State:
                </td>
                <td>
                    <?= $record->state; ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
