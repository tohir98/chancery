<div class="box box-solid box-primary employee_info_box" id="current_residence_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Residence Info
            <a href="/personnel/<?= $this->uri->segment(2); ?>/edit_residence_info" class="btn btn-warning pull-right" style="margin-right: 5px;">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php 
        if($record): ?>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    Place of residence:
                </td>
                <td>
                    <?= $record->place_of_residence ?>
                </td>
            </tr>
            <tr>
                <td>
                    Address:
                </td>
                <td>
                    <?= $record->address; ?>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <?= $record->city; ?>
                </td>
            </tr>
            <tr>
                <td>
                    State:
                </td>
                <td>
                    <?= $record->state ?>
                </td>
            </tr>
            <tr>
                <td>
                    Region:
                </td>
                <td>
                    <?= $record->region ?>
                </td>
            </tr>
            <tr>
                <td>
                    Deanery:
                </td>
                <td>
                    <?= $record->deanery; ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
