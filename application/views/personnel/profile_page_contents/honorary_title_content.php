<div class="box box-solid box-primary employee_info_box" id="honorary_title_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Honorary Titles
            <a href="#addNewTitle" data-toggle="modal" class="btn btn-warning pull-right" style="margin-right: 5px;">Add New</a>

        </h3>
    </div>
    <div class="box-body">
        <?php if ($Honorarytitles): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Place</th>
                        <th>Date</th>
                        <th>Ordaining Prelate</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php foreach ($Honorarytitles as $title): ?>
                    <tr>
                        <td><?= $title->honourary_title; ?></td>
                        <td><?= $title->place; ?></td>
                        <td><?= date('d-M-Y', strtotime($title->date)); ?> </td>
                        <td><?= $title->prelate; ?></td>
                        <td>
                            <a title="Edit" href="/personnel/editHonoraryTitle/<?= $title->personnel_honorary_title_id; ?>/<?= $this->uri->segment(2); ?>" class="edit">
                                <i class="fa fa-edit"></i> Edit</a> |
                            <a title="Remove" href="/personnel/deleteHonoraryTitle/<?= $title->personnel_honorary_title_id; ?>" class="delete">
                                <i class="fa fa-trash"></i> Remove
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else:
            echo show_no_data("Honorary titles no found");
            endif; ?>
    </div>
</div>

<div class="modal" id="addNewTitle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Honorary Title</h4>
            </div>
            <form role="form" method="post" action="<?= site_url("/personnel/addHonoraryTitle"); ?>">
                <?php include APPPATH . 'views/personnel/edit/_honorary_title_form.php'; ?>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" value="<?= $this->uri->segment(2); ?>" />
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>