<div class="box box-solid box-primary employee_info_box" id="family_contact_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Family Contact Info
            <a href="/personnel/<?= $this->uri->segment(2); ?>/edit_family_contact" class="edit btn btn-warning pull-right" style="margin-right: 5px;">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php 
        if($record): ?>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    First Name:
                </td>
                <td>
                    <?= ucfirst($record->first_name); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Last Name:
                </td>
                <td>
                    <?= ucfirst($record->last_name); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Phone Number:
                </td>
                <td>
                    <?= $record->phone_number; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Relationship:
                </td>
                <td>
                    <?= $record->relationship ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
