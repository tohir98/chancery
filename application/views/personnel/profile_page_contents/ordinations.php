<div class="box box-solid box-primary employee_info_box" id="ordination_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Ordinations
            <a href="#addNewOrdination" data-toggle="modal" class="btn btn-warning pull-right" style="margin-right: 5px;">Add New</a>

        </h3>
    </div>
    <div class="box-body">
        <?php if ($ordinations): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Ordination Type</th>
                        <th>Place</th>
                        <th>Date</th>
                        <th>Ordaining Prelate</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php foreach ($ordinations as $ordination): ?>
                    <tr>
                        <td><?= $ordination->ordination_type; ?></td>
                        <td><?= $ordination->place; ?></td>
                        <td><?= date('d-M-Y', strtotime($ordination->date)); ?> </td>
                        <td><?= $ordination->ordaining_prelate; ?></td>
                        <td>
                            <a title="Edit" href="/personnel/editOrdination/<?= $ordination->personnel_ordination_id; ?>/<?= $this->uri->segment(2); ?>" class="edit">
                                <i class="fa fa-edit"></i> Edit</a> |
                            <a title="Remove" href="/personnel/deleteOrdination/<?= $ordination->personnel_ordination_id; ?>" class="delete">
                                <i class="fa fa-trash"></i> Remove
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="modal" id="addNewOrdination">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Ordination</h4>
            </div>
            <form role="form" method="post" action="<?= site_url("/personnel/addOrdination"); ?>">
                <?php include APPPATH . 'views/personnel/edit/_ordination_form.php'; ?>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" value="<?= $this->uri->segment(2); ?>" />
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>