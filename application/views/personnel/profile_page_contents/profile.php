<div class="box box-solid box-primary employee_info_box" id="profile_container">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Profile
            <a class="btn btn-warning btn-flat pull-right" href="/personnel/<?= $this->uri->segment(2); ?>/edit_record" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php if ($record): ?>
            <table class="table table-striped table-hover">
                <tr>
                    <td style="width: 50%">
                        Title:
                    </td>
                    <td>
                        <?= ucfirst($record->title_name) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <?= ucfirst($record->surname) . ' ' . ucfirst($record->other_names) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date of Birth:
                    </td>
                    <td>
                        <?= $record->dob !== '0000-00-00' ? date('d-M-Y', strtotime($record->dob)) : 'NA' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Place of Birth:
                    </td>
                    <td>
                        <?= $record->place_of_birth ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nationality:
                    </td>
                    <td>
                        <?= $record->nationality ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        State:
                    </td>
                    <td>
                        <?= $record->state; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        LGA:
                    </td>
                    <td>
                        <?= $record->lgovt; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <?= $record->email ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone:
                    </td>
                    <td>
                        <?= $record->phone_number_1 ?> <br>
                        <?= $record->phone_number_2 ?>
                    </td>
                </tr>
                <?php if ($record->personnel_type == PERSONNEL_PRIST): ?>
                    <tr>
                        <td>
                            Incardination Type:
                        </td>
                        <td>
                            <?= $record->incardination ?> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Name of Diocese/Congregation/Armed Forces:
                        </td>
                        <td>
                            <?= $record->congregation ?> 
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
