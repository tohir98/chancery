<div class="box box-solid box-primary employee_info_box" id="current_assignment_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Current Assignment
            <a class="btn btn-warning btn-flat pull-right" href="#currentAssignment" data-toggle="modal" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php if ($record): ?>
            <table class="table table-striped table-hover">
                <tr>
                    <td>
                        Current Assignment:
                    </td>
                    <td>
                        <?= ucfirst($record->current_assignment) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Position:
                    </td>
                    <td>
                        <?= ucfirst($record->current_position) ?>
                    </td>
                </tr>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="modal" id="currentAssignment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit Current Assignment</h4>
            </div>
            <form role="form" method="post" action="<?= site_url("/personnel/editCurrentAssignment"); ?>">
                <div class="modal-body">
                    <table class="table table-bordered table-condensed table-striped table-hover">
                        <tr>
                            <td style="width: 20%">Name of Assignment</td>
                            <td><input required type="text" class="form-control" id="current_assignment" name="current_assignment" value="<?= $record->current_assignment; ?>"></td>
                        </tr>
                        <tr>
                            <td style="width: 15%">Current Position</td>
                            <td style="width: 35%">
                                <select name="current_position_id" id="current_position_id"  class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($current_positions)):
                                        $sel = '';
                                        foreach ($current_positions as $office):
                                            if (!empty($record)) :
                                                if ($office->current_position_id == $record->current_position_id) :
                                                    $sel = 'selected';
                                                else:
                                                    $sel = '';
                                                endif;
                                            endif;
                                            ?>
                                            <option value="<?= $office->current_position_id ?>" <?= $sel; ?>><?= ucfirst(trim($office->position)) ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" value="<?= $this->uri->segment(2); ?>" />
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
