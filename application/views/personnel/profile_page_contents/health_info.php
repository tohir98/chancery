<div class="box box-solid box-primary employee_info_box" id="health_info_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Health Info
            <a href="/personnel/<?= $this->uri->segment(2); ?>/edit_health_info" class="edit btn btn-warning pull-right" style="margin-right: 5px;">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php 
        if($health_info): ?>
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    Genotype:
                </td>
                <td>
                    <?= $health_info->genotype ?>
                </td>
            </tr>
            <tr>
                <td>
                    Blood Group:
                </td>
                <td>
                    <?= $health_info->blood_group; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Health History:
                </td>
                <td>
                    <?= $health_info->health_history; ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
