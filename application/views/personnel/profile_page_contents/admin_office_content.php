<div class="box box-solid box-primary employee_info_box" id="admin_office_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Admin Offices
            <a href="#addNewSeAssignment" data-toggle="modal" class="btn btn-warning pull-right" style="margin-right: 5px;">Add New</a>

        </h3>
    </div>
    <div class="box-body">
        <?php if ($secondary_assignments): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name of Assignment</th>
                        <th>Position</th>
                        <th>Period</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php foreach ($secondary_assignments as $ass): ?>
                    <tr>
                        <td><?= $ass->name_of_secondary_assignment; ?></td>
                        <td><?= $ass->admin_office; ?></td>
                        <td><?= $ass->year_from; ?> to <?= $ass->year_to; ?></td>
                        <td>
                            <a title="Edit" href="/personnel/editSeAssignment/<?= $ass->personnel_secondary_assignment_id; ?>/<?= $this->uri->segment(2); ?>" class="edit">
                                <i class="fa fa-edit"></i> Edit</a> |
                            <a title="Delete" href="/personnel/deleteSeAssignment/<?= $ass->personnel_secondary_assignment_id; ?>" class="delete">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="modal" id="addNewSeAssignment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Admin. Office</h4>
            </div>
            <form role="form" method="post" action="<?= site_url("/personnel/addSecondaryAssignment"); ?>">
                <?php include APPPATH . 'views/personnel/edit/_secondary_assignment_form.php'; ?>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" value="<?= $this->uri->segment(2); ?>" />
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>