<fieldset ng-if="personnel_type != 2 && personnel_type > 0 ">
    <legend>
        Family Member Contact
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 20%">First Name</td>
            <td><input required type="text" class="form-control" name="contact_first_name"></td>
            <td>Last Name</td>
            <td><input required type="text" class="form-control" name="contact_last_name"></td>
        </tr>
        <tr>
            <td>Relationship</td>
            <td>
                <select required name="relationship_id" class="select2 form-control">
                    <option value="">Select relationship</option>
                    <?php
                    if (!empty($relationships)):
                        foreach ($relationships as $relation):
                            ?>
                            <option value="<?= $relation->relationship_id ?>"><?= trim($relation->relationship) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>Phone Number</td>
            <td><input required type="text" class="form-control" id="emergencyphone_number" name="emergencyphone_number" value=""></td>
        </tr>
    </table>
</fieldset>