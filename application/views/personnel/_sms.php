<div class="modal" id="sms-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Send SMS</h4>
            </div>
            <form class="form-horizontal" method="post" action="<?= site_url('/personnel/sendSms'); ?>">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>Sender ID</td>
                            <td>
                                <input required type="text" name="sender_id" id="sender_id" class="form-control input-medium" maxlength="10" />
                            </td>
                        </tr>
                        <tr>
                            <td>Content</td>
                            <td>
                                <textarea required style="width: 90%" rows="2" name="sms_message"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_ids" value="{{buildIdList(selectedPersonnels, 'personnel_id', ',')}}" />
                    <button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat">Send SMS</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>