<!-- Bootstrap WYSIHTML5 -->
<link href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    
<?= show_notification(); ?>
<section class="content-header">
    <h1>
        <a class="btn btn-warning btn-flat" href="/personnel/profiles"><i class="fa fa-chevron-left"></i> Back</a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Profile</li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Compose New Message</h3>
                </div><!-- /.box-header -->
                <form method="post" class="form-horizontal" style="margin-left: 10px">
                
                <div class="box-body">
                    <div class="form-group">
                        <?php if (!empty($personnels)):
                            foreach ($personnels as $person) : ?>
                        <span class="label label-info"><?= $person->email;?></span>
                            <?php endforeach;
                        endif; ?>
                    </div>
                    <div class="form-group">
                        <input required class="form-control input-xlarge" placeholder="Subject:" name="email_subject" id="email_subject"/>
                    </div>
                    <div class="form-group">
                        <textarea required id="compose-textarea" name="email_message" class="form-control" style="height: 300px; width: 80%">
                      
                        </textarea>
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <button class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                    </div>
                    <button class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
                </div><!-- /.box-footer -->
                </form>
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        //Add text editor
        $("#compose-textarea").wysihtml5();
    });
</script>