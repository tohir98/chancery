<div class="modal-body">
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 15%">Education Type</td>
            <td style="width: 35%">
                <select name="education_type_id" id="education_type_id"  class="form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($education_types)):
                        $sel = '';
                        foreach ($education_types as $type):
                            if (!empty($education)) :
                                if ($type->education_type_id == $education->education_type_id) :
                                    $sel = 'selected';
                                else:
                                    $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $type->education_type_id ?>" <?= $sel; ?>><?= ucfirst(trim($type->education_type)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 20%">Institution Name</td>
            <td><input required type="text" class="form-control" id="institution_name" name="institution_name" value="<?= isset($education->institution_name) ? $education->institution_name : ''; ?>"></td>
        </tr>
        <tr>
            <td>Period Year</td>
            <td>
                From: 
                <input type="text" class="" name="year_from" id="from" placeholder="2001" value="<?= isset($education->year_from) ? $education->year_from : ''; ?>" >

                To 
                <input type="text" class="" name="year_to" id="to" placeholder="2005" value="<?= isset($education->year_to) ? $education->year_to : ''; ?>">
            </td>
        </tr>
        <tr>
            <td>Degree</td>
            <td><input required type="text" class="form-control" id="degree" name="degree" value="<?= isset($education->degree) ? $education->degree : ''; ?>"></td>
        </tr>
    </table>
</div>