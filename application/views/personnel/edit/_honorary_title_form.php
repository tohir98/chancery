<div class="modal-body">
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 15%">Type</td>
            <td style="width: 35%">
                <select name="honorary_title_id" id="honorary_title_id"  class="form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($honourary_titles)):
                        $sel = '';
                        foreach ($honourary_titles as $type):
                            if (!empty($hTitle)) :
                                if ($type->honorary_title_id == $hTitle->honorary_title_id) :
                                    $sel = 'selected';
                                else:
                                    $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $type->honorary_title_id ?>" <?= $sel; ?>><?= ucfirst(trim($type->honourary_title)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 20%">Place</td>
            <td><input required type="text" class="form-control" id="place" name="place" value="<?= isset($hTitle->place) ? $hTitle->place : ''; ?>"></td>
        </tr>
        <tr>
            <td>Date</td>
            <td>
                <input type="text" class="datepicker" name="date" id="date" value="<?= isset($hTitle->date) ? date('d/m/Y', strtotime($hTitle->date)) : ''; ?>" >
            </td>
        </tr>
        <tr>
            <td>Ordaining Prelate</td>
            <td><input required type="text" class="form-control" id="prelate" name="prelate" value="<?= isset($hTitle->prelate) ? $hTitle->prelate : ''; ?>"></td>
        </tr>
    </table>
</div>