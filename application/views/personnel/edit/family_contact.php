<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Family Contact</h4>
        </div>
        <form role="form" method="post" action="<?= site_url("/personnel/edit_family_contact/{$personnel_id}"); ?>">
            <div class="modal-body">
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <tr>
                        <td style="width: 20%">First Name</td>
                        <td><input required type="text" class="form-control" name="first_name" value="<?= isset($contact->first_name) ? $contact->first_name : ''; ?>"></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input required type="text" class="form-control" name="last_name" value="<?= isset($contact->last_name) ? $contact->last_name : ''; ?>"></td>
                    </tr>
                    <tr>
                        <td>Relationship</td>
                        <td>
                            <select required name="relationship_id" class="select2 form-control">
                                <option value="">Select relationship</option>
                                <?php
                                if (!empty($relationships)):
                                    $sel = '';
                                    foreach ($relationships as $relation):
                                        if ($contact->relationship_id == $relation->relationship_id) :
                                            $sel = 'selected';
                                            else:
                                            $sel = '';
                                        endif;
                                        ?>
                                        <option value="<?= $relation->relationship_id ?>" <?= $sel;?>><?= trim($relation->relationship) ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td><input required type="text" class="form-control" id="phone_number" name="phone_number" value="<?= isset($contact->phone_number) ? $contact->phone_number : '';?>"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" >Update</button>
            </div>
        </form>
    </div>
</div>