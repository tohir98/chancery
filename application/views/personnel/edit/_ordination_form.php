<div class="modal-body">
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 15%">Ordination Type</td>
            <td style="width: 35%">
                <select name="ordination_type_id" id="ordination_type_id"  class="form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($ordination_types)):
                        $sel = '';
                        foreach ($ordination_types as $type):
                            if (!empty($ordination)) :
                                if ($type->ordination_type_id == $ordination->ordination_type_id) :
                                    $sel = 'selected';
                                else:
                                    $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $type->ordination_type_id ?>" <?= $sel; ?>><?= ucfirst(trim($type->ordination_type)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 20%">Place</td>
            <td><input required type="text" class="form-control" id="place" name="place" value="<?= isset($ordination->place) ? $ordination->place : ''; ?>"></td>
        </tr>
        <tr>
            <td>Date</td>
            <td>
                <input type="text" class="datepicker" name="date" id="date" value="<?= isset($ordination->date) ? date('d/m/Y', strtotime($ordination->date)) : ''; ?>" >
            </td>
        </tr>
        <tr>
            <td>Ordaining Prelate</td>
            <td><input required type="text" class="form-control" id="ordaining_prelate" name="ordaining_prelate" value="<?= isset($ordination->ordaining_prelate) ? $ordination->ordaining_prelate : ''; ?>"></td>
        </tr>
    </table>
</div>