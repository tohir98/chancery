<script>
    var lga_ = '<?= !empty($profile) ? $profile->lga : ''; ?>';
    var selected_state_ = '<?= !empty($profile) ? $profile->state_id : ''; ?>';
    var region_id_ = '<?= !empty($residence) ? $residence->region_id : ''; ?>';
    var selected_deanery_ = '<?= !empty($residence) ? $residence->deanery_id : ''; ?>';
    var incardination_type_id_ = '<?= !empty($profile) ? $profile->incardination_type_id : ''; ?>';
    var selected_congregation_id_ = '<?= !empty($profile) ? $profile->congregation_id : ''; ?>';
</script>
<?php include '_header.php'; ?>

<section class="content" ng-app="personnel" ng-controller="personnelCtrl" ng-init="personnel_type = '<?= $profile->personnel_type; ?>'; personal.region_id = '<?= $residence->region_id ?>';
                ">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form role="form" method="post" action="">
                        <div class="modal-body">
                            <table class="table table-bordered table-condensed table-striped">
                                <tr>
                                    <td style="width: 20%">Place of Residence</td>
                                    <td>
                                        <input required type="text" class="form-control" id="place_of_residence" name="place_of_residence" value="<?= $residence->place_of_residence; ?>">
                                    </td>
                                    <td style="width: 20%">Address</td>
                                    <td>
                                        <input required type="text" class="form-control" id="address" name="address" value="<?= $residence->address; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>
                                        <input required type="text" class="form-control" id="city" name="city" value="<?= $residence->city; ?>">
                                    </td>
                                    <td>State</td>
                                    <td>
                                        <select required class="form-control" id="state_id" name="state_id">
                                            <option value="">Select State</option>
                                            <?php
                                            if (!empty($states)):
                                                $sel = '';
                                                foreach ($states as $state):
                                                    if ($residence->state_id == $state->state_id) :
                                                        $sel = 'selected';
                                                    else:
                                                        $sel = '';
                                                    endif;
                                                    ?>
                                                    <option value="<?= $state->state_id ?>" <?= $sel; ?> ><?= ucfirst(trim($state->state)) ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php if ($profile->personnel_type != PERSONNEL_SEMINARIANS): ?>
                                    <tr>
                                        <td>Region</td>
                                        <td>
                                            <select required class="form-control" id="region_id" name="region_id"  ng-model="personal.region_id" ng-change="loadDeanary()">
                                                <option value="">Select Region</option>
                                                <?php
                                                if (!empty($regions)):
                                                    foreach ($regions as $region):
                                                        ?>
                                                        <option value="<?= $region->region_id ?>"><?= ucfirst(trim($region->region)) ?></option>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td>Deanery</td>
                                        <td>
                                            <div ng-if="personal.deanaryStatus !== ''">
                                                {{personal.deanaryStatus}}
                                            </div>
                                            <div ng-if="personal.deanaryStatus === ''">
                                                <select name="deanery_id" id="deanery_id" class="form-control">
                                                    <option ng-repeat="lg in personal.deanarys" value="{{lg.id}}" ng-selected="selected_deanery.indexOf(lg.id + '') > -1">
                                                        {{lg.name}}
                                                    </option>
                                                </select>

                                            </div>
                                        </td>
                                    </tr>
                                <?php endif; ?>

                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" >Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="/js/personnel.js"></script>