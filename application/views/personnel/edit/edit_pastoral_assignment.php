<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Pastoral Assignment</h4>
        </div>
        <form role="form" method="post" action="<?= site_url("/personnel/editPaAssignment/{$assignment_id}/{$personnel_id}"); ?>">
            <?php include '_pa_assignment_form.php'; ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" >Update</button>
            </div>
        </form>
    </div>
</div>