<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Home Parish</h4>
        </div>
        <form role="form" method="post" action="<?= site_url("/personnel/edit_home_parish/{$personnel_id}"); ?>">
            <div class="modal-body">
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <tr>
                        <td style="width: 20%">Parish</td>
                        <td>
                            <input type="text" name="parish" id="parish" value="<?= $home_parish->parish ? : ''; ?>" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%">City</td>
                        <td>
                            <input type="text" name="city" id="city" value="<?= $home_parish->city ? : ''; ?>" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%">State</td>
                        <td style="width: 30%">
                            <select name="state_id" id="state_id" class="select2 form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($states)):
                                    $sel = '';
                                    foreach ($states as $state):
                                        if (isset($home_parish->state_id)) :
                                            if ($home_parish->state_id == $state->state_id) :
                                                $sel = 'selected';
                                            else:
                                                $sel = '';
                                            endif;
                                        endif;
                                        ?>
                                        <option value="<?= $state->state_id ?>" <?= $sel; ?>><?= trim($state->state) ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" >Update</button>
            </div>
        </form>
    </div>
</div>