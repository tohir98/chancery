<script>
    var lga_ = '<?= !empty($profile) ? $profile->lga : ''; ?>';
    var selected_state_ = '<?= !empty($profile) ? $profile->state_id : ''; ?>';
    var region_id_ = '<?= !empty($residence) ? $residence->region_id : ''; ?>';
    var selected_deanery_ = '<?= !empty($residence) ? $residence->deanery_id : ''; ?>';
    var incardination_type_id_ = '<?= !empty($profile) ? $profile->incardination_type_id : ''; ?>';
    var selected_congregation_id_ = '<?= !empty($profile) ? $profile->congregation_id : ''; ?>';
</script>
<?php include '_header.php'; ?>
<!-- Main content -->
<section class="content" ng-app="personnel" ng-controller="personnelCtrl" ng-init="personnel_type = '<?= $profile->personnel_type; ?>'; personal.state_id = '<?= $profile->state_id; ?>';
                personal.incardination_type_id = '<?= $profile->incardination_type_id; ?>'">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">

                            <div class="box-body">
                                <form id="frm" action="" role="form" method="post" enctype="multipart/form-data" class="form-horizontal" >

                                    <table class="table table-condensed" ng-if="personnel_type > 0" >

                                        <tr ng-if="personnel_type != 3">
                                            <td>Title</td>
                                            <td>
                                                <select required class="form-control" id="title_id" name="title_id" ng-if="personnel_type == 1">
                                                    <option value="">Select Title</option>
                                                    <?php
                                                    if (!empty($titles)):
                                                        $sel = '';
                                                        foreach ($titles as $title):
                                                            if ($title->title_id == $profile->title_id) :
                                                                $sel = 'selected';
                                                                else:
                                                                $sel = '';
                                                            endif;
                                                            ?>
                                                            <option value="<?= $title->title_id ?>" <?= $sel; ?> ><?= trim($title->title); ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                                <input type="text" name="title" placeholder="Title" class="form-control" ng-if="personnel_type == 2" value="<?= $profile->title; ?>" />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">Surname</td>
                                            <td style="width: 30%">
                                                <input type="text" name="surname" id="surname" class="form-control" value="<?= $profile->surname; ?>" />
                                            </td>
                                            <td style="width: 20%">Other Names</td>
                                            <td>
                                                <input type="text" name="other_names" id="other_names" class="form-control" value="<?= $profile->other_names; ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth:</td>
                                            <td><input required type="text" class="form-control datepicker" id="dob" name="dob" value="<?= date('d/m/Y', strtotime($profile->dob)); ?>" ></td>
                                            <td>Place of Birth</td>
                                            <td><input required type="text" class="form-control" id="placeof_origin" name="place_of_birth" value="<?= $profile->place_of_birth; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality</td>
                                            <td><input required type="text" class="form-control" id="nationality" name="nationality" value="<?= $profile->nationality; ?>"></td>
                                            <td>State</td>
                                            <td>
                                                <select required class="form-control" id="state_id" name="state_id" ng-model="personal.state_id" ng-change="loadLgas()">
                                                    <option value="">Select State</option>
                                                    <?php
                                                    if (!empty($states)):
                                                        foreach ($states as $state):
                                                            ?>
                                                            <option value="<?= $state->state_id ?>"><?= ucfirst(trim($state->state)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td>Local Government Area</td>
                                            <td>
                                                <div ng-if="personal.lgaStatus !== ''">
                                                    {{personal.lgaStatus}}
                                                </div>
                                                <div ng-if="personal.lgaStatus === ''">
                                                    <select name="lga_id" id="lga_id" class="form-control">
                                                        <option ng-repeat="lg in personal.lgas" value="{{lg.id}}" ng-selected="selected_lga.indexOf(lg.id + '') > -1">
                                        {{lg.name}}
                                    </option>
                                                    </select>

                                                </div>
                                            </td>
                                            <td>Email</td>
                                            <td><input required type="email" class="form-control" id="email" name="email" value="<?= $profile->email; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Phone Number 1</td>
                                            <td><input required type="text" class="form-control" id="phone" name="phone_number_1" value="<?= $profile->phone_number_1; ?>"></td>
                                            <td>Phone Number 2</td>
                                            <td><input type="text" class="form-control" id="phone2" name="phone_number_2" value="<?= $profile->phone_number_2; ?>"></td>
                                        </tr>
                                        <tr ng-if="personnel_type == 1">
                                            <td>
                                                <span>Incardination</span>
                                            </td>
                                            <td>
                                                <select required class="form-control" id="incardination_type_id" name="incardination_type_id"  ng-model="personal.incardination_type_id" ng-change="loadCongregation()" ng-if="personnel_type == 1">
                                                    <option value="">Select Incardination</option>
                                                    <?php
                                                    if (!empty($incardination_types)):
                                                        foreach ($incardination_types as $ty):
                                                            ?>
                                                            <option value="<?= $ty->incardination_type_id ?>" ><?= ucfirst(trim($ty->incardination_type)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <span>Name of Diocese/Congregation/Armed forces</span>
                                            </td>
                                            <td style="width: 30%">
                                                <div ng-if="personal.congStatus !== '' && personnel_type == 1">
                                                    {{personal.congStatus}}
                                                </div>
                                                <div ng-if="personal.congStatus === '' && personnel_type != 3">
                                                    <select name="congregation_id" id="congregation_id" class="form-control">
                                                        <option ng-repeat="lg in personal.congregations" value="{{lg.id}}" ng-selected="selected_congregation.indexOf(lg.id + '') > -1">
                                                            {{lg.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>    
                                        </tr>
                                        <tr ng-if="personnel_type == 2">
                                            <td>Date of Religious Profession</td>
                                            <td>
                                                <input type="text" name="dateof_reli_prof" class="datepicker" value="<?= date('d/m/Y', strtotime($profile->dateof_reli_prof)); ?>" />
                                            </td>
                                            <td>
                                                <span>Name Congregation</span>
                                            </td>
                                            <td>
                                                <select required="" class="form-control select2" name="female_congregation_id" id="female_congregation_id">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($female_congregs)):
                                                        $sel = '';
                                                        foreach ($female_congregs as $cong) :
                                                            if ((int) $profile->female_congregation_id == $cong->female_congregation_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                            ?>
                                                            <option value="<?= $cong->female_congregation_id; ?>" <?= $sel; ?>><?= $cong->congregation; ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>
                                    </table>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg pull-right" ng-disabled="personnel_type == 0">Update</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/personnel.js"></script>