<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Apostolate</h4>
        </div>
        <form role="form" method="post" action="<?= site_url("/personnel/editApostolate/{$apostolate_id}/{$personnel_id}");?>">
            <div class="modal-body">
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <tr>
                        <td style="width: 15%">Name of Assignment</td>
                        <td style="width: 35%">
                            <input required type="text" class="form-control" id="name_of_assignment" name="name_of_assignment" value="<?= $apostolate->name_of_assignment; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%">Position</td>
                        <td>
                            <input required type="text" name="position" id="position_primary" class="form-control" value="<?= $apostolate->position; ?>" />

                        </td>
                    </tr>
                    <tr>
                        <td>Period Year</td>
                        <td>
                            From: <input required type="text" class="" name="year_from" placeholder="Jan - 2017" value="<?= $apostolate->year_from; ?>" > To 
                            <input type="text" class="" name="year_to" placeholder="Jun - 2017" value="<?= $apostolate->year_to; ?>" ></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" >Update</button>
            </div>
        </form>
    </div>
</div>