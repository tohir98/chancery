<div class="modal-body">
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 20%">Name of Assignment</td>
            <td><input required type="text" class="form-control" id="name_of_assignment" name="name_of_assignment" value="<?= isset($assignment->name_of_assignment) ? $assignment->name_of_assignment : ''; ?>"></td>
        </tr>
        <tr>
            <td style="width: 15%">Pastoral Position</td>
            <td style="width: 35%">
                <select name="pastoral_position_id" id="pastoral_position_id"  class="form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($pastoral_positions)):
                        $sel = '';
                        foreach ($pastoral_positions as $office):
                            if (!empty($assignment)) :
                                if ($office->pastoral_position_id == $assignment->pastoral_position_id) :
                                    $sel = 'selected';
                                else:
                                    $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $office->pastoral_position_id ?>" <?= $sel; ?>><?= ucfirst(trim($office->pastoral_position)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <td>Period Year</td>
            <td>
                From: 
                <input type="text" class="" name="year_from" id="from" placeholder="2001" value="<?= isset($assignment->year_from) ? $assignment->year_from : ''; ?>" > <br>

                To 
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<input type="text" class="" name="year_to" id="to" placeholder="2005" value="<?= isset($assignment->year_to) ? $assignment->year_to : ''; ?>">
            </td>
        </tr>
    </table>
</div>