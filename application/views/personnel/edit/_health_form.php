<div class="modal-body">
    <table class="table table-bordered table-condensed table-striped table-hover">
        <tr>
            <td style="width: 20%">Genotype</td>
            <td style="width: 30%">
                <select name="genotype_id" id="genotype_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($genotypes)):
                        $sel = '';
                        foreach ($genotypes as $type):
                         if (isset($health_info->genotype_id)) :
                                if ($health_info->genotype_id == $type->genotype_id) :
                                     $sel = 'selected';
                                    else:
                                     $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $type->genotype_id ?>" <?= $sel; ?>><?= trim($type->genotype) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 20%">Blood Group</td>
            <td>
                <select name="blood_group_id" id="blood_group_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($blood_groups)):
                        $sel = '';
                        foreach ($blood_groups as $group):
                            if (isset($health_info->blood_group_id)) :
                                if ($health_info->blood_group_id == $group->blood_group_id) :
                                     $sel = 'selected';
                                    else:
                                     $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $group->blood_group_id ?>" <?= $sel; ?>><?= trim($group->blood_group) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Health History</td>
            <td>
                <textarea name="health_history" id="health_history" rows="4" style="width: 80%">
                    <?= isset($health_info->health_history) ? trim($health_info->health_history) : ''; ?>
                </textarea>
            </td>
        </tr>
    </table>
</div>