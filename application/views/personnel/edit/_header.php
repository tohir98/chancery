<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<?= show_notification(); ?>
<section class="content-header">

    <h1>
        <a href="/personnel/<?= $this->uri->segment(2); ?>/view_record" class="btn btn-warning btn-flat btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a> 
        Edit Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Profile</li>
    </ol>
</section>