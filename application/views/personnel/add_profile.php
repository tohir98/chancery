<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script>
    var lga_ = '<?= !empty($profile) ? $profile->lga : ''; ?>';
    var selected_state_ = '<?= !empty($profile) ? $profile->state_id : ''; ?>';
    var region_id_ = '<?= !empty($residence) ? $residence->region_id : ''; ?>';
    var selected_deanery_ = '<?= !empty($residence) ? $residence->deanery_id : ''; ?>';
    var incardination_type_id_ = '<?= !empty($profile) ? $profile->incardination_type_id : ''; ?>';
    var selected_congregation_id_ = '<?= !empty($profile) ? $profile->congregation_id : ''; ?>';
</script>
<?= show_notification(); ?>
<!--<section class="content-header">
    <img src="/img/chan_logo.png" />
</section>-->

<!-- Main content -->
<section class="content" ng-app="personnel" ng-controller="personnelCtrl">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <h1><img src="/img/chan_logo.png" /> Chancery Data Form</h1>
                            </div>
                            <div class="box-body">
                                <form id="frm" action="" role="form" method="post" enctype="multipart/form-data" class="form-horizontal" >
                                    <div class="form-group">
                                        <label class="col-md-3">Personnel Type</label>
                                        <div class="col-md-3">
                                            <select name="personnel_type" ng-model="personnel_type" class="form-control select2">
                                                <option value="0">Select</option>
                                                <?php foreach ($personnel_types as $pType => $val): ?>
                                                    <option value="<?= $pType; ?>"><?= $val; ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3">Picture</label>
                                        <div class="col-md-3">
                                            <input required type="file" name="userfile" />
                                        </div>
                                    </div>
                                    <table class="table table-condensed" ng-if="personnel_type > 0" >

                                        <tr ng-if="personnel_type != 3">
                                            <td>Title</td>
                                            <td>
                                                <select required class="form-control" id="title_id" name="title_id" ng-if="personnel_type == 1">
                                                    <option value="">Select Title</option>
                                                    <?php
                                                    if (!empty($titles)):
                                                        foreach ($titles as $title):
                                                            ?>
                                                            <option value="<?= $title->title_id ?>" ><?= trim($title->title); ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                                <input type="text" name="title" placeholder="Title" class="form-control" ng-if="personnel_type == 2" />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">Surname</td>
                                            <td style="width: 30%">
                                                <input type="text" name="surname" id="surname" class="form-control" value="" />
                                            </td>
                                            <td style="width: 20%">Other Names</td>
                                            <td>
                                                <input type="text" name="other_names" id="other_names" class="form-control" value="" />
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>Date of Birth:</td>
                                            <td><input required type="text" class="form-control datepicker" id="dob" name="dob" value="" ></td>
                                            <td>Place of Birth</td>
                                            <td><input required type="text" class="form-control" id="placeof_origin" name="placeof_birth" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality</td>
                                            <td><input required type="text" class="form-control" id="nationality" name="nationality" value=""></td>
                                            <td>State</td>
                                            <td>
                                                <select required class="form-control" id="state_id" name="state_id" ng-model="personal.state_id" ng-change="loadLgas()">
                                                    <option value="">Select State</option>
                                                    <?php
                                                    if (!empty($states)):
                                                        foreach ($states as $state):
                                                            ?>
                                                            <option value="<?= $state->state_id ?>"><?= ucfirst(trim($state->state)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td>Local Government Area</td>
                                            <td>
                                                <div ng-if="personal.lgaStatus !== ''">
                                                    {{personal.lgaStatus}}
                                                </div>
                                                <div ng-if="personal.lgaStatus === ''">
                                                    <select name="lga_id" id="lga_id" class="form-control">
                                                        <option ng-repeat="lg in personal.lgas" value="{{lg.id}}">
                                                            {{lg.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>
                                            <td>Email</td>
                                            <td><input required type="email" class="form-control" id="email" name="email" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>Phone Number 1</td>
                                            <td><input required type="text" class="form-control" id="phone" name="phone" value=""></td>
                                            <td>Phone Number 2</td>
                                            <td><input type="text" class="form-control" id="phone2" name="phone2" value=""></td>
                                        </tr>



                                        <tr ng-if="personnel_type == 1">
                                            <td>
                                                <span>Incardination</span>
                                            </td>
                                            <td>
                                                <select required class="form-control" id="incardination_type_id" name="incardination_type_id"  ng-model="personal.incardination_type_id" ng-change="loadCongregation()" ng-if="personnel_type == 1">
                                                    <option value="">Select Incardination</option>
                                                    <?php
                                                    if (!empty($incardination_types)):
                                                        foreach ($incardination_types as $ty):
                                                            ?>
                                                            <option value="<?= $ty->incardination_type_id ?>" ><?= ucfirst(trim($ty->incardination_type)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <span>Name of Diocese/Congregation/Armed forces</span>
                                            </td>
                                            <td style="width: 30%">
                                                <div ng-if="personal.congStatus !== '' && personnel_type == 1">
                                                    {{personal.congStatus}}
                                                </div>
                                                <div ng-if="personal.congStatus === '' && personnel_type != 3">
                                                    <select name="congregation_id" id="congregation_id" class="form-control">
                                                        <option ng-repeat="lg in personal.congregations" value="{{lg.id}}">
                                                            {{lg.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>    
                                        </tr>
                                        <tr ng-if="personnel_type == 2">
                                            <td>Date of Religious Profession</td>
                                            <td>
                                                <input type="text" name="dateof_reli_prof" class="datepicker" />
                                            </td>
                                            <td>
                                                <span>Name Congregation</span>
                                            </td>
                                            <td>
                                                <select required="" class="form-control select2" name="female_congregation_id" id="female_congregation_id">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($female_congregs)):
                                                        foreach ($female_congregs as $cong) :
                                                            ?>
                                                            <option value="<?= $cong->female_congregation_id; ?>"><?= $cong->congregation; ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                    <?php include '_home_parish.php'; ?>
                                    <?php include '_home_parish_sem.php'; ?>
                                    <?php include '_current_residence.php'; ?>
                                    <?php include '_emergency.php'; ?>
                                    <?php include '_health_info.php'; ?>
                                    <?php include '_ordination.php'; ?>
                                    <?php include '_honourary.php'; ?>
                                    <?php include '_education_background.php'; ?>
                                    <?php include '_current_assignment.php'; ?>
                                    <?php include '_primary_pastoral_assignment.php'; ?>
                                    <?php include '_secondary_pastoral_assignment.php'; ?>
                                    <?php include '_pastoral_assignment_sem.php'; ?>
                                    <?php include '_apostolate.php'; ?>


                                    <p>&nbsp;</p>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg pull-right" ng-disabled="personnel_type == 0">Save</button>
                                        <button type="reset" class="btn btn-warning btn-lg">Clear</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/personnel.js"></script>