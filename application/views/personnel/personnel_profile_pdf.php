<?php
//var_dump($profile); EXIT;
?>
<html>
    <head>
        <style>
            body {
                font-size: 12px;
                font-family: Garamond;
            }
        </style>
    </head>

    <body>
        <div class="">
            <table width=100% style = "border:0px solid black; border-collapse:collapse;">
                <tr>
                    <td style="width:33.3%">
                        <img src="/img/chan_logo.png" style="max-height: 120px" />
                    </td>
                    <td style="width:33.3%">&nbsp;</td>
                    <td style="width:33.3%; text-align: right">
                        <img src="<?= $profile->picture_url; ?>" style="border: 1px solid #843534; max-height: 120px; max-width: 100px" />
                    </td>
                </tr>
            </table>

            <br/>

            <div align=center>
                <strong>PERSONNEL PROFILE</strong>
            </div>

            <br/>

            <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                <tr>
                    <th style = "border:1px solid black;" colspan="2">PERSONAL INFORMATION</th>
                </tr>
                <tr>
                    <td style = "border:1px solid black; width: 50%">Title</td>
                    <td style = "border:1px solid black;"><?= $profile->title_name; ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black; width: 50%">Name</td>
                    <td style = "border:1px solid black;"><?= $profile->surname . ' ' . $profile->other_names ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Date of Birth</td>
                    <td style = "border:1px solid black;"><?= $profile->dob ? date('d-M-Y', strtotime($profile->dob)) : 'NA' ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">LGA</td>
                    <td style = "border:1px solid black;"><?= $profile->lgovt ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">State</td>
                    <td style = "border:1px solid black;"><?= $profile->state ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Nationality</td>
                    <td style = "border:1px solid black;"><?= $profile->nationality; ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Email</td>
                    <td style = "border:1px solid black;"><?= $profile->email; ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Phone Number</td>
                    <td style = "border:1px solid black;"><?= $profile->phone_number_1; ?> <?= $profile->phone_number_2; ?></td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Incardination Type:</td>
                    <td style = "border:1px solid black;"><?= $profile->incardination; ?> </td>
                </tr>
                <tr>
                    <td style = "border:1px solid black;">Name of Diocese/Congregation/Armed Forces:</td>
                    <td style = "border:1px solid black;"><?= $profile->congregation; ?> </td>
                </tr>
            </table>

            <hr style="margin-top: 10px; background-color: brown; height: 5px">

            <br/>
            <?php if (!empty($residenceInfo)): ?>
                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="2">RESIDENCE INFORMATION</th>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black; width: 50%">Place of residence</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->place_of_residence; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Address</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->address; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">City</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->city ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">State</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->state ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Region</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->region; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Deanery</td>
                        <td style = "border:1px solid black;"><?= $residenceInfo->deanery; ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <hr style="margin-top: 10px; background-color: brown; height: 5px">

            <br/>

            <?php if (!empty($homeParishInfo)): ?>
                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="2">HOME PARISH INFORMATION</th>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black; width: 50%">Parish</td>
                        <td style = "border:1px solid black;"><?= $homeParishInfo->parish; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">City</td>
                        <td style = "border:1px solid black;"><?= $homeParishInfo->city; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">State</td>
                        <td style = "border:1px solid black;"><?= $homeParishInfo->state; ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <hr style="margin-top: 10px; background-color: brown; height: 5px">

            <br/>

            <?php if (!empty($familyContantInfo)): ?>
                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="2">FAMILY CONTACT</th>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black; width: 50%">First Name</td>
                        <td style = "border:1px solid black;"><?= ucfirst($familyContantInfo->first_name); ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Last Name</td>
                        <td style = "border:1px solid black;"><?= ucfirst($familyContantInfo->last_name); ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Phone Number</td>
                        <td style = "border:1px solid black;"><?= $familyContantInfo->phone_number; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Relationship</td>
                        <td style = "border:1px solid black;"><?= $familyContantInfo->relationship; ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <?php if (!empty($healthInfo)): ?>
                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="2">HEALTH INFORMATION</th>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black; width: 50%">Genotype</td>
                        <td style = "border:1px solid black;"><?= $healthInfo->genotype; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Blood Group</td>
                        <td style = "border:1px solid black;"><?= $healthInfo->blood_group; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Health History</td>
                        <td style = "border:1px solid black;"><?= $healthInfo->health_history; ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <?php if (!empty($ordinations)): ?>

                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="4">ORDINATIONS</th>
                    </tr>
                    <tr>
                        <th style = "border:1px solid black;">Ordination Type</th>
                        <th style = "border:1px solid black;">Place</th>
                        <th style = "border:1px solid black;">Date</th>
                        <th style = "border:1px solid black;">Ordaining Prelate</th>
                    </tr>
                    <?php foreach ($ordinations as $ord) : ?>
                        <tr>
                            <td style = "border:1px solid black;"><?= $ord->ordination_type; ?></td>
                            <td style = "border:1px solid black;"><?= $ord->place; ?></td>
                            <td style = "border:1px solid black;"><?= date('d-M-Y', strtotime($ord->date)); ?></td>
                            <td style = "border:1px solid black;"><?= $ord->ordaining_prelate; ?></td>
                        </tr>
                    <?php endforeach; ?>

                </table>
            <?php endif; ?>

            <?php if (!empty($honoraryTitles)): ?>

                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="4">HONORARY TITLES</th>
                    </tr>
                    <tr>
                        <th style = "border:1px solid black;">Type</th>
                        <th style = "border:1px solid black;">Place</th>
                        <th style = "border:1px solid black;">Date</th>
                        <th style = "border:1px solid black;">Ordaining Prelate</th>
                    </tr>
                    <?php foreach ($honoraryTitles as $title) : ?>
                        <tr>
                            <td style = "border:1px solid black;"><?= $title->honourary_title; ?></td>
                            <td style = "border:1px solid black;"><?= $title->place; ?></td>
                            <td style = "border:1px solid black;"><?= date('d-M-Y', strtotime($title->date)); ?></td>
                            <td style = "border:1px solid black;"><?= $title->prelate; ?></td>
                        </tr>
                    <?php endforeach; ?>

                </table>
            <?php endif; ?>

            <?php if (!empty($profile->current_assignment)): ?>
                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="2">CURRENT ASSIGNMENT</th>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black; width: 50%">Current Assignment</td>
                        <td style = "border:1px solid black;"><?= $profile->current_assignment; ?></td>
                    </tr>
                    <tr>
                        <td style = "border:1px solid black;">Position</td>
                        <td style = "border:1px solid black;"><?= $profile->current_position; ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <?php if (!empty($pastoralAssignments)): ?>

                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="3">PASTORAL ASSIGNMENTS</th>
                    </tr>
                    <tr>
                        <th style = "border:1px solid black;">Name of Assignment</th>
                        <th style = "border:1px solid black;">Position</th>
                        <th style = "border:1px solid black;">Period</th>
                    </tr>
                    <?php foreach ($pastoralAssignments as $ass) : ?>
                        <tr>
                            <td style = "border:1px solid black;"><?= $ass->name_of_assignment; ?></td>
                            <td style = "border:1px solid black;"><?= $ass->pastoral_position; ?></td>
                            <td style = "border:1px solid black;"><?= $ass->year_from; ?> to <?= $ass->year_to; ?></td>
                        </tr>
                    <?php endforeach; ?>

                </table>
            <?php endif; ?>
                
            <?php if (!empty($admin_offices)): ?>

                <hr style="margin-top: 10px; background-color: brown; height: 5px">

                <br/>

                <table width=100% style = "border:1px solid black; border-collapse:collapse;" cellpadding="2" align="center">
                    <tr>
                        <th style = "border:1px solid black;" colspan="3">ADMINISTRATIVE OFFICES</th>
                    </tr>
                    <tr>
                        <th style = "border:1px solid black;">Name of Assignment</th>
                        <th style = "border:1px solid black;">Position</th>
                        <th style = "border:1px solid black;">Period</th>
                    </tr>
                    <?php foreach ($admin_offices as $ass) : ?>
                        <tr>
                            <td style = "border:1px solid black;"><?= $ass->name_of_secondary_assignment; ?></td>
                            <td style = "border:1px solid black;"><?= $ass->admin_office; ?></td>
                            <td style = "border:1px solid black;"><?= $ass->year_from; ?> to <?= $ass->year_to; ?></td>
                        </tr>
                    <?php endforeach; ?>

                </table>
            <?php endif; ?>


        </div>
    </body>
</html>