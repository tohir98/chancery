<fieldset ng-if="personnel_type == 2">
    <legend>
        Work(s) Assigned/Apostolate:
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="ed in passignments">
        <tr>
            <td style="width: 15%">Name of Assignment</td>
            <td style="width: 35%">
                <input required type="text" class="form-control" id="nameof_assignment_primary" name="apostolate[{{$index}}][nameof_assignment]" value="">
            </td>
            <td style="width: 20%">Position</td>
            <td>
                <input required type="text" name="apostolate[{{$index}}][position]" id="position_primary" class="form-control" />
                   
            </td>
        </tr>
        <tr>
            <td>Period Year</td>
            <td>
                From: <input required type="text" class="" name="apostolate[{{$index}}][from]" placeholder="Jan - 2017" > To 
                <input type="text" class="" name="apostolate[{{$index}}][to]" placeholder="Jun - 2017" ></td>
            <td style="width: 20%">&nbsp;</td>
            <td>
                <a href="#" onclick="return false;" ng-click="removePrimaryAssignment($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
    <a href="#" onclick="return false;" ng-click="addNewPrimaryAssignment()" class="btn btn-success btn-sm btn-flat pull-right">
        <i class="fa fa-plus-circle"></i> Add Another
    </a>
</fieldset>