<div class="modal" id="modal-edit-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Change Status</h4>
            </div>
            <form class="form-horizontal" method="post" action="<?= site_url('/personnel/chaangeStatus'); ?>">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>Status</td>
                            <td>
                                <select required name="status_id" id="status_id" class="form-control" >
                                    <option value="" selected="selected">Select Status</option>
                                    <?php
                                    if (!empty($personnel_statuses)):
                                        foreach ($personnel_statuses as $value => $status):
                                            ?>
                                            <option value="<?= $value; ?>"><?= $status; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>

                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="personnel_id" id="personnel_id" />
                    <button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat">Update</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>