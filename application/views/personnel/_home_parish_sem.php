<fieldset ng-if="personnel_type == 3 && personnel_type > 0">
    <legend>Home Parish</legend>
    <table class="table table-bordered table-condensed table-striped">
        <tr>
            <td style="width: 20%">Parish</td>
            <td>
                <input required type="text" class="form-control" id="home_parish" name="home_parish">
            </td>
            <td style="width: 20%">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>City</td>
            <td style="width: 30%">
                <input required type="text" class="form-control" id="home_parish_city" name="home_parish_city">
            </td>


            <td>State</td>
            <td>
                <select required class="form-control" id="home_parish_state_id" name="home_parish_state_id">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        foreach ($states as $state):
                            ?>
                            <option value="<?= $state->state_id ?>" ><?= ucfirst(trim($state->state)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Region</td>
            <td>
                <select required class="form-control" id="region_id" name="region_id"  ng-model="personal.region_id" ng-change="loadDeanary()">
                    <option value="">Select Region</option>
                    <?php
                    if (!empty($regions)):
                        foreach ($regions as $region):
                            ?>
                            <option value="<?= $region->region_id ?>" ><?= ucfirst(trim($region->region)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>Deanery</td>
            <td>
                <div ng-if="personal.deanaryStatus !== ''">
                    {{personal.deanaryStatus}}
                </div>
                <div ng-if="personal.deanaryStatus === ''">
                    <select name="deanery_id" id="deanery_id" class="form-control">
                        <option ng-repeat="lg in personal.deanarys" value="{{lg.id}}">
                            {{lg.name}}
                        </option>
                    </select>

                </div>
            </td>
        </tr>
    </table>
</fieldset>