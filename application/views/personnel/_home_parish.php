<fieldset ng-if="personnel_type == 1 && personnel_type > 0">
    <legend>Home Parish</legend>
    <table class="table table-bordered table-condensed table-striped">
        <tr>
            <td style="width: 20%">Parish</td>
            <td>
                <input required type="text" class="form-control" id="home_parish" name="home_parish">
            </td>
            <td style="width: 20%">City</td>
            <td>
                <input required type="text" class="form-control" id="home_parish_city" name="home_parish_city">
            </td>
        </tr>
        <tr>
            <td style="width: 20%">State</td>
            <td>
                <select required class="form-control" id="home_parish_state_id" name="home_parish_state_id">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        foreach ($states as $state):
                            ?>
                            <option value="<?= $state->state_id ?>" ><?= ucfirst(trim($state->state)) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</fieldset>