<fieldset ng-if="personnel_type == 1 ">
    <legend>
        Ordination
    </legend>
    <table class="table table-bordered table-condensed table-striped table-hover" ng-repeat="ord in ordinations">
        <tr>
            <td style="width: 20%">Type</td>
            <td style="width: 30%">
                <select name="ordination[{{$index}}][ordination_type_id]" id="ordination_type_id" class="select2 form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($ordination_types)):
                        foreach ($ordination_types as $type):
                            ?>
                            <option value="<?= $type->ordination_type_id ?>"><?= trim($type->ordination_type) ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </td>
            <td style="width: 20%">Place</td>
            <td><input required type="text" class="form-control" id="place_episcopal" name="ordination[{{$index}}][place_episcopal]"></td>
        </tr>
        <tr>
            <td>Date</td>
            <td><input required type="text" class="input-medium datepicker" id="date_episcopal" name="ordination[{{$index}}][date_episcopal]"></td>
            <td>Ordaining Prelate</td>
            <td><input required type="text" class="form-control" id="ordaining_episcopal" name="ordination[{{$index}}][ordaining_episcopal]"></td>
        </tr>
        <tr>
            <td colspan="4">
                <a href="#" onclick="return false;" ng-click="removeOrdination($index)" class="btn btn-danger btn-sm btn-flat pull-right"><i class="fa fa-trash-o"></i> Remove</a>
            </td>
        </tr>
    </table>
    <a href="#" onclick="return false;" ng-click="addNewOrdination()" class="btn btn-success btn-sm btn-flat pull-right">
        <i class="fa fa-plus-circle"></i> Add More
    </a>
</fieldset>