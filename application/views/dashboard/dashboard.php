<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= !empty($income_sources) ? count($income_sources) : 0; ?></h3>
                    <p>Income Sources</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="<?= site_url('/setup/income_sources') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= !empty($vendors) ? count($vendors) : 0; ?></h3>
                    <p>Vendors/Payees</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?= site_url('/setup/vendors') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= !empty($account_charts) ? count($account_charts) : 0; ?></h3>
                    <p>Account Charts</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?= site_url('/setup/accounts') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= !empty($users_count) ? count($users_count) : 0; ?></h3>
                    <p>Users</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?= site_url('/administration/users') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header ui-sortable-handle">

                        <h3><i class="fa fa-calendar"></i> Upcoming Birthdays</h3>
                        <div class="pull-right box-tools">

                            <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="height: auto; max-height: 350px; overflow-y: scroll">
                        <?php  if (!empty($upcoming_birthdays)) :
                        ?>
                        <table class="table table-condensed">
                                <tbody>
                                    <?php foreach ($upcoming_birthdays as $birthday) : ?>
                                    <tr>
                                        <td style="text-align: center; color: maroon">
                                            <b><?= date('M', strtotime($birthday->dob)); ?></b>
                                            <h2 style="line-height: 20px">
                                             <?= date('d', strtotime($birthday->dob)); ?></h2>
                                        </td>
                                        <td style="" align="right">
                                           
                                            <img src="<?= isset($birthday->picture_url) ? $birthday->picture_url : '/img/profile-default.jpg'; ?>" style="height:60px;width: 50px">
                                        </td>
                                        <td>
                                            <span style="font-size: 14px; font-weight: bold"><?= $birthday->surname . ' ' . $birthday->other_names ?></span> <br>
                                            <span class="badge bg-yellow" style="font-size: 12px;"> 
                                                <?= $personnel_types[$birthday->personnel_type]; ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>


        </div>


</section>