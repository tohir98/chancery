<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script>
var start_date = '<?= date('d/m/Y', strtotime(date('Y-01-01')));?>';
var end_date = '<?= date('d/m/Y', strtotime(date('Y-m-d')));?>';
</script>

<?= show_notification();
?>
<section class="content-header">
    <h1>
        Income Reports
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Income Report</li>
    </ol>
</section>

<section class="content" ng-app="report" ng-controller="incomeCtrl">
    <div class="row">
        <div class="col-md-12 well">
            <table class="table" style="margin-bottom: 0px">
                <tr>
                    <td>
                        <select ng-model="income_source" id="income_source_id" class="form-control select2">
                            <option value="0">Select Income Source</option>
                            <?php
                            if (!empty($income_sources)):
                                foreach ($income_sources as $source):
                                    ?>
                                    <option value="<?= $source->income_source_id; ?>"><?= $source->source_name;?></option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="income_account" id="income_account_id" class="form-control select2">
                            <option value="0">Select Income Account</option>
                            <?php
                            if (!empty($income_accounts)):
                                foreach ($income_accounts as $account):
                                    ?>
                                    <option value="<?= $account->account_chart_id; ?>"><?= $account->description; ?> (<?= $account->code; ?>)</option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="income_type" id="income_type_id" class="form-control select2">
                            <option value="0">Select Income Type</option>
                            <?php
                            if (!empty($income_types)):
                                foreach ($income_types as $value => $type):
                                    ?>
                                    <option value="<?= $value; ?>"><?= $type; ?></option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="cash_account" id="cash_account_id" class="form-control select2">
                            <option value="0">Select Bank/Cash Account</option>
                            <?php
                            if (!empty($bank_accounts)):
                                foreach ($bank_accounts as $account):
                                    ?>
                                    <option value="<?= $account->account_chart_id; ?>"><?= $account->description; ?> (<?= $account->code; ?>)</option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select ng-model="heading_type" id="heading_type_id" class="form-control select2">
                            <option value="0">Select Account Heading</option>
                            <?php
                            if (!empty($heading_types)):
                                foreach ($heading_types as $type):
                                    ?>
                                    <option value="<?= $type->heading_type_id; ?>"><?= $type->heading; ?> </option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td><input type="text" ng-model="start_date" class="form-control datepicker" placeholder="start date" /></td>
                    <td><input type="text" ng-model="end_date" class="form-control datepicker" placeholder="End date" /></td>
                    <td>
                        <button class="btn btn-warning btn-flat" ng-click="filterResult()">
                            <i class="fa fa-search"></i> Filter</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success btn-flat pull-right" href="#" onclick="return false;" ng-click="downloadExcel()">
                        <i class="fa fa-download"></i>
                        Download Excel
                    </a>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-condensed" cg-busy="incomePromise" ng-cloak="">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Income Type</th>
                                <th>Source</th>
                                <th>Pay Item</th>
                                <th>Amount</th>
                                <th>Memo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="income in incomes">
                                <td>{{income.income_date}}</td>
                                <td>{{income.income_type}}</td>
                                <td>{{income.income_source}}</td>
                                <td>{{income.pay_item}}</td>
                                <td>{{income.amount}}</td>
                                <td>{{income.memo}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/report.js"></script>