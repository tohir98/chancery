<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Fixed Assets Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Fixed Assets</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-flat btn-success pull-right" href="<?= site_url('/report/download_fixed_asset_xls') ?>">
                        <i class="fa fa-download"></i> Download Excel
                    </a>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($assets)):
                        ?>
                        <table class="table table-bordered table-striped table-condensed dataTable">
                            <thead>
                                <tr>
                                    <th>Asset Name</th>
                                    <th>Asset No</th>
                                    <th>Account Category</th>
                                    <th>Price</th>
                                    <th>Purchase Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($assets as $asset): ?>
                                    <tr>
                                        <td><?= $asset->asset_name ?></td>
                                        <td><?= $asset->reg_no ?></td>
                                        <td>
                                            <?= ucfirst($asset->description) ?>(<?= $asset->code; ?>)
                                        </td>
                                        <td><?= number_format($asset->purchase_price, 2); ?></td>
                                        <td><?= date('d-M-Y', strtotime($asset->purchase_date)); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No asset has been added.";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>