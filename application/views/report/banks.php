<?= show_notification();
?>
<section class="content-header">
    <h1>
        Bank Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Banks</li>
    </ol>
</section>


<section class="content">
    <div class="row-fluid">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success btn-flat pull-right" href="<?= site_url('/report/download_bank_xls');?>">
                        <i class="fa fa-download"></i>
                        Download Excel
                    </a>
                </div>
                <div class="box-body">
                    <?php if (!empty($bank_accounts)): ?>
                        <table class="table table-condensed table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>Bank</th>
                                    <th>Account Name</th>
                                    <th>Account No</th>
                                    <th>Account Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($bank_accounts as $bank):
                                    ?>
                                    <tr>
                                        <td><?= $bank->name ?></td>
                                        <td><?= $bank->account_name ?></td>
                                        <td><?= $bank->account_number ?></td>
                                        <td><?= $account_types[$bank->account_type_id] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>