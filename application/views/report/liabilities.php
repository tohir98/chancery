<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Liability Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Liabilities</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-flat btn-success pull-right" href="<?= site_url('/report/download_liability_xls') ?>">
                        <i class="fa fa-download"></i> Download Excel
                    </a>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($liabilities)):
                        ?>
                        <table class="table table-bordered table-striped table-condensed dataTable">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($liabilities as $liability): ?>
                                    <tr>
                                        <td>
                                            <?= ucfirst($liability->description) ?>(<?= $liability->code; ?>)
                                        </td>
                                        <td><?= $liability->category ?></td>
                                        <td><?= $liability->sub_category ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No liability has been added.";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>