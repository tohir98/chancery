<?= show_notification();
?>
<section class="content-header">
    <h1>
        Payroll Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Report</a></li>
        <li><a href="#">Payroll</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                
                <div class="box-body">
                    <?php if (!empty($records)): ?>
                        <table class="table table-striped table-condensed dataTable">
                            <thead>
                                <tr>
                                    <th>Pay Date</th>
                                    <th>Pay Period</th>
                                    <th>Paid Employees</th>
                                    <th>Schedule</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                    <tr>
                                        <td><?= date('d-M-Y', strtotime($record->pay_date)) ?></td>
                                        <td><?= date('d-M-Y', strtotime($record->start_date)) . ' to ' . date('d-M-Y', strtotime($record->end_date)) ?></td>
                                        <td><?= $record->emp_count; ?></td>
                                        <td><?= $record->schedule_name; ?></td>
                                        <td>Approved</td>
                                        <td>
                                            <a href="<?= site_url('/payroll/excel_download/' . $record->ref_id); ?>"> <i class="fa fa-download"></i> Download Excel</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        echo show_no_data('No record found');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>