<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs">
    <li class="<?= strstr($path, 'report/payables') ? 'active' : '' ?>">
        <a href="<?= site_url('/report/payables') ?>" aria-expanded="true">Payables</a>
    </li>
    <li class="<?= strstr($path, 'report/paid_invoices') ? 'active' : '' ?>">
        <a href="<?= site_url('/report/paid_invoices') ?>" aria-expanded="true">Paid Invoices</a>
    </li>
</ul>