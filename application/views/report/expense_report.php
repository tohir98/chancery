<script>
var start_date = '<?= date('d/m/Y', strtotime(date('Y-01-01')));?>';
var end_date = '<?= date('d/m/Y', strtotime(date('Y-m-d')));?>';
</script>

<?= show_notification();
?>
<section class="content-header">
    <h1>
        Expense Reports
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Expense Report</li>
    </ol>
</section>

<?php //var_dump($vendors); exit; ?>

<section class="content" ng-app="report" ng-controller="expenseCtrl">
    <div class="row">
        <div class="col-md-12 well">
            <table class="table" style="margin-bottom: 0px">
                <tr>
                    <td>
                        <select ng-model="payee" id="vendor_id" class="form-control select2">
                            <option value="0">Select Payee/Vendor</option>
                            <?php
                            if (!empty($vendors)):
                                foreach ($vendors as $vendor):
                                    ?>
                                    <option value="<?= $vendor->vendor_id; ?>"><?= $vendor->vendor_name;
                            ;
                                    ?></option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="expense_account" id="expense_account_id" class="form-control select2">
                            <option value="0">Select Expense Account</option>
                            <?php
                            if (!empty($expense_accounts)):
                                foreach ($expense_accounts as $account):
                                    ?>
                                    <option value="<?= $account->account_chart_id; ?>"><?= $account->description; ?> (<?= $account->code; ?>)</option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="expense_type" id="expense_type_id" class="form-control select2">
                            <option value="0">Select Expense Type</option>
                            <?php
                            if (!empty($expense_types)):
                                foreach ($expense_types as $value => $type):
                                    ?>
                                    <option value="<?= $value; ?>"><?= $type; ?></option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td>
                        <select ng-model="cash_account" id="cash_account_id" class="form-control select2">
                            <option value="0">Select Bank/Cash Account</option>
                            <?php
                            if (!empty($bank_accounts)):
                                foreach ($bank_accounts as $account):
                                    ?>
                                    <option value="<?= $account->account_chart_id; ?>"><?= $account->description; ?> (<?= $account->code; ?>)</option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select ng-model="heading_type" id="heading_type_id" class="form-control select2">
                            <option value="0">Select Account Heading</option>
                            <?php
                            if (!empty($heading_types)):
                                foreach ($heading_types as $type):
                                    ?>
                                    <option value="<?= $type->heading_type_id; ?>"><?= $type->heading; ?> </option>     
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td><input type="text" ng-model="start_date" class="form-control datepicker" placeholder="start date" /></td>
                    <td><input type="text" ng-model="end_date" class="form-control datepicker" placeholder="End date" /></td>
                    <td>
                        <button class="btn btn-warning btn-flat" ng-click="filterResult()">
                            <i class="fa fa-search"></i> Filter
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success btn-flat pull-right" href="#" onclick="return false;" ng-click="downloadExcel()">
                        <i class="fa fa-download"></i>
                        Download Excel
                    </a>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-condensed" cg-busy="incomePromise" ng-cloak="">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Expense Type</th>
                                <th>Payee</th>
                                <th>Pay Item</th>
                                <th>Amount</th>
                                <th>Memo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="expense in expenses">
                                <td>{{expense.incured_date}}</td>
                                <td>{{expense.expense_type}}</td>
                                <td>{{expense.vendor}}</td>
                                <td>{{expense.pay_item}}</td>
                                <td>{{expense.amount}}</td>
                                <td>{{expense.memo}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/report.js"></script>