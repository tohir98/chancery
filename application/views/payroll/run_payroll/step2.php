<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Run Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Run Payroll</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>Step 2. Review Employee Pay</h3>
                    </div>
                    <div class="box-body">
                        <?php 
                        if (!empty($paid_employees)): ?>
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" onClick="toggle(this)" /></th>
                                        <th>Staff Name</th>
                                        <th>Take Home Pay</th>
                                        <th>Payslip</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($paid_employees as $emp): ?>
                                        <tr>
                                            <td><input type="checkbox" class="toggle" name="employee_id[]" value="<?= $emp->employee_id; ?>" /></td>
                                            <td><?= $emp->first_name . ' ' . $emp->last_name; ?></td>
                                            <td>NGN <?= number_format($emp->grosspay, 2); ?></td>
                                            <td>
                                                <a target="_blank" href="<?= site_url('/payroll/compensation_payslip/'. $emp->employee_id); ?>">View Payslip</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php
                        else:
                            echo show_no_data('No employee found');
                        endif;
                        ?>
                    </div>
                    
                </div>
                
                <div >
                        <div class="pull-right">
                            <a href="<?= site_url('payroll/cancel_payrun/'. $this->uri->segment(4)); ?>" class="btn btn-danger  btn-flat confirm_cancel">
                                <i class="fa fa-times"></i> Cancel Payroll
                            </a>
                            <button type="submit" class="btn btn-primary btn-flat">
                                 <i class="fa fa-check"></i> Approve Payroll
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</section>

<script src="/js/payroll.js"></script>