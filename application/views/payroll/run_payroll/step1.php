<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Run Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Run Payroll</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Select Pay Period</h3>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <table class="table">
                            <tr>
                                <td>Pay Period</td>
                                <td style="width: 300px">
                                    <select required name="period_id" id="period_id" class="form-control">
                                        <option value="">Select Period</option>
                                        <?php
                                        if (!empty($upcoming_periods)):
                                            foreach ($upcoming_periods as $period):
                                                ?>
                                                <option value="<?= $period->payroll_schedule_period_id; ?>"><?= date('d-M-Y', strtotime($period->start_date)) . ' to ' . date('d-M-Y', strtotime($period->end_date)); ?></option>
                                            <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                                <td style="width: 50%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Pay date</td>
                                <td>
                                    <input required type="text" name="pay_date" id="pay_date" class="form-control datepicker" value="<?= date('d/m/Y', strtotime($upcoming_periods[0]->pay_date)); ?>" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <button type="submit" class="btn btn-primary btn-flat">Next</button>
                                    <a class="btn btn-danger btn-flat" href="<?= site_url('/payroll/run_payroll'); ?>">Cancel</a>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
