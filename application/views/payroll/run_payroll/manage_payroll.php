<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Manage Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Manage Payroll</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Payment Schedule</h3>
                </div>
                <div class="box-body">
                    <?php 
                    if(!empty($pay_schedules)): ?>
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Schedule Name</th>
                                <th>Next Pay Period</th>
                                <th>Next Pay Date</th>
                                <th>Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pay_schedules as $schedule): ?>
                            <tr>
                                <td><?= $schedule->schedule_name?></td>
                                <td><?= date('d-M-Y', strtotime($schedule->from)) . ' to ' . date('d-M-Y', strtotime($schedule->to)) ?></td>
                                <td><?= date('d-M-Y', strtotime($schedule->pay_date)); ?></td>
                                <td><?= $schedule->status?></td>
                                <td>
                                    <a href="<?= site_url('/payroll/run_pay/step1/' . $schedule->ref_id); ?>">Run Payroll</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php 
                            if (!empty($run_schedules)):
                            foreach ($run_schedules as $schedule): ?>
                            <tr>
                                <td><?= $schedule->schedule_name?></td>
                                <td><?= date('d-M-Y', strtotime($schedule->from)) . ' to ' . date('d-M-Y', strtotime($schedule->to)) ?></td>
                                <td><?= date('d-M-Y', strtotime($schedule->pay_date)); ?></td>
                                <td>Step <?= $schedule->step; ?></td>
                                <td>
                                    <a href="<?= site_url('/payroll/run_pay/step' . $schedule->step . '/'.$schedule->ref_id); ?>">
                                        Continue Process</a>
                                </td>
                            </tr>
                            <?php endforeach; endif; ?>
                        </tbody>
                    </table>
                    <?php else:
                        echo show_no_data('No schedule found');
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>