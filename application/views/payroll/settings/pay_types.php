<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Staff Compensation</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_settings_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Allowances
                                </h3>
                                <a href="#new_allowance" data-toggle="modal" class="btn btn-success btn-flat btn-sm pull-right"> Add Allowance</a>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php if (!empty($allowances)): ?>
                                    <table class="table table-bordered table-hover table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Percentage of Gross</th>
                                                <th>Date Created</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($allowances as $allowance): ?>
                                                <tr>
                                                    <td><?= $allowance->title; ?></td>
                                                    <td><?= $allowance->percentage; ?> %</td>
                                                    <td><?= date('d-M-Y', strtotime($allowance->date_added)) ?></td>                                              
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/payroll/edit_allowance/' . $allowance->ref_id) ?>" class="edit_pay_type">Edit</a></li>
                                                                <li><a href="<?= site_url('/payroll/delete_allowance/' . $allowance->ref_id) ?>" class="delete_pay_type">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No Allowance found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Deductions
                                </h3>
                                <a href="#new_deduction" data-toggle="modal" class="btn btn-success btn-flat btn-sm pull-right"> Add Deduction</a>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php if (!empty($deductions)): ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Amount</th>
                                                <th>Date Created</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($deductions as $deduction): ?>
                                                <tr>
                                                    <td><?= $deduction->deduction; ?></td>
                                                    <td><?= number_format($deduction->amount, 2); ?></td>
                                                    <td><?= date('d-M-Y', strtotime($deduction->date_added)) ?></td>                                              
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/payroll/edit_deduction/' . $deduction->ref_id) ?>" class="edit_pay_type">Edit</a></li>
                                                                <li><a href="<?= site_url('/payroll/delete_deduction/' . $deduction->ref_id) ?>" class="delete_pay_type">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No Deduction found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php include '_add_allowance.php'; ?>
<?php include '_add_deduction.php'; ?>

<!-- Edit -->
<div class="modal" id="modal_edit_pay_type">
    
</div>


<script>
    $(function () {
        $('.delete_pay_type').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this pay type';
            OaaStudy.doConfirm({
                title: 'Confirm',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_pay_type', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_pay_type').modal('show');
        $('#modal_edit_pay_type').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_pay_type').html('');
            $('#modal_edit_pay_type').html(html);
            $('#modal_edit_pay_type').modal('show').fadeIn();
        });
        return false;
    });
</script>