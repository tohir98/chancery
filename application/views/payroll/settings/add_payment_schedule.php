<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Add Payment Schedules</li>
    </ol>
</section>

<section class="content" ng-app="schedule">
    <div class="row">
        <div class="col-md-12">
            <?php include '_settings_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Add Payment Schedule
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body" ng-controller="scheduleCtrl">
                                <form method="post" name="frmSchedule" class="form-horizontal" action="" >
                                    <table class="table">
                                        <tr>
                                            <td><label class="control-label">Schedule Name</label></td>
                                            <td><input type="text" class="form-control" name="schedule_name" /></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Period</label></td>
                                            <td>
                                                <select ng-model="period_id" required class="form-control" name="period_id">
                                                    <?php
                                                    if (!empty($pay_periods)):
                                                        foreach ($pay_periods as $value => $period):
                                                            ?>
                                                            <option value="<?= $value; ?>"><?= $period; ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                Pay Period for {{periods[period_id]}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                From: <br>
                                                <input type="text" value="<?= date('Y-m-01'); ?>" name="date_from" />
                                            </td>
                                            <td>
                                                To: <br>
                                                <input type="text" value="<?= date('Y-m-t'); ?>" name="date_to" />
                                            </td>
                                            <td>
                                                Pay Date: <br>
                                                <input type="text" value="<?= date('Y-m-d', strtotime('last friday of this month')); ?>" name="pay_date" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <button type="submit" class="btn btn-flat btn-primary"> Save </button>
                                                <button type="reset" class="btn btn-flat btn-warning"> Reset </button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script src="/js/pay_schedule.js"></script>