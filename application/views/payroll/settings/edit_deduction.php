<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Add Deduction</h4>
        </div>
        <form role="form" method="post" action="<?= site_url('/payroll/edit_deduction/'.$deduction->ref_id); ?>">
            <div class="modal-body">
                <div class="form-group">
                    <label for="bank_id">Title</label>
                    <input required type="text" class="form-control" id="deduction" name="deduction" placeholder="Deduction Name" value="<?= $deduction->deduction; ?>" />
                </div>
                <div class="form-group">
                    <label for="bank_id">Amount</label>
                    <input required type="text" class="form-control" id="amount" name="amount" value="<?= $deduction->amount; ?>"  />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat btn-sm" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-flat btn-sm" >Update Deduction</button>
            </div>
        </form>
    </div>
</div>