
<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs">
    <li class="<?= strstr($path, 'payroll/settings') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/settings') ?>" aria-expanded="true">Pay Types</a>
    </li>
    <li class="<?= strstr($path, 'payroll/payment_schedule') || strstr($path, 'payroll/add_new_schedule') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/payment_schedule') ?>" aria-expanded="true">Payment Schedule</a>
    </li>
    <li class="<?= strstr($path, 'payroll/permissions') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/permissions') ?>" aria-expanded="true">Permissions</a>
    </li>
</ul>