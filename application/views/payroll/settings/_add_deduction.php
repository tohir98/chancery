<div class="modal" id="new_deduction">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Deduction</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/payroll/add_deduction'); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="bank_id">Title</label>
                        <input required type="text" class="form-control" id="deduction" name="deduction" placeholder="Deduction Name" />
                    </div>
                    <div class="form-group">
                        <label for="bank_id">Amount</label>
                        <input required type="text" class="form-control" id="amount" name="amount" placeholder="5000" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-flat btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat btn-sm" >Save Deduction</button>
                </div>
            </form>
        </div>
    </div>
</div>