<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Payment Schedules</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_settings_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Payment Schedules
                                </h3>
                                <a href="<?= site_url('/payroll/add_new_schedule'); ?>" data-toggle="modal" class="btn btn-success btn-flat btn-sm pull-right"> Add New Schedule</a>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php if (!empty($pay_schedules)): ?>
                                    <table class="table table-bordered table-hover table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>Schedule Name</th>
                                                <th>Period</th>
                                                <th>Next Pay Period</th>
                                                <th>Pay Date</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($pay_schedules as $schedule): ?>
                                                <tr>
                                                    <td><?= $schedule->schedule_name; ?></td>
                                                    <td><?= $pay_periods[$schedule->period_id]; ?> </td>
                                                    <td><?= date('d M Y', strtotime($schedule->from)) . ' to ' . date('d M Y', strtotime($schedule->to)) ?></td>
                                                    <td><?= date('d M Y', strtotime($schedule->pay_date)); ?> </td>
                                                    <td>
                                                        <a class="delete_schedule" href="<?= site_url('/payroll/delete_schedule/' . $schedule->schedule_id); ?>">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No schedule found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script src="/js/pay_schedule.js"> </script>