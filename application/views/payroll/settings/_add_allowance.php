<div class="modal" id="new_allowance">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Allowance</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/payroll/add_allowance'); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="bank_id">Title</label>
                        <input required type="text" class="form-control" id="title" name="title" placeholder="Allowance Name" />
                    </div>
                    <div class="form-group">
                        <label for="bank_id">Percentage of Gross Pay</label>
                        <input required type="text" class="form-control" id="percentage" name="percentage" placeholder="50" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-flat btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat btn-sm" >Save Allowance</button>
                </div>
            </form>
        </div>
    </div>
</div>