
<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs">
    <li class="<?= strstr($path, 'payroll/compensation') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/compensation') ?>" aria-expanded="true">Paid Staffs</a>
    </li>
    <li class="<?= strstr($path, 'payroll/unpaid_staffs') || strstr($path, 'payroll/add_compensation') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/unpaid_staffs') ?>" aria-expanded="true">Add Compensation</a>
    </li>
<!--    <li class="<?= strstr($path, 'payroll/pay_grades') ? 'active' : '' ?>">
        <a href="<?= site_url('/payroll/pay_grades') ?>" aria-expanded="true">Pay Grades</a>
    </li>-->
</ul>