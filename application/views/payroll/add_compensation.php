<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Add Compensation</li>
    </ol>
</section>

<?php
$components = $compensation ? json_decode($compensation[0]->components) : [];
$deductio = $compensation ? json_decode($compensation[0]->deductions) : [];
?>

<!-- Main content -->
<section class="content" ng-app="compensation">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title">
                                    Compensation for <?= str_replace('-', ' ', $this->uri->segment(4)); ?>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <h4 class="lead">1. Gross Regular Pay 
                                    <?php if (!empty($components)): ?>
                                    NGN <?= number_format($compensation[0]->grosspay, 2); ?> Per Month
                                    <?php endif; ?>
                                    <a href="#grosspay_modal" data-toggle="modal"><?= !empty($components) ? 'Edit' : 'Add'; ?></a></h4>
                                <br>
                                <?php if (!empty($components)): ?>
                                   
                                    <table class="table table-striped table-invoice table-condensed">
                                        <?php foreach ($components as $comp): ?>
                                            <tr>
                                                <td style="width: 500px"><?= $comp->title; ?>(<?= $comp->percentage; ?> %)</td>
                                                <td><?= number_format($comp->amount, 2); ?></td>
                                            </tr>
                                        <?php endforeach;
                                        ?>
                                    </table>
                                <?php endif; ?>

                                <h4 class="lead">2. Deductions 
                                    <a href="#deduction_modal" data-toggle="modal"><?= !empty($deductio) ? 'Edit' : 'Add'; ?></a>
                                </h4>
                                <?php if (!empty($deductio)): ?>

                                    <table class="table table-striped table-invoice table-condensed">
                                        <?php foreach ($deductio as $deduction): ?>
                                            <tr>
                                                <td style="width: 500px"><?= $deduction->title; ?></td>
                                                <td><?= number_format($deduction->amount, 2); ?></td>
                                            </tr>
                                        <?php endforeach;
                                        ?>
                                    </table>
                                <?php endif; ?>

                                <h4 class="lead">3. Payment Schedule <a href="#pay_schedule_modal" data-toggle="modal"><?= $compensation[0]->schedule_name != '' ? 'Edit' : 'Add'; ?></a>
                                </h4>   
                                <?php if ($compensation[0]->schedule_name != '') :
                                    ?>

                                    <table class="table table-striped table-invoice table-condensed">
                                        <tbody>
                                            <tr>
                                                <td style="width: 500px">Pay Frequency</td>
                                                <td><?= $pay_periods[$compensation[0]->period_id] ?></td>
                                            </tr>
                                            <tr>
                                                <td>Next Pay Period</td>
                                                <td><?= date('d-M-Y', strtotime($schedule_period->start_date)) . ' to ' . date('d-M-Y', strtotime($schedule_period->end_date)); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Next Pay Date</td>
                                                <td><?= date('d-M-Y', strtotime($schedule_period->pay_date)); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                <?php endif; ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>

    <?php include 'compensation/_grosspay.php'; ?>
    <?php include 'compensation/_deduction.php'; ?>
    <?php include 'compensation/_payment_schedule.php'; ?>

</section>


<script src="/js/compensation.js"></script>