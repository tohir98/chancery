<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Staff Compensation</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Pay Grades
                                </h3>
                                <a href="#new_paygrade" data-toggle="modal" class="btn btn-success btn-flat btn-sm pull-right"> Create Pay Grade</a>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php if (!empty($pay_grades)): ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Pay Grade</th>
                                                <th>No. of Staff(s)</th>
                                                <th>Status</th>
                                                <th>Date Created</th>
                                                <th>Last Updated</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($pay_grades as $pay_grade): ?>
                                                <tr>
                                                    <td><?= $pay_grade->pay_grade; ?></td>
                                                    <td>0</td>
                                                    <td><?= $statuses[$pay_grade->status]; ?></td>
                                                    <td><?= date('d-M-Y', strtotime($pay_grade->date_created)) ?></td>
                                                    <td><?= $pay_grade->last_updated ? date('d-M-Y', strtotime($pay_grade->last_updated)) : '' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/payroll/edit_paygrade/' . $pay_grade->ref_id) ?>" class="edit_account">Edit</a></li>
                                                                <li><a href="<?= site_url('/payroll/view_compensation/' . $pay_grade->ref_id) ?>" class="edit_account">View Compensation</a></li>
                                                                <li><a href="<?= site_url('/payroll/delete_paygrade/' . $pay_grade->ref_id) ?>" class="deleteGrade">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No paygrade found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php include '_add_paygrade.php'; ?>


<script>
    $(function () {
        $('.deleteGrade').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this pay grade';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>