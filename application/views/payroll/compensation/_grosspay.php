<div class="modal" id="grosspay_modal" ng-controller="grosspayCtrl" ng-init='comp.allowances =<?= json_encode($allowances)?>'>
    <div class="modal-dialog" style="width: 65%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Gross Pay Components</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/payroll/add_grosspay/' . $this->uri->segment(3)  . '/' . $this->uri->segment(4)) ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="bank_id">Gross Regular Pay:</label>
                        <input required type="text"  id="grosspay" name="grosspay" ng-model="comp.grosspay" /> Per Month
                    </div>
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Component</th>
                                <th>Percentage</th>
                                <th>Amount</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="allowance in comp.allowances">
                                <td>{{allowance.title}}
                                    <input type="hidden" value="{{allowance.title}}" name="allowance[{{$index}}][title]" />
                                </td>
                                <td><input type="text" class="input-mini" name="allowance[{{$index}}][percentage]" ng-model="allowance.percentage" /></td>
                                <td>
                                    <input type="text" name="allowance[{{$index}}][amount]" ng-model="allowance.percentage / 100 * comp.grosspay"  />
                                </td>
                                <td>
                                    <a href="#" title="Remove allowance" ng-click="removeAllowance($index)">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr ng-repeat="component in comp.components">
                                <td><input type="text" name="components[{{$index}}][title]" /></td>
                                <td><input type="text" class="input-mini" name="components[{{$index}}][percentage]" ng-model="component.percentage" /></td>
                                <td>
                                    <input type="text" name="components[{{$index}}][amount]" ng-model="component.percentage / 100 * comp.grosspay"  />
                                </td>
                                <td>
                                    <a href="#" title="Remove allowance" ng-click="removeComponent($index)">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <a href="#" onclick='return false;' ng-click="addNewComponent()" class="btn btn-success btn-flat btn-sm">Add New Allowance</a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th>{{getTotalPercent()}} %</th>
                                <th>{{getTotalAmount()| currency:'NGN'}} </th>
                                <th>Per Month</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">Cancel</button>
                    <button ng-disabled="getTotalAmount() != comp.grosspay" type="submit" class="btn btn-primary btn-flat" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>