<div class="modal" id="pay_schedule_modal" ng-controller="">
    <div class="modal-dialog" style="width: 65%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Available Payment Schedule</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/payroll/add_payment_schedule/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)) ?>">
                <div class="modal-body">
                    <?php if(!empty($pay_schedules)): ?>
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Schedule Name</th>
                                <th>Period</th>
                                <th>Next Pay Period</th>
                                <th>Pay Date</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pay_schedules as $schedule): ?>
                                <tr>
                                    <td><?= $schedule->schedule_name; ?></td>
                                    <td><?= $pay_periods[$schedule->period_id]; ?> </td>
                                    <td><?= date('d M Y', strtotime($schedule->from)) . ' to ' . date('d M Y', strtotime($schedule->to)) ?></td>
                                    <td><?= date('d M Y', strtotime($schedule->pay_date)); ?> </td>
                                    <td>
                                        <input type="radio" data-val="<?= $schedule->schedule_id ?>" class='icheck-me chekin' name="schedule_id" value="<?= $schedule->schedule_id ?>"  data-skin="square" data-color="blue" <?php if ($schedule->schedule_id == $compensation[0]->schedule_id || $this->input->get('selectedSchedule') == $schedule->schedule_id): ?> checked <?php endif; ?>>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>