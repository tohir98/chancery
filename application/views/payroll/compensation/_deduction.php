<div class="modal" id="deduction_modal" ng-controller="deductionCtrl" ng-init='comp.deductions =<?= json_encode($deductions)?>'>
    <div class="modal-dialog" style="width: 65%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Deduction</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/payroll/add_deductions/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)) ?>">
                <div class="modal-body">
                    
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Deduction</th>
                                <th>Amount</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="deduction in comp.deductions">
                                <td>{{deduction.deduction}}
                                    <input type="hidden" value="{{deduction.deduction}}" name="deduction[{{$index}}][title]" />
                                </td>
                                <td><input type="text" class="input-mini" name="deduction[{{$index}}][amount]" ng-model="deduction.amount" /></td>
                                <td>
                                    <a href="#" title="Remove allowance" ng-click="removeDeduction($index)">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr ng-repeat="component in comp.components">
                                <td><input type="text" name="components[{{$index}}][title]" /></td>
                                <td>
                                    <input type="text" name="components[{{$index}}][amount]" ng-model="component.amount"  />
                                </td>
                                <td>
                                    <a href="#" title="Remove allowance" ng-click="removeComponent($index)">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <a href="#" onclick='return false;' ng-click="addNewComponent()" class="btn btn-success btn-flat">Add New Deduction</a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th>{{getTotalDeduction()| currency:'NGN'}} </th>
                                <th>&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>