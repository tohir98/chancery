<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Staff Compensation</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    Add Compensation
                                </h3>
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        Bulk Action <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?= site_url('/payroll/add_compensation/') ?>" class="">Add Compensation</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php if (!empty($employees)): ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Status</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($employees as $emp): ?>
                                                <tr>
                                                    <td><?= ucfirst($emp->first_name . ' ' . $emp->last_name); ?></td>
                                                    <td><?= $emp->position; ?></td>
                                                    <td><?= $statuses[$emp->status]; ?></td>
                                                    <td>
                                                        <a href="<?= site_url("/payroll/add_compensation/{$emp->employee_id}/{$emp->first_name}-{$emp->last_name}") ?>" title="Add Staff to compensation">Add Compensation</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No staff found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php include '_add_paygrade.php'; ?>


<script>
    $(function () {
        $('.deleteGrade').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this pay grade';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>