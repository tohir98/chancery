<?= show_notification();
?>
<section class="content-header">
    <h1>
        Payroll Records
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Records</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Records</h3>
                </div>
                <div class="box-body">
                    <?php if (!empty($records)): ?>
                        <table class="table table-striped table-condensed dataTable">
                            <thead>
                                <tr>
                                    <th>Pay Date</th>
                                    <th>Pay Period</th>
                                    <th>Paid Employees</th>
                                    <th>Schedule</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                    <tr>
                                        <td><?= date('d-M-Y', strtotime($record->pay_date)) ?></td>
                                        <td><?= date('d-M-Y', strtotime($record->start_date)) . ' to ' . date('d-M-Y', strtotime($record->end_date)) ?></td>
                                        <td><?= $record->emp_count; ?></td>
                                        <td><?= $record->schedule_name; ?></td>
                                        <td>Approved</td>
                                        <td>
                                            <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li>
                                                                    <a href="<?= site_url('/payroll/view_record/'. $record->ref_id); ?>" >View Details</a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= site_url('/payroll/excel_download/'.$record->ref_id); ?>">Download Excel</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php
                    else:
                        echo show_no_data('No record found');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>