<?= show_notification();
?>
<section class="content-header">
    <a class="btn btn-warning btn-sm btn-flat" href="<?= site_url('/payroll/pay_records'); ?>">
        <i class="fa fa-chevron-left"></i> Back
    </a>
    <h1>
        <?= $schedule->schedule_name; ?> : <?= date('d-M-Y', strtotime($schedule->start_date)); ?> To <?= date('d-M-Y', strtotime($schedule->end_date)); ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Records</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>History</h3>
                </div>
                <div class="box-body">
                    <?php if (!empty($records)):
                        ?>
                        <table class="table table-striped table-condensed dataTable">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Currency</th>
                                    <th>Gross Pay</th>
                                    <th>Take Home Pay</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record):
                                    ?>
                                    <tr>
                                        <td><?= ucfirst($record->first_name) . ' ' . ucfirst($record->last_name); ?></td>
                                        <td>NGN</td>
                                        <td><?= number_format($record->grosspay, 2); ?></td>
                                        <td><?= number_format($record->netpay, 2); ?></td>
                                        <td>Approved</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="<?= site_url('/payroll/view_payslip/' . $record->payroll_run_ref . '/' . $record->employee_id    ); ?>" >View Payslip</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
    <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        echo show_no_data('No record found');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>