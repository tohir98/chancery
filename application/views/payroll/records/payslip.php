<section class="content-header" style="float: right">
    <a href="<?= $this->input->server('HTTP_REFERER'); ?>" class="btn btn-warning btn-sm btn-flat">
        <i class="fa fa-chevron-left"></i> Back
    </a>

    <a class="btn btn-success btn-sm btn-flat" target="_blank" href="#">
        <i class="fa fa-print"></i> Print PDF
    </a>
</section>
<?php
$earnings = json_decode($records->allowances);
$deductions = json_decode($records->deductions);
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div style="text-align:center;">
                <h3><?= BUSINESS_NAME ?></h3>
                <h5> Payslip </h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-condensed table-bordered">
                                <tr>
                                    <td>Employment Date</td>
                                    <td><?= date('d-M-Y', strtotime($records->employment_date)); ?></td>
                                </tr>
                                <tr>
                                    <td>Payment Method</td>
                                    <td>Bank Transfer</td>
                                </tr>
                                <tr>
                                    <td>Payment Period</td>
                                    <td><?= date('d-M-Y', strtotime($schedule->start_date)) . ' To ' . date('d-M-Y', strtotime($schedule->end_date)); ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-condensed table-bordered">
                                <tr>
                                    <th>Staff Name</th>
                                    <th><?= ucfirst($records->first_name) . ' ' . ucfirst($records->last_name); ?></th>
                                </tr>
                                <tr>
                                    <td>Bank Account</td>
                                    <td><?= $records->bank_name; ?> (<?= $records->account_no; ?>)</td>
                                </tr>
                                <tr>
                                    <td>Currency</td>
                                    <td>NGN</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- Earnings/Deductions -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="table table-condensed table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Earnings</th>
                                                    <th style="text-align:right;">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($earnings)):
                                                    $grosspay = 0;
                                                    foreach ($earnings as $earning):
                                                        $grosspay += $earning->amount;
                                                        ?>
                                                        <tr>
                                                            <td><?= $earning->title; ?></td>
                                                            <td style="text-align:right;"><?= number_format($earning->amount, 2); ?></td>
                                                        </tr>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="baseline">
                                        <table class="table table-condensed table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Deductions</th>
                                                    <th style="text-align:right;">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($deductions)):
                                                    $total_deduction = 0;
                                                    foreach ($deductions as $deduction):
                                                    $total_deduction += $deduction->amount;
                                                        ?>
                                                        <tr>
                                                            <td><?= $deduction->title; ?></td>
                                                            <td style="text-align:right;"><?= number_format($deduction->amount, 2); ?></td>
                                                        </tr>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                                <tr>
                                                    <th>Total Deduction</th>
                                                    <th style="text-align:right;"><?= number_format($total_deduction, 2); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Gross Pay
                                        <span style="float: right"><?= number_format($grosspay, 2); ?></span>
                                    </th> 
                                    <th>Net Pay
                                        <span style="float: right"><?= number_format(($grosspay - $total_deduction), 2); ?></span>
                                    </th> 
                                    
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
