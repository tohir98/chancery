<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Payroll
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Compensation</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">

                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                
                                if (!empty($paid_employees)): ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Annual Take Home Pay</th>
                                                <th>Monthly Take Home Pay</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($paid_employees as $emp): ?>
                                                <tr>
                                                    <td>
                                                        <?= ucwords($emp->first_name . ' ' . $emp->last_name); ?>
                                                    </td>
                                                    <td><?= number_format(($emp->grosspay * 12) , 2); ?></td>
                                                    <td><?= number_format($emp->grosspay , 2); ?></td>
                                                    <td>
                                                        <a href="<?= site_url('/payroll/compensation_payslip/' . $emp->employee_id); ?>">View Payslip</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('Noting found');
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this account';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>