<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Transaction
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Transaction</a></li>
        <li class="active">expenses</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/transaction/_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                            Add Expense <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="<?= site_url('/transaction/expenses/add_payable') ?>">Invoices/Payables</a></li>
                                            
                        <li><a href="<?= site_url('/transaction/expenses/add_disbursement') ?>">Disbursements</a></li>
                        <li><a href="<?= site_url('transaction/expenses/add_paid_invoices');?>">Paid Invoices</a></li>
                                        </ul>
                                    </div>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if (!empty($expenses)):
                                    ?>
                                    <table id="example1" class="table table-striped table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Reference</th>
                                                <th>Vendor</th>
                                                <th>Amount</th>
                                                <th>Memo</th>
                                                <th>Recurring Period</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($expenses as $exp): ?>
                                                <tr>
                                                    <td><?= date('d M Y', strtotime($exp->incured_date)); ?></td>
                                                    <td><?= $expense_types[$exp->expense_type_id] ?></td>
                                                    <td><?= $exp->ref_id ?></td>
                                                    <td><?= $exp->vendor_name ?></td>
                                                    <td><?= number_format($exp->amount, 2) ?></td>
                                                    <td><?= $exp->memo ?></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No expenditure has been added.";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this account';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>