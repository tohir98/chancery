<tr>
    <td>
        <div class="form-group">
            <label for="quarter_id">Quarter</label>
            <select required name="quarter_id" id="quarter_id" class="form-control" style="width: 200px">
                <option value="" selected>Select </option>
                <?php foreach ($quarters as $id => $value): ?>
                    <option value="<?= $id; ?>"><?= $value; ?></option>
                <?php endforeach; ?>
            </select>
            <br>
            <div id="div_month">
                <select class="form-control" style="width: 200px">
                    <option value="0" selected>Select quarter first </option>
                </select>
            </div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <label for="year">Year</label>
            <select name="year" id="year" class="form-control">
                <option>Select</option>
                <?php for ($year = 2014; $year <= date('Y'); $year++): ?>
                    <option value="<?= $year; ?>"><?= $year; ?></option>
                <?php endfor; ?>
            </select>
        </div>
    </td>
</tr>
<tr>
    <td>
        <div class="form-group">
            <label for="date_acquired">Date</label>
            <input required type="text" class="form-control datepicker" id="incured_date" name="incured_date" placeholder="Date" value="<?= isset($incured_date) ? $incured_date : date('d/m/Y'); ?>" style="cursor: pointer">
        </div>
    </td>
    <td>
        &nbsp;
    </td>
</tr>