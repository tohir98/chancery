<?php // var_dump($income_accounts);  ?>
<table class="table col-md-8">
    <tr>
        <td>
            <div class="form-group">
                <label for="quarter_id">Quarter</label>
                <select required name="quarter_id" id="quarter_id" class="form-control" style="width: 200px">
                    <option value="0" selected>Select </option>
                    <?php foreach ($quarters as $id => $value): ?>
                        <option value="<?= $id; ?>"><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <br>
                <div id="div_month">
                    <select class="form-control" style="width: 200px">
                        <option value="0" selected>Select quarter first </option>
                    </select>
                </div>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="year">Year</label>
                <select name="year" id="year" class="form-control">
                    <option>Select</option>
                    <?php for ($year = 2014; $year <= date('Y'); $year++): ?>
                        <option value="<?= $year; ?>"><?= $year; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="date_acquired">Date</label>
                <input required type="text" class="form-control" id="incured_date" name="incured_date" placeholder="Date" value="<?= isset($incured_date) ? $incured_date : ''; ?>" style="cursor: pointer">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="expense_type_id">Expense Type</label>
                <select required name="expense_type_id" id="expense_type_id" class="form-control">
                    <option value="" selected>Select Type</option>
                    <?php
                    if (!empty($l_accounts)):
                        foreach ($l_accounts as $type):
                            ?>
                            <option value="<?= $type->account_chart_id; ?>"><?= ucfirst($type->description); ?> (<?= $type->code; ?>) </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="vendor_id">Payee/Vendor ID</label>
                <select required name="vendor_id" id="vendor_id" class="form-control">
                    <option value="" selected>Select Vendor</option>
                    <?php
                    if (!empty($vendors)):
                        foreach ($vendors as $vendor):
                            ?>
                            <option value="<?= $vendor->vendor_id; ?>"><?= ucfirst($vendor->vendor_name); ?> </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Amount</label>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?= isset($amount) ? $amount : ''; ?>">
            </div>
        </td>
        
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="memo">Memo</label>
                <input required type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="asset_name">Due Date</label>
                <input type="text" name="due_date" id="due_date" value="" class="form-control datepicker" value="<?= isset($due_date) ? $due_date : ''; ?>" style="cursor: pointer" />
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="form-group">
                <label for="ap_account_id">AP Account</label>
                <select required name="ap_account_id" id="ap_account_id" class="form-control">
                    <option value="" selected>Select AP Account</option>
                    <?php
                    if (!empty($ap_accounts)):
                        foreach ($ap_accounts as $ap):
                            ?>
                            <option value="<?= $ap->account_chart_id; ?>"><?= ucfirst($ap->description); ?> (<?= $ap->code; ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="discount">Discount</label>
                <input type="text" name="discount" id="discount" class="form-control" />
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="bank_account_id">Expense Category</label>
                <select required name="expense_category_id" id="expense_category_id" class="form-control">
                    <option value="" selected>Select Expense Category</option>
                    <?php
                    if (!empty($expense_categories)):
                        foreach ($expense_categories as $category):
                            ?>
                            <option value="<?= $category->account_chart_id; ?>"><?= $category->description; ?> (<?= $category->code; ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="cash_account_id">Source Cash Account</label>
                <select required name="cash_account_id" ng-model="cash_account_id" id="cash_account_id" class="form-control" ng-change="getBankBalance()">
                    <option value="" selected>Select Account</option>
                    <?php
                    if (!empty($cash_accts)):
                        foreach ($cash_accts as $acct):
                            ?>
                            <option value="<?= $acct->account_chart_id; ?>"><?= ucfirst($acct->description); ?> (<?= $acct->code ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
    </tr>
</table>
