<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Transaction
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Transaction</a></li>
        <li class="active">Expense</li>
    </ol>
</section>

<section class="content" ng-app="expense" ng-controller="expenseCtrl">
    <div class="row">
        <?php include APPPATH . 'views/transaction/_tab.php'; ?>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">

                    <h3 class="box-title">
                        <?= $page_title; ?>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form role="form" method="post" action="">
                        <?php
                        if ($type == EXPENSE_PAYABLE):
                            include '_payable_form.php';
                        elseif ($type == EXPENSE_DISBURSEMENT):
                            include '_disbursement_form.php';
                        elseif ($type == EXPENSE_PAID_INVOICES):
                            include '_paid_invoice_form.php';
                        endif;
                        ?>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-flat" ><?php
                                if ($type == EXPENSE_PAYABLE):
                                    echo 'Add Payable';
                                elseif ($type == EXPENSE_DISBURSEMENT):
                                    echo 'Add Disbursement';
                                elseif ($type == EXPENSE_PAID_INVOICES):
                                    echo 'Add Paid Invoices';
                                endif;
                                ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Related Account Info.
                    </h3>
                </div>
                <div class="box-body">
                    <div ng-cloak="">
                        <?php if ($type == EXPENSE_PAYABLE): ?>
                            <h5> AP :<b> {{ap_info.description}} </b> </h5>
                            <h5> Balance : <b> {{ap_info.balance| number:2}} </b></h5>
                        <?php elseif ($type == EXPENSE_PAID_INVOICES): ?>
                            <h5> AP Balance :<b> {{ap_info.description}}({{ap_info.balance| number:2}}) </b> </h5>
                            <h5> Bank Account :<b> {{bank.description}} </b> </h5>
                            <h5> Balance : <b> {{bank.balance| number:2}} </b></h5>
                        <?php elseif ($type == EXPENSE_DISBURSEMENT): ?>
                            <h5> Expense Account :<b> {{bank.description}} </b> </h5>
                            <h5> Balance : <b> {{bank.balance| number:2}} </b></h5>
                        <?php endif;
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script src="/js/expense.js"></script>
<script>
                            $('document').ready(function () {

                                $('#quarter_id').change(function () {
                                    var quarter_id_ = $("#quarter_id :selected").val();


                                    $.ajax({
                                        url: '<?= site_url('transaction/transaction_controller/load_month/') ?>/' + quarter_id_,
                                        type: 'GET',
                                        success: function (mth) {
                                            $('#div_month').html(mth);
                                            $('#month_id').addClass('form-control');
                                        }
                                    });

                                });

                            });
</script>


