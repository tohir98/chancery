<table class="table col-md-8">
    <?php include '_period.php'; ?>
    <tr>
        <td>
            <div class="form-group">
                <label for="vendor_id">Payee/Vendor</label>
                <select required name="vendor_id" id="vendor_id" class="form-control" ng-model="vendor_id" ng-change="getAPBalance()">
                    <option value="" selected>Select Vendor</option>
                    <?php
                    if (!empty($vendors)):
                        foreach ($vendors as $vendor):
                            ?>
                            <option value="<?= $vendor->vendor_id; ?>"><?= ucfirst($vendor->vendor_name); ?> </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Amount</label>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" ng-model="amount" >
            </div>
        </td>
        
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="ap_account_id">AP Account</label>
                <select required name="ap_account_id" id="ap_account_id" class="form-control" ng-model="ap_account_id" ng-change="getAPBalance()">
                    <option value="" selected>Select AP Account</option>
                    <?php
                    if (!empty($ap_accounts)):
                        foreach ($ap_accounts as $ap):
                            ?>
                            <option value="<?= $ap->account_chart_id; ?>"><?= ucfirst($ap->description); ?> (<?= $ap->code; ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
           <div class="form-group">
                <label for="cash_account_id">Cash Account</label>
                <select required name="cash_account_id" ng-model="cash_account_id" id="cash_account_id" class="form-control" ng-change="getBankBalance()">
                    <option value="" selected>Select Account</option>
                    <?php
                    if (!empty($cash_accts)):
                        foreach ($cash_accts as $acct):
                            ?>
                            <option value="<?= $acct->account_chart_id; ?>"><?= ucfirst($acct->description); ?> (<?= $acct->code ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="memo">Memo</label>
                <input type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
