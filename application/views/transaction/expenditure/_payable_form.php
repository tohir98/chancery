<table class="table col-md-8">
    <?php include '_period.php'; ?>
    <tr>
        <td>
            <div class="form-group">
                <label for="vendor_id">Payee/Vendor</label>
                <select required name="vendor_id" id="vendor_id" ng-model="vendor_id" class="form-control" ng-change="getAPBalance()">
                    <option value="" selected>Select Vendor</option>
                    <?php
                    if (!empty($vendors)):
                        foreach ($vendors as $vendor):
                            ?>
                            <option value="<?= $vendor->vendor_id; ?>"><?= ucfirst($vendor->vendor_name); ?> </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Amount</label>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?= isset($amount) ? $amount : ''; ?>">
            </div>
        </td>
        
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="ap_account_id">AP Account</label>
                <select required name="ap_account_id" id="ap_account_id" class="form-control" ng-model="ap_account_id" ng-change="getAPBalance()">
                    <option value="" selected>Select AP Account</option>
                    <?php
                    if (!empty($ap_accounts)):
                        foreach ($ap_accounts as $ap):
                            ?>
                            <option value="<?= $ap->account_chart_id; ?>"><?= ucfirst($ap->description); ?> (<?= $ap->code; ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="asset_name">Due Date</label>
                <input type="text" name="due_date" id="due_date" value="" class="form-control datepicker" value="<?= isset($due_date) ? $due_date : ''; ?>" style="cursor: pointer" />
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="form-group">
                <label for="memo">Memo</label>
                <input type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="discount">Discount</label>
                <input type="text" name="discount" id="discount" class="form-control" />
            </div>
        </td>
    </tr>
</table>
