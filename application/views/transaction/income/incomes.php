<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Transaction
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Transaction</a></li>
        <li class="active">Income</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/transaction/_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                        Add Income <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?= site_url('transaction/incomes/add_receivable') ?>" class="edit_vendor">Receivable</a></li>
                                        <li><a href="<?= site_url('/transaction/incomes/add_cash_receipts') ?>" class="deleteVendor">Cash Receipts</a></li>
                                        <li><a href="<?= site_url('/transaction/incomes/add_deposit') ?>" class="deleteVendor">Paid Receivables</a></li>
                                    </ul>
                                </div>
                                
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if (!empty($incomes)):
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Account Type/Code</th>
                                                <th>Source</th>
                                                <th>Payment Item</th>
                                                <th>Amount</th>
                                                <th>Memo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($incomes as $income): ?>
                                                <tr>
                                                    <td><?= date('d-M-Y', strtotime($income->income_date)) ?></td>
                                                    <td><?= $income_types[$income->income_type_id] ?> (<?= $income->code ?>)</td>
                                                    <td><?= $income->source_name ?></td>
                                                    <td><?= ucwords($income->description); ?></td>
                                                    <td><?= number_format($income->amount, 2) ?></td>
                                                    <td><?= $income->memo ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No income has been added.";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this account';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>