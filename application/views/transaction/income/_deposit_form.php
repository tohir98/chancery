<table class="table table-condensed">
    <tr>
        <td>
            <div class="form-group">
                <label for="quarter_id">Quarter</label>
                <select required name="quarter_id" id="quarter_id" class="form-control" style="width: 200px">
                    <option value="0" selected>Select </option>
                    <?php foreach ($quarters as $id => $value): ?>
                        <option value="<?= $id; ?>"><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <div id="div_month">
                    <select class="form-control" style="width: 200px">
                        <option value="0" selected>Select quarter first </option>
                    </select>
                </div>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="year">Year</label>
                <select name="year" id="year" class="form-control">
                    <option>Select</option>
                    <?php for ($year = 2014; $year <= date('Y'); $year++): ?>
                        <option value="<?= $year; ?>"><?= $year; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="income_date">Date</label>
                <input required type="text" class="form-control" id="income_date" name="income_date" placeholder="Date" value="<?= isset($income_date) ? $income_date : date('m/d/Y'); ?>" style="cursor: pointer">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="memo">Memo</label>
                <input required type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="income_source_id">Income Source</label>
                <select required name="income_source_id" ng-model="income_source_id" class="form-control" ng-change="getARBalance()">
                    <option value="" selected>Select Source</option>
                    <?php
                    if (!empty($income_sources)):
                        foreach ($income_sources as $source):
                            ?>
                            <option value="<?= $source->income_source_id; ?>"><?= ucfirst($source->source_name); ?> </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Amount</label>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" ng-model="amount" ng-change="validate()">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="income_source_id">AR Account</label>
                <select required name="account_chart_id" ng-model="account_chart_id" class="form-control" ng-change="getARBalance()">
                    <option value=''>Select Account</option>
                    <?php
                    if (!empty($accounts)):
                        foreach ($accounts as $source):
                            ?>
                            <option value="<?= $source->account_chart_id; ?>"><?= ucfirst($source->description); ?> (<?= $source->code ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>

        <td>
            <div class="form-group">
                <label for="cash_account_id">Cash Account</label>
                <select required name="cash_account_id" ng-model="cash_account_id" id="cash_account_id" class="form-control" ng-change="getBankBalance()">
                    <option value="" selected>Select Account</option>
                    <?php
                    if (!empty($cash_accts)):
                        foreach ($cash_accts as $acct):
                            ?>
                            <option value="<?= $acct->account_chart_id; ?>"><?= ucfirst($acct->description); ?> (<?= $acct->code ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>

        </td>

    </tr>
</table>
