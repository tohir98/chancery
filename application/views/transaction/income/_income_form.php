<?php // var_dump($income_accounts);           ?>
<table class="table table-condensed">
    <tr>
        <td>
            <div class="form-group">
                <label for="quarter_id">Quarter</label>
                <select required name="quarter_id" id="quarter_id" class="form-control" style="width: 200px">
                    <option value="0" selected>Select </option>
                    <?php foreach ($quarters as $id => $value): ?>
                        <option value="<?= $id; ?>"><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <br>
                <div id="div_month">
                    <select class="form-control" style="width: 200px">
                        <option value="0" selected>Select quarter first </option>
                    </select>
                </div>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="year">Year</label>
                <select name="year" id="year" class="form-control">
                    <option>Select</option>
                    <?php for ($year = 2014; $year <= date('Y'); $year++): ?>
                        <option value="<?= $year; ?>"><?= $year; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="income_date">Date</label>
                <input type="text" class="form-control" id="income_date" name="income_date" placeholder="Date" value="<?= isset($income_date) ? $income_date : date('m/d/Y'); ?>" style="cursor: pointer">
            </div>
        </td>
        <td>
            <?php if ($income_type == INCOME_CASH_RECEIPT): ?>
                <?php if (!in_array('memo', $to_hide)): ?>

                    <div class="form-group">
                        <label for="memo">Memo</label>
                        <input required type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
                    </div>

                <?php endif;
            endif; ?>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="income_source_id">Income Source</label>
                <select required name="income_source_id" id="income_source_id" ng-model="income_source_id" class="form-control" ng-change="getCashBalance()">
                    <option value="" selected>Select Source</option>
                    <?php
                    if (!empty($income_sources)):
                        foreach ($income_sources as $source):
                            ?>
                            <option value="<?= $source->income_source_id; ?>"><?= ucfirst($source->source_name); ?> </option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Amount</label>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?= isset($amount) ? $amount : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <?php if ($income_type == INCOME_RECEIVABLE || $income_type == INCOME_DEPOSIT): ?>
                    <label for="income_source_id">AR Account</label>
                <?php elseif ($income_type == INCOME_CASH_RECEIPT): ?>
                    <label for="income_source_id">Income Account</label>
<?php endif; ?>
                <select required name="account_chart_id" ng-model="account_chart_id" id="account_chart_id" class="form-control" <?php if ($income_type == INCOME_RECEIVABLE) : ?> ng-change="getARBalance()" <?php endif; ?> <?php if ($income_type == INCOME_CASH_RECEIPT) : ?> ng-change="getCashBalance()" <?php endif; ?>>
                    <option value="" selected>Select Account</option>
                    <?php
                    if (!empty($accounts)):
                        foreach ($accounts as $source):
                            ?>
                            <option value="<?= $source->account_chart_id; ?>"><?= ucfirst($source->description); ?> (<?= $source->code ?>)</option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>

        <td>
<?php if ($income_type == INCOME_RECEIVABLE): ?>
                <div class="form-group">
                    <label for="due_date">Due Date</label>
                    <input required type="text" class="form-control" id="due_date" name="due_date" placeholder="Date" value="<?= isset($income_date) ? $income_date : ''; ?>" style="cursor: pointer">
                </div>

<?php elseif ($income_type == INCOME_CASH_RECEIPT): ?>
                <div class="form-group">
                    <label for="cash_account_id">Cash Account</label>
                    <select required name="cash_account_id" id="cash_account_id" ng-model="cash_account_id" class="form-control" ng-change="getBankBalance()">
                        <option value="" selected>Select Account</option>
                        <?php
                        if (!empty($cash_accts)):
                            foreach ($cash_accts as $acct):
                                ?>
                                <option value="<?= $acct->account_chart_id; ?>"><?= ucfirst($acct->description); ?> (<?= $acct->code ?>)</option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
<?php endif; ?>

        </td>

    </tr>

        <?php if ($income_type == INCOME_RECEIVABLE || $income_type == INCOME_DEPOSIT) : ?>
        <tr>
    <?php if (!in_array('memo', $to_hide)): ?>
                <td>
                    <div class="form-group">
                        <label for="memo">Memo</label>
                        <input required type="text" class="form-control" id="memo" name="memo" placeholder="memo" value="<?= isset($memo) ? $memo : ''; ?>">
                    </div>
                </td>
                <?php endif; ?>
            <td>
                <?php if (in_array('bank', $to_hide)): ?>
                    &nbsp;
    <?php else: ?>
                    <div class="form-group">
                        <label for="bank_account_id">Bank</label>
                        <select required name="bank_account_id" id="bank_account_id" class="form-control">
                            <option value="" selected>Select Bank</option>
                            <?php
                            if (!empty($bank_accounts)):
                                foreach ($bank_accounts as $bank):
                                    ?>
                                    <option value="<?= $bank->bank_account_id; ?>"><?= $bank->bank_name; ?> (<?= $bank->account_number; ?>)</option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
    <?php endif; ?>

            </td>
        </tr>
<?php endif; ?>

</table>

<div class="modal" id="modal-new_user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Ajax Error</h4>
            </div>
            <div class="modal-body">
                <p>A problem was encountered while fetching balance, pls try again</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
