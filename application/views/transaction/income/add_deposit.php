<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Transaction
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Transaction</a></li>
        <li class="active">Income</li>
    </ol>
</section>

<section class="content" ng-app="income" ng-controller="incomeCtrl">
    <div class="row">
        <?php include APPPATH . 'views/transaction/_tab.php'; ?>
        <div class="col-md-8">


            <div class="box">
                <div class="box-header">

                    <h3 class="box-title">
                        <?= $title; ?>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form role="form" method="post" action="">
                        <div class="modal-body">
                            <?php include '_deposit_form.php'; ?>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="income_type_id" value="<?= $income_type ?>" />
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" ng-disabled="validate()" >Add Deposit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Related Account Info.
                    </h3>
                </div>
                <div class="box-body">
                    <div ng-cloak="">
                        <h5> AR Balance :<b> {{ar_info.balance | number:2}} </b> </h5>
                        <h5> Bank Account :<b> {{bank.description}} </b> </h5>
                        <h5> Balance : <b> {{bank.balance| number:2}} </b></h5>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script src="/js/income.js"></script>
<script>
                            $('document').ready(function () {
                                $('#income_date').datepicker();
                                $('#due_date').datepicker();

                                $('#quarter_id').change(function () {
                                    var quarter_id_ = $("#quarter_id :selected").val();


                                    $.ajax({
                                        url: '<?= site_url('transaction/transaction_controller/load_month/') ?>/' + quarter_id_,
                                        type: 'GET',
                                        success: function (mth) {
                                            $('#div_month').html(mth);
                                            $('#month_id').addClass('form-control');
                                        }
                                    });

                                });
                            });
</script>


