<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs">
    <li class="<?= strstr($path, 'setup/accounts') || strstr($path, 'setup/opening_balance') ? 'active' : '' ?>">
        <a href="<?= site_url('setup/accounts') ?>" aria-expanded="true">Accounts</a>
    </li>
    <li class="<?= strstr($path, 'setup/expense_types') ? 'active' : '' ?>">
        <a href="<?= site_url('setup/expense_types') ?>" aria-expanded="true">Expense Types</a>
    </li>
    <li class="<?= strstr($path, 'setup/fixed_assets') ? 'active' : '' ?>">
        <a href="<?= site_url('setup/fixed_assets') ?>" aria-expanded="true">Fixed Assets</a>
    </li>
    <li class="<?= strstr($path, 'setup/income_types') ? 'active' : '' ?>">
        <a href="<?= site_url('setup/income_types') ?>" aria-expanded="true">Income Types</a>
    </li>
    <li class="<?= strstr($path, '/setup/income_sources') ? 'active' : '' ?>">
        <a href="<?= site_url('/setup/income_sources') ?>" aria-expanded="true">Income Sources</a>
    </li>
    <li class="<?= strstr($path, '/setup/banks') ? 'active' : '' ?>">
        <a href="<?= site_url('/setup/banks') ?>" aria-expanded="true">Banks</a>
    </li>
    <li class="<?= strstr($path, '/setup/vendors') ? 'active' : '' ?>">
        <a href="<?= site_url('/setup/vendors') ?>" aria-expanded="true">Vendors/Payee</a>
    </li>
</ul>