<?= show_notification(); ?>
<section class="content-header">
    <h1>
        <a class="btn btn-warning btn-flat btn-sm" href="<?= site_url('/setup/view_record/'.$this->uri->segment(3)); ?>"> <i class="fa fa-chevron-left"></i>Back</a>
        Edit Profile Picture
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <!--        <li><a href="#">Student Enrollment Form</a></li>
                <li class="active">Course Selection</li>-->
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h1>&nbsp;</h1>
                </div>
                <div class="box-body">
                    <?php
                    $profile_pic_src = "/img/profile-default.jpg";
                    if ($profile_picture):
                        $profile_pic_src = $profile_picture[0]->profile_picture_url;
                    endif;
                    ?>
                    <form method="post" enctype="multipart/form-data">
                        <table class="table">
                            <tr>
                                <td>Profile Picture</td>
                                <td><img src="<?= $profile_pic_src ?>" style="max-width: 150px" />
                                    <input required type="file" name="userfile" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="hidden" name="employee_id" value="<?= $employee_id; ?>" />
                                    <button type="submit" class="btn btn-primary btn-flat"> Upload </button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>