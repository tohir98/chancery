<div class="box box-solid box-primary employee_info_box" id="emergency_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Emergency Contact

        </h3>
    </div>
    <div class="box-body">
        <?php if ($emergency_details): ?>
            <table class="table table-striped">
                <?php foreach ($emergency_details as $label => $value): ?>
                    <tr>
                        <td>
                            <?= $label; ?>
                        </td>
                        <td>
                            <?= $value; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php endif;
        ?>
    </div>
</div>
