<div class="box box-solid box-primary employee_info_box" id="bank_details_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Bank Information

        </h3>
    </div>
    <div class="box-body">
        <?php if ($bank_details): ?>
            <table class="table table-striped">
                <?php foreach ($bank_details as $idx => $val): ?>
                    <tr>
                        <td>
                            <?= $idx; ?>
                        </td>
                        <td>
                            <?= $val; ?>
                        </td>
                    </tr>
                     <?php endforeach; ?>
                </table>
            <?php 
        endif; ?>
    </div>
</div>
