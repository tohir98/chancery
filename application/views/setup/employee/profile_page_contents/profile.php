<div class="box box-solid box-primary employee_info_box" id="profile_container">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Profile
            
        </h3>
    </div>
    <div class="box-body">
        <?php if($record): ?>
        <table class="table table-striped">
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <?= ucfirst($record->first_name) . ' ' . ucfirst($record->last_name) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Gender:
                </td>
                <td>
                    <?= $record->gender_id ?>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Birth:
                </td>
                <td>
                    <?= $record->dob !== '0000-00-00' ? date('d-M-Y', strtotime($record->dob)) : 'NA' ?>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?= $record->email ?>
                </td>
            </tr>
            <tr>
                <td>
                    Address:
                </td>
                <td>
                    <?= $record->address ?>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <?= $record->city ?>
                </td>
            </tr>
            <tr>
                <td>
                    State:
                </td>
                <td>
                    <?= $record->state; ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
