<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('/setup/employees') ?>" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Employees</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <select class="form-control institutions" id="cboStudents">

                <?php
                if (!empty($employees)):
                    foreach ($employees as $emp):
                        $sel = '';
                        if ($emp->employee_id == $this->uri->segment(3)):
                            $sel = 'selected';
                        else:
                            $sel = '';
                        endif;
                        ?>
                <option value="<?= $emp->employee_id; ?>" <?= $sel; ?>><?= ucfirst($emp->first_name . ' ' . $emp->last_name); ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                    <div class="row-fluid" style="height:180px;overflow:hidden">

                        <?php
                        $pic_src = "/img/avatar5.png";
                        if (strlen($profile_picture) > 0):
                            $pic_src = $profile_picture;
                        endif;
                        ?>
                        <img src="<?= $pic_src ?>" style="max-width: 150px">
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <div class="btn-group" style="width: 100%">
                        <button type="button" style="width: 100%" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit
                            <span class="caret"></span> 
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= site_url('/setup/change_emp_picture/' . $this->uri->segment(3)) ?>">Change Picture</a></li>
                            <?php if ($profile_picture): ?>
                                <li><a class="remove_pic" href="<?= site_url('/setup/remove_emp_picture/' . $this->uri->segment(3)) ?>">Remove Picture</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body no-padding" style="margin-top: 10px">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a class="left_menu_button" href="#profile_container"> Profile</a></li>
<!--                                <li> <a class="left_menu_button" href="#contact_details_container">Contact Details</a></li>-->
                                <li> <a class="left_menu_button" href="#bank_details_container">Bank Details</a></li>
                                <li> <a class="left_menu_button" href="#emergency_container">Emergency Contact</a></li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php
            echo $profile_content;
//            echo $contact_details_content;
            echo $bank_details_content;
            echo $emergency_content;
//            echo $commission_info_content;
            ?>
        </div>
    </div>
</section>

<script src="/js/links.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
            $('.remove_pic').click(function (e) {
                e.preventDefault();
                var h = this.href;
                var message = 'Are you sure you want to remove this profile picture ?';
                OaaStudy.doConfirm({
                    title: 'Confirm ',
                    message: message,
                    onAccept: function () {
                        window.location = h;
                    }
                });
            });
        });

     
        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.employee_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.institutions').change(function () {
            var ref_id_ = $(this).val();

            window.location.href = '<?php echo site_url('/setup/view_record'); ?>/' + ref_id_;
        });




    });


</script>
