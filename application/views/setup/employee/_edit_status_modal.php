<div class="modal" id='edit_status_modal' ng-controller="statusCtrl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="edCancelBtn close" data-dismiss='modal' aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Status</h4>
            </div>
            <form method="post" action="<?= site_url('/setup/edit_status') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Status</label>
                        <select required name="status_id" id="status_id" ng-model="status_id" class="form-control">
                            <option value="" selected="selected">Select Status</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                            <option value="3">Terminated</option>
                        </select>
                    </div>
                    <div id="dv_termination" ng-show="status_id==3">
                        <div class="form-group">
                            <label>Reason</label>
                            <textarea name="reason" id="reason" class="form-control" style="width: 95%"></textarea>
                        </div>
                        <div class="form-group" ng-show="status_id==3">
                            <label>Termination Date</label>
                            <input type="text" name="date_termination" id="date_termination" class="form-control datepicker" placeholder="dd/mm/yyyy" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="employee_id" id="employee_id" />
                    <a class="btn btn-warning edCancelBtn" data-dismiss='modal'>Cancel</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>