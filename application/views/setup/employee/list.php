<div ng-app="employees">
<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Employees Directory
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Employees</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-flat" href="<?= site_url('/setup/add_employee'); ?>" > 
                                    <i class="fa fa-plus-circle"></i> Add Employee
                                </a>
                            </div>
                            <div class="box-body">
                                <?php if (!empty($employees)): ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Contact Details</th>
                                                <th>Gender</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($employees as $emp):
                                                ?>
                                                <tr>
                                                    <td><img src="/img/profile_image.png" class="img-thumbnail" style="width: 40px; height: 40px"></td>
                                                    <td>
                                                        <a href="<?= site_url('/setup/view_record/'. $emp->employee_id); ?>">
                                                <?= ucfirst($emp->first_name) . ' ' . ucfirst($emp->last_name) ?> </a>
                                                    </td>
                                                    <td><?= $emp->phone ?> <br>
                                                        <?= $emp->email; ?>
                                                    </td>
                                                    <td><?= $genders[$emp->gender_id]; ?></td>
                                                    <td><?= $statuses[$emp->status]; ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="#" onclick="return false;" data-employee_id="<?= $emp->employee_id ?>" class="edit_account">Change Status</a></li>
                                                                        <li><a href="<?= site_url('/setup/editEmployee/' . $emp->employee_id) ?>">Edit</a></li>
                                                                <li><a href="<?= site_url('/setup/deleteEmployee/' . $emp->employee_id) ?>" class="deleteEmployee">Delete</a></li>
                                                                <li><a href="<?= site_url('/setup/view_record/' . $emp->employee_id) ?>" class="">View Record</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data("Employees on found");
                                endif;
                                ?>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="addAccount">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Bank Account</h4>
            </div>
            <form role="form" method="post">
                <?php include '_account_form.php'; ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit status modal -->
<?php include '_edit_status_modal.php'; ?>

<div class="modal" id="modal_edit_account">
</div>

</div>

<script src="/js/employees.js"></script>