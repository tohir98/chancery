<?= show_notification(); ?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('/setup/employees') ?>" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-chevron-left"></i> Back</a>
        <?= isset($title) ? $title : 'New Employee'; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('/admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Add Employee</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                            </div>
                            <div class="box-body" ng-app="journal" ng-controller="journalCtrl">
                                <form id="frm" action="" role="form" method="post" >
                                    <table class="table table-condensed" >
                                        <tr>
                                            <td style="width: 20%">First Name</td>
                                            <td>
                                                <input type="text" name="first_name" id="first_name" class="form-control" value="<?= !empty($emp_info) ? $emp_info->first_name : ''; ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">Last Name</td>
                                            <td>
                                                <input type="text" name="last_name" id="last_name" class="form-control" value="<?= !empty($emp_info) ? $emp_info->last_name : ''; ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td>
                                                <select required name="gender_id" id="gender_id" class="form-control">
                                                    <option value="">Select gender</option>
                                                    <option value="1" <?= isset($emp_info->gender_id) && $emp_info->gender_id == 1 ? 'selected' : '' ?>>Female</option>
                                                    <option value="2" <?= isset($emp_info->gender_id) && $emp_info->gender_id == 2 ? 'selected' : '' ?>>Male</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth:</td>
                                            <td><input required type="text" class="form-control datepicker" id="dob" name="dob" value="<?= !empty($emp_info) ? date('d/m/Y', strtotime($emp_info->dob)) : ''; ?>" ></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= !empty($emp_info) ? $emp_info->email : ''; ?>"></td>        
                                        </tr>
                                        <tr>  
                                            <td>Address</td>
                                            <td><input required type="text" class="form-control" id="address" name="address" placeholder="address" value="<?= !empty($emp_info) ? $emp_info->address : ''; ?>"></td>
                                        </tr>
                                        <tr>  
                                            <td>City</td>
                                            <td><input required type="text" class="form-control" id="city" name="city" placeholder="city" value="<?= !empty($emp_info) ? $emp_info->city : ''; ?>"></td>
                                        </tr>
                                        <tr>  
                                            <td>State</td>
                                            <td>
                                                <select required class="form-control" id="state_id" name="state_id">
                                                    <option value="">Select State</option>
                                                    <?php
                                                    if (!empty($states)):
                                                        $sel = '';
                                                        foreach ($states as $state):
                                                            if ($emp_info->state_id == $state->state_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                            ?>
                                                            <option value="<?= $state->state_id ?>" <?= $sel; ?>><?= ucfirst(trim($state->state)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Place of Origin</td>
                                            <td><input required type="text" class="form-control" id="placeof_origin" name="placeof_origin" placeholder="Place of Origin" value="<?= !empty($emp_info) ? $emp_info->city : ''; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td><input required type="text" class="form-control" id="phone" name="phone" value="<?= isset($emp_info->phone) ? $emp_info->phone : ''; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><span style="font-size: 1.5em">Work Information</span></td>
                                        </tr>
                                        <tr>
                                            <td>Commencement Date</td>
                                            <td><input required type="text" class="form-control datepicker" id="employment_date" name="employment_date" value="<?= isset($emp_info->employment_date) ? date('m/d/Y', strtotime($emp_info->employment_date)) : ''; ?>" ></td>
                                        </tr>
                                        <tr>
                                            <td>Position</td>
                                            <td><input required type="text" class="form-control" id="position" name="position" value="<?= isset($emp_info->position) ? $emp_info->position : ''; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Job Type</td>
                                            <td>
                                                <select required id="job_type_id" name="job_type_id" class="form-control">
                                                    <option value="">Select Job Type</option>
                                                    <?php
                                                    $sel = '';
                                                    foreach ($job_types as $id => $val):
                                                        if ($emp_info->job_type_id == $id):
                                                            $sel = 'selected';
                                                        else:
                                                            $sel = '';
                                                        endif;
                                                        ?>
                                                        <option value="<?= $id ?>" <?= $sel; ?>><?= $val ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="table">
                                                    <tr>
                                                        <td>
                                                            <table class="table">
                                                                <tr>
                                                                    <td colspan="2"><span style="font-size: 1.5em">Emergency contact</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Emergency Contact</td>
                                                                    <td><input required type="text" class="form-control" id="emergency_name" name="emergency_name" value="<?= isset($emp_info->emergency_name) ? $emp_info->emergency_name : ''; ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Emergency Phone</td>
                                                                    <td><input required type="text" class="form-control" id="emergency_phone" name="emergency_phone" value="<?= isset($emp_info->emergency_phone) ? $emp_info->emergency_phone : ''; ?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Relationship</td>
                                                                    <td>
                                                                        <select required class="form-control" id="relationship_id" name="relationship_id">
                                                                            <option value="">Select Relationship</option>
                                                                            <?php
                                                                            if (!empty($relationships)):
                                                                                $sel = '';
                                                                                foreach ($relationships as $relationship):
                                                                                    if ($emp_info->relationship_id == $relationship->relationship_id):
                                                 $sel = 'selected';
                                                                                    else:
                                                                                        $sel = '';
                                                                                    endif;
                                                                                    ?>
                                                                                    <option value="<?= $relationship->relationship_id ?>" <?= $sel;?>><?= ucfirst(trim($relationship->relationship)) ?></option>
                                                                                    <?php
                                                                                endforeach;
                                                                            endif;
                                                                            ?>
                                                                        </select>   
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table class="table">
                                                                <tr>
                                                                    <td colspan="2"><span style="font-size: 1.5em">Bank Details</span></td>
                                                                </tr>
                                                                <tr>  
                                                                    <td>Bank</td>
                                                                    <td>
                                                                        <select required class="form-control" id="bank_id" name="bank_id">
                                                                            <option value="">Select Bank</option>
                                                                            <?php
                                                                            if (!empty($banks)):
                                                                                $sel = '';
                                                                                foreach ($banks as $bank):
                                                                                    if ($emp_info->bank_id == $bank->bank_id):
                                                                                        $sel = 'selected';
                                                                                        else:
                                                                                        $sel = '';
                                                                                    endif;
                                                                                    ?>
                                                                                    <option value="<?= $bank->bank_id ?>" <?= $sel; ?>><?= ucfirst(trim($bank->name)) ?></option>
                                                                                    <?php
                                                                                endforeach;
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Account Name</td>
                                                                    <td><input required type="text" class="form-control" id="account_name" name="account_name" value="<?= isset($emp_info->account_name) ? $emp_info->account_name : ''; ?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Account No</td>
                                                                    <td><input required type="text" class="form-control" id="account_no" name="account_no" value="<?= isset($emp_info->account_no) ? $emp_info->account_no : ''; ?>"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr>
                                            <td>&nbsp; </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                                <button type="reset" class="btn btn-warning">Clear</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $("#category_id").change(function (eve) {
        eve.preventDefault();

    });
</script>