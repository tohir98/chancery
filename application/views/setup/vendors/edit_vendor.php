<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Vendor</h4>
        </div>
        <form name="frmEditUser" method="post" action="<?= site_url('/setup/editVendor/' . $vendor_info->vendor_id) ?>">
            <div class="modal-body">
                <?php include '_vendor_form.php'; ?>
            </div>
            
        </form>
    </div><!-- /.modal-content -->
</div><!--