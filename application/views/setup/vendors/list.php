<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Vendors</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php include APPPATH . 'views/setup/_tab.php'; ?>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-flat" href="<?= site_url('/setup/vendors/add_vendor') ?>"> 
                                    <i class="fa fa-plus-circle"></i> Add Vendor
                                </a>
                            </div>
                            <div class="box-body">
                                <?php if (!empty($vendors)): ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Vendor</th>
                                                <th>Abbreviation</th>
                                                <th>Contact Details</th>
                                                <th>Vendor Since</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($vendors as $vendor):
                                                ?>
                                                <tr>
                                                    <td><?= $vendor->vendor_name ?></td>
                                                    <td><?= $vendor->abbrevation ?></td>
                                                    <td>
                                                        <?= $vendor->phone ?> <br>
                                                        <?= $vendor->email ?> 
                                                    </td>
                                                    <td><?= date('d M Y', strtotime($vendor->vendor_since)) ?></td>
                                                    <td><?= $statuses[$vendor->status] ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">

                                                                <li><a href="<?= site_url('/setup/editVendor/' . $vendor->vendor_id) ?>" class="edit_vendor">Edit</a></li>
                                                                <li><a href="<?= site_url('/setup/deleteVendor/' . $vendor->vendor_id) ?>" class="deleteVendor">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php
                                else:
                                    echo show_no_data("No vendor has been added");
                                endif;
                                ?>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal_edit_vendor">
</div>

<script>
    $(function () {
        $('.groupToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this sub group, you will not be able to assign it, Are you sure you want to continue? ' : 'Are you sure you want to enable selected sub group ?';
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });

        $('body').delegate('.deleteVendor', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this vendor?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_vendor', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_vendor').modal('show');
        $('#modal_edit_vendor').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_vendor').html('');
            $('#modal_edit_vendor').html(html);
            $('#modal_edit_vendor').modal('show').fadeIn();
        });
        return false;
    });


</script>