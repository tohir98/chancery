


<table class="table table-condensed table-bordered">
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Vendor Name</label>
                <input required type="text" class="form-control" id="vendor_name" name="vendor_name" placeholder="Vendor Name" value="<?= isset($vendor_info->vendor_name) ? $vendor_info->vendor_name : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Abbreviation</label>
                <input required type="text" class="form-control" id="abbrevation" name="abbrevation" placeholder="Description" value="<?= isset($vendor_info->abbrevation) ? $vendor_info->abbrevation : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Account Number</label>
                <input type="text" class="form-control" id="account_number" name="account_number" placeholder="Account Number" value="<?= isset($vendor_info->account_number) ? $vendor_info->account_number : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Vendor Since</label>
                <input required type="text" class="form-control" id="vendor_since" style="cursor: pointer" name="vendor_since" placeholder="" value="<?= isset($vendor_info->vendor_since) ? date('m/d/Y', strtotime($vendor_info->vendor_since)) : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2"> Contact Information</td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Address</label>
                <input required type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?= isset($vendor_info->address) ? $vendor_info->address : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">City</label>
                <input required type="text" class="form-control" id="city" name="city" placeholder="City" value="<?= isset($vendor_info->city) ? $vendor_info->city : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">State</label>
                <select required class="form-control" name="state_id" id="state_id">
                    <option value="">Select State</option>
                    <?php
                    if (!empty($states)):
                        $sel = '';
                        foreach ($states as $state):
                            if (isset($vendor_info->state_id)):
                                if ($vendor_info->state_id == $state->state_id):
                                    $sel = "selected";
                                else:
                                    $sel = '';
                                endif;
                            endif;
                            ?>
                            <option value="<?= $state->state_id ?>" <?= $sel ?>><?= $state->state; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= isset($vendor_info->email) ? $vendor_info->email : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Contact Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?= isset($vendor_info->phone) ? $vendor_info->phone : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group pull-right">
                <br>
                <? if (!empty($vendor_info)): ?>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                <? else: ?>

                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-lg btn-primary" >Save</button>
                <? endif; ?>
            </div>
        </td>
    </tr>
</table>

<script>
    $('document').ready(function () {
        $('#vendor_since').datepicker();
    });
</script>