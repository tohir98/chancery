<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Income Source</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/setup/_tab.php'; ?>

            <div class="box">
                <div class="box-header">

                    <h3 class="box-title">
                        New Vendor
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form role="form" method="post">
                        <div class="modal-body">
                            <?php include '_vendor_form.php'; ?>
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>