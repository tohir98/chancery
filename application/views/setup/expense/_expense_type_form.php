<div class="form-group">
    <label for="class_name">Expense Type</label>
    <input required type="text" class="form-control" id="expense_type" name="expense_type" placeholder="Expense Type" value="<?= isset($expense_type) ? $expense_type : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Description</label>
    <input required type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?= isset($description) ? $description : ''; ?>">
</div>