<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Groups</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/setup/_tab.php'; ?>
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-flat" href="#modal-expense_type" data-toggle="modal"> 
                                    <i class="fa fa-plus-circle"></i> Add Expense Type
                                </a>
                            </div>
                            <div class="box-body">
                                <?php
                                if (!empty($expense_types)):
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Income Type</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sn = 0;
                                            foreach ($expense_types as $type):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn ?></td>
                                                    <td><?= $type->expense_type ?></td>
                                                    <td><?= $type->description ?></td>
                                                    <td><?= $type->status ? 'Active' : 'Inactive' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info">Action</button>
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/setup/editExpenseType/' . $type->expense_type_id) ?>" class="edit_type">Edit</a></li>
                                                                <li><a href="<?= site_url('/setup/deleteExpenseType/' . $type->expense_type_id) ?>" class="delete">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
    <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No expense type has been added. <a href=#modal-expense_type data-toggle=modal>Click here to add one.</a>";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal_edit_expense">
</div>

<div class="modal" id="modal-expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Expense Type</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
<?php include '_expense_type_form.php'; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this expense type';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_type', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_expense').modal('show');
        $('#modal_edit_expense').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_expense').html('');
            $('#modal_edit_expense').html(html);
            $('#modal_edit_expense').modal('show').fadeIn();
        });
        return false;
    });



</script>