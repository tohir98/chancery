<?php

$expense_type = $expense_typ->expense_type;
$description = $expense_typ->description;
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Income Type</h4>
        </div>
        <form name="frmEditUser" method="post" action="<?= site_url('/setup/editExpenseType/' . $expense_typ->expense_type_id) ?>">
            <div class="modal-body">
                <?php include '_expense_type_form.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!--