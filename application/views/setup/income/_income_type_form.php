<div class="form-group">
    <label for="class_name">Income Type</label>
    <input required type="text" class="form-control" id="income_type" name="income_type" placeholder="Income Type" value="<?= isset($income_type) ? $income_type : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Description</label>
    <input required type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?= isset($description) ? $description : ''; ?>">
</div>