<?php
$source_name = $income_source->source_name;
$abbrevation = $income_source->abbrevation;
$account_no = $income_source->account_no;
$website = $income_source->website;
$remarks = $income_source->remarks;
$contact_person = $income_source->contact_person;
$email = $income_source->email;
$phone = $income_source->phone;
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Income Source</h4>
        </div>
        <form name="frmEditUser" method="post" action="<?= site_url('/setup/editIncomeSource/' . $income_source->income_source_id) ?>">
            <div class="modal-body">
                <?php include '_income_source_form.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!--