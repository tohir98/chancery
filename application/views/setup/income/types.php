<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Accounts</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/setup/_tab.php'; ?>
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    <a class="btn btn-flat btn-success" href="#modal-income_type" data-toggle="modal">
                                        <i class="fa fa-plus-circle"></i> Add Income type
                                    </a>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if (!empty($income_types)):
                                    ?>
                                    <table class="table table-bordered table-striped table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Income Type</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $sn = 0;
                                            foreach ($income_types as $type):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn ?></td>
                                                    <td><?= $type->income_type ?></td>
                                                    <td><?= $type->description ?></td>
                                                    <td><?= $type->status ? 'Active' : 'Inactive' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                 Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/setup/editIncomeType/' . $type->income_type_id) ?>" class="edit_type">Edit</a></li>
                                                                <li><a href="<?= site_url('/setup/deleteIncomeType/' . $type->income_type_id) ?>" class="delete">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
    <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No income type has been added. <a href=#modal-income_type data-toggle=modal>Click here to add one.</a>";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-income_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Income Type</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
<?php include '_income_type_form.php'; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-flat" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_income">
</div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this income type';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_type', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_income').modal('show');
        $('#modal_edit_income').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_income').html('');
            $('#modal_edit_income').html(html);
            $('#modal_edit_income').modal('show').fadeIn();
        });
        return false;
    });
</script>