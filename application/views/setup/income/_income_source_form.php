<table class="table">
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Source Name</label>
                <input required type="text" class="form-control" id="income_type" name="source_name" placeholder="Source Name" value="<?= isset($source_name) ? $source_name : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Abbrevation</label>
                <input required type="text" class="form-control" id="abbrevation" name="abbrevation" placeholder="Abbrevation" value="<?= isset($abbrevation) ? $abbrevation : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Account No</label>
                <input required type="text" class="form-control" id="account_no" name="account_no" placeholder="Account No" value="<?= isset($account_no) ? $account_no : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Website</label>
                <input required type="text" class="form-control" id="website" name="website" placeholder="Website" value="<?= isset($website) ? $website : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="class_name">Remarks</label>
                <input required type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks" value="<?= isset($remarks) ? $remarks : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="class_name">Contact Person</label>
                <input required type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Contact Person" value="<?= isset($contact_person) ? $contact_person : ''; ?>">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="email">Email</label>
                <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= isset($email) ? $email : ''; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Email" value="<?= isset($phone) ? $phone : ''; ?>">
            </div>
        </td>
    </tr>
</table>