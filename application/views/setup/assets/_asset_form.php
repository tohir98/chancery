<div class="form-group">
    <label for="class_name">Asset Name</label>
    <input required type="text" class="form-control" id="asset_name" name="asset_name" placeholder="Asset Name" value="<?= isset($asset_detail->asset_name) ? $asset_detail->asset_name : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Asset No.</label>
    <input required type="text" class="form-control" id="reg_no" name="reg_no" placeholder="Asset No" value="<?= isset($asset_detail->reg_no) ? $asset_detail->reg_no : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Account Category</label>
    <select required name="account_chart_id" id="account_chart_id" class="form-control">
        <option value="" selected>Select Category</option>
        <?php
        if (!empty($accounts)):
            $sel = ' ';
            foreach ($accounts as $account):
                if (isset($asset_detail->account_chart_id)):
                    if ($asset_detail->account_chart_id == $account->account_chart_id):
                        $sel = ' selected';
                    else:
                        $sel = ' ';
                    endif;
                endif;
                ?>
                <option value="<?= $account->account_chart_id ?>" <?= $sel; ?>><?= ucfirst($account->description) ?></option>
                <?php
            endforeach;
        endif;
        ?>
    </select>
</div>
<div class="form-group">
    <label for="class_name">Purchase Price</label>
    <input required type="text" class="form-control" id="purchase_price" name="purchase_price" placeholder="Purchase Price" value="<?= isset($asset_detail->purchase_price) ? $asset_detail->purchase_price : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Purchase Date</label>
    <input required type="text" class="form-control datepicker" id="purchase_date" name="purchase_date" value="<?= isset($asset_detail->purchase_date) ? date('m/d/Y', strtotime($asset_detail->purchase_date)) : ''; ?>" style="cursor: pointer">
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success btn-flat" ><?= !empty($asset_detail) ? 'Update' : 'Save'; ?> Asset</button>
    <button type="reset" class="btn btn-warning btn-flat">Reset</button>
</div>
