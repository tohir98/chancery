<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Fixed Assets</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/setup/_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    <a class="btn btn-flat btn-success" href="<?= site_url('/setup/fixed_assets/new') ?>">
                                        <i class="fa fa-plus-circle"></i> Add Fixed Asset
                                    </a>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if (!empty($assets)):
                                    ?>
                                    <table class="table table-bordered table-striped table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>Asset Name</th>
                                                <th>Asset No</th>
                                                <th>Account Category</th>
                                                <th>Price</th>
                                                <th>Purchase Date</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($assets as $asset): ?>
                                                <tr>
                                                    <td><?= $asset->asset_name ?></td>
                                                    <td><?= $asset->reg_no ?></td>
                                                    <td>
                                                        <?= ucfirst($asset->description) ?>(<?= $asset->code; ?>)
                                                    </td>
                                                    <td><?= number_format($asset->purchase_price, 2); ?></td>
                                                    <td><?= date('d-M-Y', strtotime($asset->purchase_date)); ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li>
                                                                    <a href="<?= site_url('/setup/fixed_assets/edit/' . $asset->ref_id) ?>">Edit</a>
                                                                </li>
                                                                <li><a href="<?= site_url('/setup/delete_asset/' . $asset->asset_id) ?>" class="delete">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No asset has been added. <a href='#new_asset' data-toggle='modal'>Click here to add one.</a>";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this asset';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });


    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>