<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Fixed Assets</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <?php include APPPATH . 'views/setup/_tab.php'; ?>
        <div class="col-md-12">
            
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">

                        <div class="box">
                            <div class="box-header">
                                <h3>
                                    <?= !empty($asset_detail) ? 'Edit' : 'New' ?> Asset
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <form method="post" class="form-horizontal">
                                    <?php include '_asset_form.php'; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
