<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Accounts
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('/admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a href="<?= site_url('/setup/accounts') ?>" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-chevron-left"></i> Back</a>
                                <h3>
                                    Add New Account</h3>
                            </div>
                            <div class="box-body" ng-app="journal" ng-controller="journalCtrl">
                                <form id="frm" action="" role="form" method="post" >
                                    <table class="table" >
                                         <tr>
                                             <td style="width: 20%">Account Category</td>
                                            <td>
                                                <select class="form-control" ng-model="category_id" id="account_category_id" name="account_category_id" ng-change="loadSubCategories()">
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    if (!empty($categories)):
                                                        $sel = '';
                                                        foreach ($categories as $category):
                                                            ?>
                                                            <option value="<?= $category->account_category_id ?>"><?= trim($category->category) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Sub Category</td>
                                            <td>
                                                <div ng-if="subcategoryStatus !== ''">
                                                    {{subcategoryStatus}}
                                                </div>
                                                <div ng-if="subcategoryStatus === ''">
                                                    <select name="account_sub_category_id" id="account_sub_category_id" class="form-control" ng-model="sub_category_id">
                                                        <option ng-repeat="subcategory in subcategories" value="{{subcategory.id}}">
                                                            {{subcategory.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Code</td>
                                            <td><input required type="text" class="form-control" id="code" name="code" placeholder="Account Code" value="" maxlength="4"></td>        
                                        </tr>
                                        <tr>  
                                            <td>Description</td>
                                            <td><input required type="text" class="form-control" id="description" name="description" placeholder="Description" value=""></td>
                                        </tr>
                                        <tr>  
                                            <td>Account Heading</td>
                                            <td>
                                                <select required class="form-control" id="heading_type_id" name="heading_type_id">
                                                    <option value="">Select Account Heading</option>
                                                    <?php
                                                    if (!empty($heading_types)):
                                                        $sel = '';
                                                        foreach ($heading_types as $heading):
                                                            ?>
                                                    <option value="<?= $heading->heading_type_id ?>"><?= ucfirst(trim($heading->heading)) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp; </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                                <button type="reset" class="btn btn-warning">Clear</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>
                            </div>
                        </div>

                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<script>
    $("#category_id").change(function (eve) {
        eve.preventDefault();

    });
</script>