<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Account Balance</h4>
        </div>
        <form name="frmEditUser" method="post" action="<?= site_url('/setup/editAccountBalance/' . $account_bal->account_balance_id . '/' . $account_chart_id) ?>">
            <div class="modal-body">
                <?php include '_opening_closing_bal.php'; ?>
            </div>
            
        </form>
    </div><!-- /.modal-content -->
</div><!--