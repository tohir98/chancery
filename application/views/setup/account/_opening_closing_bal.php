<div class="modal-body">
    <div class="form-group">
        <label class="control-label">Year</label>
        <input required class="form-control" name="year" placeholder="<?= date('Y'); ?>" value="<?= isset($account_bal->year) ? $account_bal->year : ''; ?>" />
    </div>
    <div class="form-group">
        <label class="control-label">Opening Balance</label>
        <input required class="form-control" name="opening_balance" placeholder="500000" value="<?= isset($account_bal->opening_balance) ? $account_bal->opening_balance : ''; ?>" />
    </div>
    <div class="form-group">
        <label class="control-label">Closing Balance</label>
        <input class="form-control" name="closing_balance" placeholder="2500000" value="<?= isset($account_bal->closing_balance) ? $account_bal->closing_balance : ''; ?>" />
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" name="account_chart_id" value="<?= $account_chart_id; ?>" />
    <a class="btn btn-danger  btn-flat btn-sm edCancelBtn" data-dismiss='modal'>Cancel</a>
    <button type="submit" class="btn btn-primary btn-flat btn-sm" ><?= !empty($account_bal) ? 'Update' : 'Save'; ?></button>
</div>