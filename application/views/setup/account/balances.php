<?= show_notification(); ?>
<section class="content-header">
    <a class="btn btn-warning btn-flat btn-sm" href="<?= site_url('/setup/accounts'); ?>">
        <i class="fa fa-chevron-left"></i>
        Back
    </a>
    <h1>
        <?= $account_info[0]->description; ?> Opening/Closing Balance
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Accounts</li>
        <li class="active">Opening/Closing Balance</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include APPPATH . 'views/setup/_tab.php'; ?>
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title">
                                    <a class="btn btn-block btn-success btn-flat pull-right" href="#addBalance" data-toggle='modal'>
                                        <i class="fa fa-plus-circle"></i> Add Opening/Closing Balance
                                    </a>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if (!empty($balances)):
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Year</th>
                                                <th>Opening Balance</th>
                                                <th>Closing Balance</th>
                                                <th style="width: 100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($balances as $balance): ?>
                                                <tr>
                                                    <td><?= $balance->year ?></td>
                                                    <td><?= number_format($balance->opening_balance, 2); ?></td>
                                                    <td><?= (int) $balance->closing_balance > 0 ? number_format($balance->closing_balance, 2) : ''; ?></td>
                                                    <td>
                                                        <a href="<?= site_url('/setup/editAccountBalance/' . $balance->account_balance_id. '/' . $account_chart_id) ?>" class="editBal">
                                                            <i class="fa fa-edit"></i> edit
                                                        </a> |
                                                        <a href="<?= site_url('/setup/deleteAccountBalance/' . $balance->account_balance_id) ?>" class="deleteBal">
                                                            <i class="fa fa-trash"></i> delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    $msg = "No balance has been added.";
                                    echo show_no_data($msg);
                                endif;
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>


<div class="modal" id='addBalance'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal' aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Opening/Closing Balance</h4>
            </div>
            <form method="post" action="">
               <?php include '_opening_closing_bal.php'; ?>
            </form>
        </div>
    </div>
</div>


<div class="modal" id="modal_edit_balance">
</div>

<script>
    $(function () {
        $('.deleteBal').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this account balance';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    
    $('body').delegate('.editBal', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_balance').modal('show');
        $('#modal_edit_balance').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_balance').html('');
            $('#modal_edit_balance').html(html);
            $('#modal_edit_balance').modal('show').fadeIn();
        });
        return false;
    });
   
</script>