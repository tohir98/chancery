<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Setup
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Bank Accounts</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php include APPPATH . 'views/setup/_tab.php'; ?>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-flat" href="#addAccount" data-toggle="modal"> 
                                    <i class="fa fa-plus-circle"></i> Add Bank Account
                                </a>
                            </div>
                            <div class="box-body">
                                <?php if (!empty($bank_accounts)): ?>
                                    <table class="table table-condensed table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>Account Name</th>
                                                <th>Account No</th>
                                                <th>Account Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($bank_accounts as $bank):
                                                ?>
                                                <tr>
                                                    <td><?= $bank->name ?></td>
                                                    <td><?= $bank->account_name ?></td>
                                                    <td><?= $bank->account_number ?></td>
                                                    <td><?= $account_types[$bank->account_type_id] ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                                Action <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="<?= site_url('/setup/editBankAccount/' . $bank->bank_account_id) ?>" class="edit_account">Edit</a></li>
                                                                <li><a href="<?= site_url('/setup/deleteBankAccount/' . $bank->bank_account_id) ?>" class="deleteAccount">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endif;
                                ?>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="addAccount">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Bank Account</h4>
            </div>
            <form role="form" method="post">
                <?php include '_account_form.php'; ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_account">
</div>

<script>
    $(function () {
        $('.groupToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this sub group, you will not be able to assign it, Are you sure you want to continue? ' : 'Are you sure you want to enable selected sub group ?';
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });

        $('body').delegate('.deleteAccount', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this account?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                cancelText: 'No',
                acceptText: 'Yes',
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_account', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_account').modal('show');
        $('#modal_edit_account').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_account').html('');
            $('#modal_edit_account').html(html);
            $('#modal_edit_account').modal('show').fadeIn();
        });
        return false;
    });


</script>