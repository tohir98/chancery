<div class="modal-body">
    <div class="form-group">
        <label for="bank_id">Bank</label>
        <select required name="bank_id" id="bank_id" class="form-control">
            <option value="">Select Bank</option>
            <?php
            if (!empty($banks)):
                $sel = '';
                foreach ($banks as $bank) :
                if (isset($account_info->bank_id) && ($bank->bank_id == $account_info->bank_id) ):
                    $sel = 'selected';
                else:
                    $sel = '';
                endif;
                    ?>
                    <option value="<?= $bank->bank_id ?>" <?=$sel;?>><?= $bank->name ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="account_name">Account Name</label>
        <input required type="text" class="form-control" id="account_name" name="account_name" placeholder="Account Name" value="<?php if (isset($account_info->account_name)): echo $account_info->account_name; endif; ?>" />
    </div>
    <div class="form-group">
        <label for="account_number">Account Number</label>
        <input required type="text" class="form-control" id="account_number" name="account_number" maxlength="10" placeholder="Account No" value="<?php if (isset($account_info->account_number)): echo $account_info->account_number; endif; ?>">
    </div>
    <div class="form-group">
        <label for="bank_id">Account Type</label>
        <select required name="account_type_id" id="account_type_id" class="form-control">
            <option value="">Select Account Type</option>
            <?php
            if (!empty($account_types)):
                $sel = '';
                foreach ($account_types as $account_type_id => $account_type) :
                if (isset($account_info->account_type_id) && ($account_type_id == $account_info->account_type_id) ):
                    $sel = 'selected';
                else:
                    $sel = '';
                endif;
                    ?>
                    <option value="<?= $account_type_id ?>" <?=$sel;?>><?= $account_type ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </div>
</div>