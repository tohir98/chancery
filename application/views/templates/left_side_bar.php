<!-- Left side column. contains the logo and sidebar -->
<?php if ($this->user_auth_lib->logged_in()): ?>


    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/chan_logo.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?= user_type_status($this->user_auth_lib->get('access_level')) ?></p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                    <?= $side_nav_menu ?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

<?php endif; ?>