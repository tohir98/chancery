<header class="main-header">
    <a href="/admin/dashboard#" class="logo"><?= $school_name ?></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Income <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= site_url('transaction/incomes/add_receivable');?>">Receivables</a></li>
                        <li><a href="<?= site_url('/transaction/incomes/add_cash_receipts') ?>">Cash Receipts</a></li>
                        <li><a href="<?= site_url('/transaction/incomes/add_deposit') ?>">Paid Receivables</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Expenses <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= site_url('/transaction/expenses/add_payable') ?>">Invoices/Payables</a></li>                
                        <li><a href="<?= site_url('/transaction/expenses/add_disbursement') ?>">Disbursements</a></li>
                        <li><a href="<?= site_url('transaction/expenses/add_paid_invoices');?>">Paid Invoices</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Payroll <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= site_url('/payroll/compensation') ?>">Staff Compensation</a></li>  
                        <li><a href="<?= site_url('/payroll/run_payroll');?>">Run Payroll</a></li>
                        <li><a href="<?= site_url('/payroll/settings');?>">Settings</a></li>
                        <li><a href="<?= site_url('/payroll/pay_records');?>">Records</a></li>
                    </ul>
                </li>
                
            </ul>
            <a href="<?= $logout_url ?>" class="pull-right" style="
               padding: 14px;
               color: white;
               display: block;
               border: 1px solid blueviolet;
               background: #324292;
               ">logout</a>
        </div>

    </nav>
</header>