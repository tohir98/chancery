<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright © <?= date('Y') ?> <a href="#"><?= BUSINESS_NAME ?></a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->


<!-- Input Mask -->

<script src="/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
<!-- bootstrap color picker -->
<script src="/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='/plugins/fastclick/fastclick.min.js'></script>
<!-- AdminLTE App -->
<script src="/js/app.min.js" type="text/javascript"></script>

<!-- Sparkline -->
<script src="/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<!--<script src="/js/demo.js" type="text/javascript"></script>-->
<!-- iCheck 
<script src="/plugins/iCheck/icheck.min.js" type="text/javascript"></script> -->
<!-- Page Script -->
<script>
    $(function () {
        //Enable color picker
//        $(".my-colorpicker2").colorpicker();
        $("[data-mask]").inputmask();
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
//        $('input[type="checkbox"]').iCheck({
//            checkboxClass: 'icheckbox_flat-blue',
//            radioClass: 'iradio_flat-blue'
//        });

        $(function () {
            $(".dataTable").dataTable();
//            $('.dataTable').dataTable({
//                "bPaginate": true,
//                "bLengthChange": false,
//                "bFilter": false,
//                "bSort": true,
//                "bInfo": true,
//                "bAutoWidth": false
//            });
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $("input[type='checkbox']", ".mailbox-messages").iCheck("uncheck");
            } else {
                //Check all checkboxes
                $("input[type='checkbox']", ".mailbox-messages").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
            e.preventDefault();
            //detect type
            var $this = $(this).find("a > i");
            var glyph = $this.hasClass("glyphicon");
            var fa = $this.hasClass("fa");

            //Switch states
            if (glyph) {
                $this.toggleClass("glyphicon-star");
                $this.toggleClass("glyphicon-star-empty");
            }

            if (fa) {
                $this.toggleClass("fa-star");
                $this.toggleClass("fa-star-o");
            }
        });
    });
</script>