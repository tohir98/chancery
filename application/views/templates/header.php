<!DOCTYPE html>
<html
    <head>
        <meta charset="UTF-8">
        <title><?= $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="/img/fav_icon.png">
        <!-- Bootstrap 3.3.2 -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Bootstrap Color Picker -->
        <link href="/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>
        <!-- Font Awesome Icons -->
        <!--<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <!--<link href="/css/ionicons.min.css" rel="stylesheet" type="text/css">-->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar 2.2.5-->
        <link href="/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css">
        <link href="/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print">
        <!-- Morris chart -->
        <link href="/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
        <link href="/css/agent.css" rel="stylesheet" type="text/css">
        <!-- Bootstrap switch -->
        <link href="/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
        <!-- Select2 -->
        <link href="/plugins/select2/select2.min.css" rel="stylesheet" type="text/css">
        
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link href="/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css">
        <!-- iCheck 
        <link href="/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css">-->
        <!-- Toastr -->
        <link href="/css/toastr.min.css" rel="stylesheet" type="text/css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.1.3 -->
        <script src="/plugins/jQuery/jQuery-2.1.3.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/js/angular.min.js" type="text/javascript"></script>
        <script src="/js/oaastudy.js" type="text/javascript"></script>
        <script src="/js/accounts.js" type="text/javascript"></script>
        <script src="/js/bootstrap-switch.js" type="text/javascript"></script>
        <script src="/js/toastr.min.js" type="text/javascript"></script>
        <!-- Select2 -->
        <script src="/plugins/select2/select2.full.min.js" type="text/javascript"></script>
    </head>