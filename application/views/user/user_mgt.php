<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <?php if ($this->user_auth_lib->get('access_level') == USER_TYPE_SUPER_ADMIN): ?>
                    <div class="box-header">
                        <a href="#modal-new_user" data-toggle="modal" class="btn btn-primary btn-flat"> <i class="fa fa-plus-circle"></i> New User</a>
                    </div>
                <?php endif; ?>

                <div class="box-body">
                    <?php
                    if (!empty($users)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 0;
                                foreach ($users as $user):
                                    ?>
                                    <tr>
                                        <td><?= ++$count; ?></td>
                                        <td><?= ucfirst($user->first_name) . ' ' . ucfirst($user->last_name); ?></td>
                                        <td> <?= $user->username ?> </td>
                                        <td> <?= $user->phone ?> </td>
                                        <td><?= $user->status ? 'Active' : 'Inactive' ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('administration/edit_user/' . $user->user_id) ?>" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('administration/delete_user/' . $user->user_id) ?>" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No user has been created. <a href='#modal-new_user' data-toggle='modal'>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-new_user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New User</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <?php include '_user_form.php'; ?>
                    <div class="form-group">
                        <label for="class_name">Password</label>
                        <input required type="password" class="form-control" id="password" name="password" placeholder="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_user">
</div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this user';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_user').modal('show');
        $('#modal_edit_user').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_user').html('');
            $('#modal_edit_user').html(html);
            $('#modal_edit_user').modal('show').fadeIn();
        });
        return false;
    });
</script>