<?= show_notification(); ?>
<section class="content-header">
    <h1>
        User Logs
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">User Logs</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                    <div class="box-header">
                        &nbsp;
                    </div>

                <div class="box-body">
                    <?php
                    if (!empty($user_logs) && $this->user_auth_lib->get('access_level') == USER_TYPE_SUPER_ADMIN):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>User</th>
                                    <th>IP Address</th>
                                    <th>Action</th>
                                    <th>Date/Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 0;
                                foreach ($user_logs as $user_log):
                                    ?>
                                    <tr>
                                        <td><?= ++$count; ?></td>
                                        <td><?= ucfirst($user_log->first_name) . ' ' . ucfirst($user_log->last_name); ?></td>
                                        <td> <?= long2ip($user_log->ip_address) ?> </td>
                                        <td> <?= $user_log->message ?> </td>
                                        <td> <?= date('d M Y h:i:s', strtotime($user_log->log_date)) ?> </td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        echo show_no_data("No user logs has been generated.");
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>