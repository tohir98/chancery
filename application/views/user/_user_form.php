<div class="form-group">
    <label for="class_name">First Name</label>
    <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?= isset($first_name) ? $first_name : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Last Name</label>
    <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?= isset($last_name) ? $last_name : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Phone</label>
    <input required type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?= isset($phone) ? $phone : ''; ?>">
</div>
<div class="form-group">
    <label for="class_name">Username</label>
    <input required type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= isset($username) ? $username : ''; ?>">
</div>