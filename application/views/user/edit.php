<?php
$username = $user_info->username;
$first_name = $user_info->first_name;
$last_name = $user_info->last_name;
$phone = $user_info->phone;
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit User</h4>
        </div>
        <form name="frmEditUser" method="post" action="<?= site_url('administration/edit_user/' . $user_info->user_id) ?>">
            <div class="modal-body">
                <?php include '_user_form.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->