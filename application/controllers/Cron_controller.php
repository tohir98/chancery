<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Cron_controller
 *
 * @author tohir
 * @property Basic_model $basic_model Description
 * @property Mailer $mailer Description
 * @property SmsAdapter $smsAdapter Description
 */
class Cron_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //Do your magic here
        $this->load->model('basic_model');
        $this->load->library(array('mailer', 'SmsAdapter'));
    }

    public function push_emails_on_queue() {

        //get all email queue whose dispatch_time is less than now
        $now = date('Y-m-d H:i:s', strtotime("now"));
        $emails = $this->basic_model->fetch_all_records('email_queue', ['dispatch_time <' => $now, 'sent' => 0, 'processed' => 0]);
        //send emails
        foreach ($emails as $email) {

            $email_data = json_decode($email->email_data, TRUE);

           $msg = $this->load->view($email->email_template, $email_data, TRUE);

            $q = static::sendEmail($email->subject, $msg, $email->to, REPLY_EMAIL, BUSINESS_NAME);

            if ($q) {
                $this->basic_model->update('email_queue', ['sent' => 1, 'processed' => 1], ['email_queue_id' => $email->email_queue_id]);
                self::cliOp("Successfully sent to :", $email->to);
            } else {
                $this->basic_model->update('email_queue', array('processed' => 1), array('email_queue_id' => $email->email_queue_id));
                self::cliOp("Not sent to :", $email->to);
            }
        }
        die();
    }

    public static function cliOp() {
        echo date('[Y-m-d H:i:s]') . ' [' . getmypid() . ']' . "\t" . join(' ', func_get_args()) . PHP_EOL;
    }

    public function personnelBirthdayNotification() {
        // get all personnel celebrating birthday today
        $today = date('Y-m-d');
        $todayBirthdays = $this->basic_model->fetchPersonnelUpcomingBirthdays($today, $today);
        $personnels = $this->basic_model->fetch_all_records('personnel', ['status' => PERSONNEL_ACTIVE]);

        if (!empty($todayBirthdays)) {
            foreach ($todayBirthdays as $entry) {
                $this->_processBirthdayMessage($entry, $personnels);
            }
        }
    }

    private function _processBirthdayMessage($object, $personnels) {
        // send sms to celebrant
        if (isset($object->phone_number_1) && strlen($object->phone_number_1) > 0) {
            $this->smsAdapter->processViaSMSRoute($object->phone_number_1, BIRTHDAY_MSG);
        }

        // send email to notify other staff
        if (!empty($personnels)) {
            foreach ($personnels as $personnel) {
                // push to email queue
                $this->mailer->add_queue([
                    'to' => $personnel->email,
                    'module_id' => PERSONNEL_MODULE,
                    'sender' => REPLY_EMAIL,
                    'sender_name' => 'CHANCERY NOTIFICATIONS',
                    'email_template' => 'email_templates/birthday',
                    'email_data' => json_encode(['fullname' => $object->surname . ' ' . $object->other_names, 'dob' => $object->dob, 'picture_url' => $object->picture_url]),
                    'subject' => "{$object->surname} {$object->other_names}'s Birthday Notification",
                ]);
            }
        }
    }

    /**
     * Sends email
     * @param type $subject
     * @param type $message
     * @param type $recepient
     * @param type $sender sender email
     * @param type $sender_name Sender name
     * @return boolean
     */
    public static function sendEmail($subject, $message, $recepient, $sender, $sender_name) {
        $post = [
            'subject' => $subject,
            'message' => $message,
            'recepient' => $recepient,
            'sender' => $sender,
            'sender_name' => $sender_name,
        ];

        $url = "http://crm.oaastudy.com/api/v1/Mail/sendmail";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $resp = curl_exec($ch);
        curl_close($ch);
        return true;
    }

}
