<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of admin_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Agent_model $agent_model Description
 * @property Transaction_model $t_model Description
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
        $this->load->model('setup/transaction_model', 't_model');
        $this->load->model('personnel/personnel_model', 'per_model');
        
    }

    public function dashboard() {
        $this->user_auth_lib->check_login();

        $data = array(
            'recent_deduc' => $this->t_model->fetch_liabilities(),
            'bank_accounts' => $this->basic_model->fetchBankAccounts(),
            'pending_receivables' => $this->t_model->getPendingReceivables(),
            'users_count' => $this->basic_model->fetch_all_records('users', ['status' => ACTIVE]),
            'account_charts' => $this->basic_model->fetch_all_records(TBL_ACCOUNT_CHART),
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR),
            'income_sources' => $this->basic_model->fetch_all_records(TBL_INCOME_SOURCE),
            'upcoming_birthdays' => $this->basic_model->fetchPersonnelUpcomingBirthdays(date('Y-m-d'), date('Y-12-t')),
            'personnel_types' => $this->per_model->getPersonnelTypes(),
                
        );
        $this->user_nav_lib->run_page('dashboard/dashboard', $data, 'Dashboard |' . BUSINESS_NAME);
    }

}
