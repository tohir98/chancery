<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of users_controller
 *
 * @category Controller
 * @author TOHIR Omoloye <otcleantech@gmail.com>
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 * @property Basic_model $basic_model Description
 * @property User_model $u_model Description
 */
class Users_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper(['user_nav_helper', 'auth_helper']);
        $this->load->model('user/User_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function index() {
        $this->login();
    }

    public function login() {
        
        $redirect_url = '';
        if ($this->uri->segment(2)) {
            $redirect_url .= $this->uri->segment(2) . '/';
        }

        if ($this->uri->segment(3)) {
            $redirect_url .= $this->uri->segment(3) . '/';
        }

        auth_save_next_url($redirect_url);
        
        $data = array(
            'email_message' => '',
            'password_message' => ''
        );
        $message = '';
        if (request_is_post()) {
            $response = $this->user_auth_lib->login(request_post_data());

            if (!$response) {
                $message = 'Invalid E-mail/password.';
            } else {
                
                if (auth_next_url()) {
                    redirect(auth_next_url());
                }
                switch ((int) $response['access_level']) {
                    case USER_TYPE_SUPER_ADMIN:
                        redirect('admin/dashboard');
                        break;
                    case USER_TYPE_AGENT:
                        redirect('admin/dashboard');
                        break;
                    case USER_TYPE_BRANCH:
                        redirect('admin/dashboard');
                        break;
                    default:
                        break;
                }
            }
        }

        $data['email_message'] = $message;
        $this->load->view('user/login', $data);
    }

    public function super_admin() {
        $data = array(
            'email_message' => '',
            'password_message' => ''
        );

        if (request_is_post()) {
            $response = $this->user_auth_lib->loginAdmin(request_post_data());

            switch ((int) $response['access_level']) {
                case USER_TYPE_SUPER_ADMIN:
                    redirect('admin/dashboard');
                    break;
                case USER_TYPE_AGENT:
                    redirect('agent/dashboard');
                    break;
                case USER_TYPE_STUDENT:
                    redirect('student/dashboard');
                    break;
                default:
                    break;
            }
        }

        $this->load->view('user/login', $data);
    }

    public function forgot_password() {
        echo 'Coming soon...';
    }

    public function logout() {
        $this->user_auth_lib->logout();
        redirect(site_url());
    }

    public function verify_email($user_id, $key, $ac = null) {
        $userInfo = $this->basic_model->fetch_all_records(TBL_USERS, ['user_id' => $user_id]);

        if (!$userInfo) {
            show_404('page', false);
        }

        $db_key = sha1($userInfo[0]->username . $user_id);

        $keyValid = FALSE;
        if ($db_key == $key) {
            $keyValid = TRUE;
            $user = $userInfo;
        }

        if (!$keyValid) {
            show_404('page', false);
        }

        $login_url = site_url('login');
        $login_title = 'Reset Password';

        $success_message = '';
        $error_message = '';

        if (request_is_post()) {

            $new_password = $this->input->post("password", TRUE);
            $confirm_password = $this->input->post("confirm_password", TRUE);

            if (strlen(trim($new_password)) < 6) {
                $error_message = 'Enter valid password (minimum 6 chars).';
            } elseif ($new_password != $confirm_password) {
                $error_message = 'Invalid confirmation.';
            } else {

                $data_ = [ 'password' => $this->user_auth_lib->encrypt($new_password)];

                if ($ac == 'acc') {
                    $data_['status'] = USER_STATUS_ACTIVE;
                }


                $this->u_model->save(
                    $data_, array(
                    'user_id' => $user_id
                    )
                );

                redirect(site_url('login'));

                $success_message = 'Password changed successfully.';
            }
        }

        $bdata = array(
            'login_url' => $login_url,
            'login_title' => $login_title,
            'error_message' => $error_message,
            'success_message' => $success_message,
            'message' => $error_message
        );

        $this->load->view("user/reset_password", $bdata);

        return true;
    }

    public function edit_profile() {
        echo 'Coming soon';
    }

    public function users() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('users', array_merge(request_post_data(), ['status' => 1, 'user_type' => 2]), TRUE)) {
                $this->user_auth_lib->log_user_action(' created a new user', 101);
                notify('success', 'User created successfully');
            } else {
                notify('error', 'Unable to create user. Pls try again');
            }
        }

        $data = array(
            'users' => $this->basic_model->fetch_all_records('users', ['status' => 1])
        );
        $this->user_nav_lib->run_page('user/user_mgt', $data, 'User Management |' . BUSINESS_NAME);
    }

    public function change_password() {
        $this->user_auth_lib->check_login();
        $where = ['user_id' => $this->user_auth_lib->get('user_id')];
        $user_info = $this->basic_model->fetch_all_records('users', $where)[0];

        if (request_is_post()) {
            $cur_pass = request_post_data()['cur_password'];
            $new_pass = request_post_data()['new_password'];

            if ($this->user_auth_lib->encrypt($cur_pass) !== $user_info->password) {
                notify('error', 'Current password is Invalid');
                return;
            } else {
                $this->basic_model->update('users', ['password' => $this->user_auth_lib->encrypt($new_pass)], $where);
                notify('success', 'Password changed successfully');
            }
            redirect(site_url('users/users'));
        }

        $data = array(
            'user_info' => $user_info
        );

        $this->user_nav_lib->run_page('user/change_password', $data, 'Change Password |' . BUSINESS_NAME);
    }

    public function delete_user($user_id) {
        $this->user_auth_lib->check_login();

        if (!is_numeric($user_id) || $user_id == '') {
            return false;
        }

        if ($this->basic_model->delete('users', ['user_id' => $user_id])) {
            $this->user_auth_lib->log_user_action(' deleted a user', 101);
            notify('success', 'User deleted successfully');
        } else {
            notify('error', 'Unable to delete user, pls try again');
        }

        redirect(site_url('administration/users'));
    }

    public function edit_user($user_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update('users', request_post_data(), ['user_id' => $user_id])) {
                $this->user_auth_lib->log_user_action(' updated a user info', 101);
                notify('success', 'User updated successfully');
            } else {
                notify('error', 'Unable to update user at moment, pls try again');;
            }
            redirect(site_url('administration/users'));
        }
        $data = array(
            'user_info' => $this->basic_model->fetch_all_records('users', ['user_id' => $user_id])[0]
        );

        $this->load->view('user/edit', $data);
    }
    
    public function user_logs() {
        $data = array(
            'user_logs' => $this->u_model->fetch_user_logs()
        );
        
        $this->user_nav_lib->run_page('user/user_logs', $data, 'User logs | '. BUSINESS_NAME );
    }

}
