<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of transaction_controller
 *
 * @author TOHIR
 * 
 * @property Setup_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property Transaction_model $t_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 */
class Transaction_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('setup/transaction_model', 't_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function assets() {
        $this->user_auth_lib->check_login();

        $data = array(
            'assets' => $this->t_model->fetch_assets()
        );

        $this->user_nav_lib->run_page('transaction/assets/list', $data, 'Assets | ' . BUSINESS_NAME);
    }
    
    public function incomes() {
        $this->user_auth_lib->check_login();

        $data = array(
            'incomes' => $this->t_model->fetch_incomes(),
            'income_types' => $this->user_auth_lib->get_income_types()
        );

        $this->user_nav_lib->run_page('transaction/income/incomes', $data, 'Incomes | ' . BUSINESS_NAME);
    }

    public function new_asset() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->t_model->saveTransaction(TBL_ASSETS, request_post_data())) {
                $this->user_auth_lib->log_user_action(' added an asset', 201);
                notify('success', 'Asset added successfully');
            } else {
                notify('error', 'Unable to add asset at moment, pls try again later');
            }

            redirect(site_url('/transaction/assets'));
        }

        $data = array(
            'income_accounts' => $this->basic_model->fetch_asset_accounts(),
            'bank_accounts' => $this->basic_model->fetchBankAccounts(),
        );

        $this->user_nav_lib->run_page('transaction/assets/new_asset', $data, 'New Asset | ' . BUSINESS_NAME);
    }

    public function add_receivable() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $income_id = $this->basic_model->insert(TBL_INCOME, array_merge(request_post_data(), ['created_by' => $this->user_auth_lib->get('user_id'),
                'date_created' => date('Y-m-d h:i:s'),
                'income_date' => date('Y-m-d', strtotime(request_post_data()['income_date'])),
                'due_date' => date('Y-m-d', strtotime(request_post_data()['due_date']))
                ]));

            if ($income_id > 0) {
                $this->t_model->add_receivable($income_id, request_post_data());
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Error ! An problem occured while saving receivable');
            }

            redirect(site_url('transaction/incomes'));
        }

        $data = array(
            'accounts' => $this->basic_model->fetch_income_receivables(),
            'income_sources' => $this->basic_model->fetch_all_records('income_sources'),
            'title' => "Receivables",
            'to_hide' => ['bank'],
            'quarters' => $this->user_auth_lib->get_quarters(),
            'income_type' => INCOME_RECEIVABLE
        );

        $this->user_nav_lib->run_page('transaction/income/add_income', $data, 'Add Receivable Income |' . BUSINESS_NAME);
    }

    public function add_cash_receipts() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->insert(TBL_INCOME, array_merge(request_post_data(), ['created_by' => $this->user_auth_lib->get('user_id'),
                        'date_created' => date('Y-m-d h:i:s'),
                        'income_date' => date('Y-m-d', strtotime(request_post_data()['income_date']))]))) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Error ! An problem occured while saving receivable');
            }

            redirect(site_url('transaction/incomes'));
        }

        $data = array(
            'accounts' => $this->basic_model->fetch_income_accounts(),
            'cash_accts' => $this->basic_model->fetch_cash_accounts(),
            'income_sources' => $this->basic_model->fetch_all_records('income_sources'),
            'title' => "Cash Receipts",
            'to_hide' => ['due_date', 'bank'],
            'quarters' => $this->user_auth_lib->get_quarters(),
            'income_type' => INCOME_CASH_RECEIPT
        );

        $this->user_nav_lib->run_page('transaction/income/add_income', $data, 'Add Receivable Income |' . BUSINESS_NAME);
    }

    public function add_deposit() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            
            if ($this->t_model->process_deposit(request_post_data())) {
                notify('success', 'Deposit saved successfully');
            } else {
                notify('error', 'Error ! An problem occured while adding deposit');
            }

            redirect(site_url('transaction/incomes'));
        }

        $data = array(
            'accounts' => $this->basic_model->fetch_income_receivables(),
            'cash_accts' => $this->basic_model->fetch_cash_accounts(),
            'income_sources' => $this->basic_model->fetch_all_records('income_sources'),
            'title' => "Paid Receivables",
            'quarters' => $this->user_auth_lib->get_quarters(),
            'to_hide' => ['due_date'],
            'income_type' => INCOME_DEPOSIT
        );

        $this->user_nav_lib->run_page('transaction/income/add_deposit', $data, 'Add Deposit |' . BUSINESS_NAME);
    }
    
    public function getARBalance($account_chart_id, $income_soure_id) {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->t_model->getARBalance($account_chart_id, $income_soure_id) ? : []));
    }
    
    public function getCashBalance($account_chart_id, $income_source_id) {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->t_model->getCashBalance($account_chart_id, $income_source_id)));
    }
    
    public function getBankBalance($account_chart_id) {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->t_model->getBankBalance($account_chart_id)));
    }
    
    public function load_month($quarter_id) {
        $result = '<select name="month_id" id="month_id" style="width: 200px">';
        if ($quarter_id == FIRST_QUARTER){
            $result .= '<option value=1>Jan</option>';
            $result .= '<option value=2>Feb</option>';
            $result .= '<option value=3>Mar</option>';
        }
        elseif ($quarter_id == SECOND_QUARTER){
            $result .= '<option value=4>Apr</option>';
            $result .= '<option value=5>May</option>';
            $result .= '<option value=6>Jun</option>';
        }
        elseif ($quarter_id == THIRD_QUARTER){
            $result .= '<option value=7>Jul</option>';
            $result .= '<option value=8>Aug</option>';
            $result .= '<option value=9>Sep</option>';
        }
        elseif ($quarter_id == FOURTH_QUARTER){
            $result .= '<option value=10>Oct</option>';
            $result .= '<option value=11>Nov</option>';
            $result .= '<option value=12>Dec</option>';
        }
        
        echo $result . "</select>";
    }

}
