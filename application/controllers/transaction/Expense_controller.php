<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of transaction_controller
 *
 * @author TOHIR
 * 
 * @property Setup_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property Transaction_model $t_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 */

class Expense_controller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('setup/transaction_model', 't_model');
        $this->load->model('basic_model', 'basic_model');
    }
    
    public function index() {
        $this->user_auth_lib->check_login();

        $data = array(
            'expenses' => $this->t_model->fetch_expenses(),
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR),
            'expense_types' => $this->t_model->getexpenseTypes()
        );

        $this->user_nav_lib->run_page('transaction/expenditure/expenses', $data, 'Expenses | ' . BUSINESS_NAME);
    }
    
    public function add_payable() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->t_model->addExpense(EXPENSE_PAYABLE, request_post_data())) {
                $this->user_auth_lib->log_user_action(' added a payable expense', 201);
                notify('success', 'Payable added successfully');
            } else {
                notify('error', 'Unable to add expense at moment, pls try again later');
            }

            redirect(site_url('/transaction/expenses'));
        }

        $data = array(
            'l_accounts' => $this->basic_model->fetch_expense_accounts(),
            'ap_accounts' => $this->basic_model->fetch_liability_accounts(),
            'cash_accts' => $this->basic_model->fetch_cash_accounts(),
            'bank_accounts' => $this->basic_model->fetchBankAccounts(),
            'expense_categories' => $this->basic_model->fetch_expense_accounts(),
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR, ['status' => ACTIVE]),
            'quarters' => $this->user_auth_lib->get_quarters(),
            'type' => EXPENSE_PAYABLE,
            'page_title' => 'Invoices/Payables'
        );

        $this->user_nav_lib->run_page('transaction/expenditure/new', $data, 'Invoices/Payables | ' . BUSINESS_NAME);
    }
    
    public function add_disbursement() {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()) {
            if ($this->t_model->addExpense(EXPENSE_DISBURSEMENT, request_post_data())) {
                $this->user_auth_lib->log_user_action(' disbursed funds', 201);
                notify('success', 'Disbursement added successfully');
            } else {
                notify('error', 'Unable to disburse at moment, pls try again later');
            }

            redirect(site_url('/transaction/expenses'));
        }
        
        $data = array(
            'l_accounts' => $this->basic_model->fetch_expense_accounts(),
            'ap_accounts' => $this->basic_model->fetch_liability_accounts(),
            'cash_accts' => $this->basic_model->fetch_cash_accounts(),
            'bank_accounts' => $this->basic_model->fetchBankAccounts(),
            'expense_categories' => $this->basic_model->fetch_expense_accounts(),
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR, ['status' => ACTIVE]),
            'quarters' => $this->user_auth_lib->get_quarters(),
            'type' => EXPENSE_DISBURSEMENT,
            'page_title' => 'Disbursements'
        );

        $this->user_nav_lib->run_page('transaction/expenditure/new', $data, 'Disbursements | ' . BUSINESS_NAME);
    }
    
    public function add_paid_invoices() {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()) {
            if ($this->t_model->addExpense(EXPENSE_PAID_INVOICES, request_post_data())) {
                $this->user_auth_lib->log_user_action(' paid an invoice', 201);
                notify('success', 'Operation was successfully');
            } else {
                notify('error', 'Unable to complete your transaction at moment, pls try again later');
            }

            redirect(site_url('/transaction/expenses'));
        }
        
        $data = array(
            'l_accounts' => $this->basic_model->fetch_expense_accounts(),
            'ap_accounts' => $this->basic_model->fetch_liability_accounts(),
            'cash_accts' => $this->basic_model->fetch_cash_accounts(),
            'bank_accounts' => $this->basic_model->fetchBankAccounts(),
            'expense_categories' => $this->basic_model->fetch_expense_accounts(),
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR, ['status' => ACTIVE]),
            'quarters' => $this->user_auth_lib->get_quarters(),
            'type' => EXPENSE_PAID_INVOICES,
            'page_title' => 'Paid Invoices'
        );

        $this->user_nav_lib->run_page('transaction/expenditure/new', $data, 'Paid Invoice | ' . BUSINESS_NAME);
    }
    
    public function getAPBalance($ap_account_id, $vendor_id) {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->t_model->getAPBalance($ap_account_id, $vendor_id)));
    }
    
    public function getBankBalance($account_chart_id) {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->t_model->getBankBalance($account_chart_id)));
    }
    
}
