<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of setup_controller
 *
 * @author TOHIR
 * 
 * @property Setup_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property mailer $mailer Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Employee_model $emp_model Description
 * @property CI_Input $input Description
 */
class Setup_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('setup/employee_model', 'emp_model');
        $this->load->model('basic_model', 'basic_model');

        $this->user_auth_lib->check_login();
    }

    public function banks() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->insert(TBL_BANK_ACCOUNT, array_merge(request_post_data(), ['date_added' => date('Y-m-d'), 'added_by' => $this->user_auth_lib->get('user_id'), 'status' => ACTIVE]))) {
                notify('success', 'Bank account saved successfully');
            } else {
                notify('success', 'Unable to save bank account at moment, pls try again');
            }
        }

        $data = [
            'bank_accounts' => $this->s_model->fetchBankAccounts(),
            'banks' => $this->basic_model->fetch_all_records(TBL_BANKS),
            'account_types' => $this->basic_model->get_account_types()
        ];
        $this->user_nav_lib->run_page('setup/bank/banks', $data, 'Banks | ' . BUSINESS_NAME);
    }

    public function vendors() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->insert(TBL_BANK_ACCOUNT, array_merge(request_post_data(), ['date_added' => date('Y-m-d'), 'added_by' => $this->user_auth_lib->get('user_id'), 'status' => ACTIVE]))) {
                notify('success', 'Bank account saved successfully');
            } else {
                notify('success', 'Unable to save bank account at moment, pls try again');
            }
        }

        $data = [
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR, ['status' => ACTIVE]),
            'statuses' => $this->basic_model->get_statuses()
        ];
        $this->user_nav_lib->run_page('setup/vendors/list', $data, 'Vendors | ' . BUSINESS_NAME);
    }

    public function expense_types() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveExpenseType(request_post_data())) {
                notify('success', 'Expense type saved successfully');
            } else {
                notify('error', 'Unable to save expense type at moment, pls try again');
            }
        }

        $data = [
            'expense_types' => $this->basic_model->fetch_all_records('expense_types')
        ];
        $this->user_nav_lib->run_page('setup/expense/types', $data, 'Expense Types | ' . BUSINESS_NAME);
    }

    public function accounts() {
        $this->user_auth_lib->check_login();

        $data = [
            'account_categories' => $this->basic_model->fetch_all_records('account_category'),
            'account_sub_categories' => $this->basic_model->fetch_all_records('account_sub_category'),
            'accounts' => $this->s_model->fetch_all_accounts()
        ];
        $this->user_nav_lib->run_page('setup/account/list', $data, 'Accounts | ' . BUSINESS_NAME);
    }

    public function add_account() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveAccount(request_post_data())) {
                notify('success', 'Account created successfully');
            } else {
                notify('error', 'Unable to created account, Pls try again');
            }
            redirect(site_url('/setup/accounts'));
        }

        $data = array(
            'categories' => $this->basic_model->fetch_all_records('account_category'),
            'heading_types' => $this->basic_model->fetch_all_records(TBL_HEADING_TYPE),
        );
        $this->user_nav_lib->run_page('setup/account/add_account', $data, 'Add Accounts | ' . BUSINESS_NAME);
    }

    public function get_sub_category() {
        $category_id = $this->uri->segment(3);

        $sub_categories = $this->basic_model->fetch_all_records('account_sub_category', ['account_category_id' => $category_id]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$sub_categories) {
            $data[] = ['id' => '-1', 'name' => "[Sub categories not found]"];
        } else {
            foreach ($sub_categories as $sub) {
                $data[] = ['id' => $sub->account_sub_category_id, 'name' => ucfirst($sub->sub_category)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function delete_account($id) {
        $this->user_auth_lib->check_login();
        if ($this->s_model->deleteAccount($id)) {
            notify('success', 'Account deleted successfully');
        } else {
            notify('error', 'Unable to delete account at moment, pls try again');
        }
        redirect(site_url('/setup/accounts'));
    }

    public function deleteIncomeType($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete('income_types', ['income_type_id' => $id])) {
            notify('success', 'Income type deleted successfully');
        } else {
            notify('error', 'Unable to delete income type at moment, pls try again');
        }
        redirect(site_url('/setup/income_types'));
    }

    public function deleteExpenseType($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete('expense_types', ['expense_type_id' => $id])) {
            notify('success', 'Expense type deleted successfully');
        } else {
            notify('error', 'Unable to delete expense type at moment, pls try again');
        }
        redirect(site_url('/setup/expense_types'));
    }

    public function deleteVendor($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(TBL_VENDOR, ['vendor_id' => $id])) {
            notify('success', 'Vendor deleted successfully');
        } else {
            notify('error', 'Unable to delete vendor at moment, pls try again');
        }
        redirect(site_url('/setup/vendors'));
    }

    public function deleteIncomeSource($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete('income_sources', ['income_source_id' => $id])) {
            notify('success', 'Income source deleted successfully');
        } else {
            notify('error', 'Unable to delete income source at moment, pls try again');
        }
        redirect(site_url('/setup/income_sources'));
    }

    public function deleteBankAccount($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(TBL_BANK_ACCOUNT, ['bank_account_id' => $id])) {
            notify('success', 'Bank account deleted successfully');
        } else {
            notify('error', 'Unable to delete bank account at moment, pls try again');
        }
        redirect(site_url('/setup/banks'));
    }

    public function income_types() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveIncomeType(request_post_data())) {
                notify('success', 'Income type saved successfully');
            } else {
                notify('error', 'Unable to save income type at moment, pls try again');
            }
        }

        $data = [
            'income_types' => $this->basic_model->fetch_all_records('income_types')
        ];
        $this->user_nav_lib->run_page('setup/income/types', $data, 'Income Types | ' . BUSINESS_NAME);
    }

    public function editIncomeType($id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update('income_types', request_post_data(), ['income_type_id' => $id])) {
                $this->user_auth_lib->log_user_action(' updated income type', 101);
                notify('success', 'Income type updated successfully');
            } else {
                notify('error', 'Unable to update income type at moment, pls try again');
            }
            redirect(site_url('/setup/income_types'));
        }
        $data = array(
            'income_typ' => $this->basic_model->fetch_all_records('income_types', ['income_type_id' => $id])[0]
        );

        $this->load->view('setup/income/edit', $data);
    }

    public function editExpenseType($id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update('expense_types', request_post_data(), ['expense_type_id' => $id])) {
                $this->user_auth_lib->log_user_action(' updated expense type', 101);
                notify('success', 'Expense type updated successfully');
            } else {
                notify('error', 'Unable to update expense type at moment, pls try again');
            }
            redirect(site_url('/setup/expense_types'));
        }
        $data = array(
            'expense_typ' => $this->basic_model->fetch_all_records('expense_types', ['expense_type_id' => $id])[0]
        );

        $this->load->view('setup/expense/edit', $data);
    }

    public function editIncomeSource($id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update('income_sources', request_post_data(), ['income_source_id' => $id])) {
                $this->user_auth_lib->log_user_action(' updated income source', 101);
                notify('success', 'Income source updated successfully');
            } else {
                notify('error', 'Unable to update income source at moment, pls try again');
            }
            redirect(site_url('/setup/income_sources'));
        }
        $data = array(
            'income_source' => $this->basic_model->fetch_all_records('income_sources', ['income_source_id' => $id])[0]
        );

        $this->load->view('setup/income/edit_source', $data);
    }

    public function editBankAccount($id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_BANK_ACCOUNT, request_post_data(), ['bank_account_id' => $id])) {
                $this->user_auth_lib->log_user_action(' updated bank account', 301);
                notify('success', 'Bank account updated successfully');
            } else {
                notify('error', 'Unable to update bank account at moment, pls try again');
            }
            redirect(site_url('/setup/banks'));
        }
        $data = array(
            'account_info' => $this->basic_model->fetch_all_records(TBL_BANK_ACCOUNT, ['bank_account_id' => $id])[0],
            'banks' => $this->basic_model->fetch_all_records(TBL_BANKS),
            'account_types' => $this->basic_model->get_account_types()
        );

        $this->load->view('setup/bank/edit_account', $data);
    }

    public function income_sources() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveIncomeType(request_post_data())) {
                notify('success', 'Income type saved successfully');
            } else {
                notify('error', 'Unable to save income type at moment, pls try again');
            }
        }

        $data = [
            'income_sources' => $this->basic_model->fetch_all_records('income_sources')
        ];
        $this->user_nav_lib->run_page('setup/income/sources', $data, 'Income Sources | ' . BUSINESS_NAME);
    }

    public function add_income_source() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveIncomeSource(request_post_data())) {
                notify('success', 'Income source saved successfully');
            } else {
                notify('error', 'Unable to save income source at moment, pls try again');
            }

            redirect(site_url('/setup/income_sources'));
        }


        $data = [];

        $this->user_nav_lib->run_page('setup/income/add_source', $data, 'New Income Source | ' . BUSINESS_NAME);
    }

    public function add_vendor() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->vendor_exits(request_post_data()['vendor_name'])) {
                notify('error', 'Vendor already exist.');
                redirect(site_url('/setup/vendors'));
            }
            if ($this->s_model->saveVendor(request_post_data())) {
                notify('success', "Vendor saved successfully");
            } else {
                notify('error', 'Unable to save vendor at moment, Pls try again');
            }

            redirect(site_url('/setup/vendors'));
        }

        $data = array(
            'states' => $this->basic_model->fetch_all_records('states'),
            'statuses' => $this->basic_model->get_statuses()
        );

        $this->user_nav_lib->run_page('setup/vendors/add', $data, 'New Vendor | ' . BUSINESS_NAME);
    }

    public function editVendor($vendor_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $data = request_post_data();
            $data['vendor_since'] = date('Y-m-d', strtotime($data['vendor_since']));
            if ($this->basic_model->update(TBL_VENDOR, $data, ['vendor_id' => $vendor_id])) {
                $this->user_auth_lib->log_user_action(' updated vendor information', 301);
                notify('success', 'Vendor information updated successfully');
            } else {
                notify('error', 'Unable to update vendor info at moment, pls try again');
            }
            redirect(site_url('/setup/vendors'));
        }

        $data = array(
            'vendor_info' => $this->basic_model->fetch_all_records(TBL_VENDOR, ['vendor_id' => $vendor_id])[0],
            'states' => $this->basic_model->fetch_all_records('states'),
        );

        $this->load->view('setup/vendors/edit_vendor', $data);
    }

    public function fixed_assets() {
        $this->user_auth_lib->check_login();

        $data = [
            'assets' => $this->s_model->fetchAssets(),
            'accounts' => $this->basic_model->fetch_asset_accounts()
        ];
        $this->user_nav_lib->run_page('setup/assets/fixed_assets', $data, 'Fixed Assets | ' . BUSINESS_NAME);
    }

    public function new_asset() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->s_model->saveAsset(request_post_data())) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Ops! An error occured while saving fixed asset, pls try again');
            }
            redirect(site_url('/setup/fixed_assets'));
        }

        $data = [
            'accounts' => $this->basic_model->fetch_asset_accounts()
        ];
        $this->user_nav_lib->run_page('setup/assets/new_asset', $data, 'Fixed Assets | ' . BUSINESS_NAME);
    }

    public function edit_asset($ref_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {

            $data = request_post_data();
            $data['purchase_date'] = date('Y-m-d', strtotime(request_post_data()['purchase_date']));

            if ($this->basic_model->update(TBL_ASSETS, $data, ['ref_id' => $ref_id])) {
                $this->user_auth_lib->log_user_action(" updated asset with ref : $ref_id", 300);
                notify('success', 'Asset edited successfully');
            } else {
                notify('error', 'Ops! An error occured while saving fixed asset, pls try again');
            }
            redirect(site_url('/setup/fixed_assets'));
        }

        $data = [
            'asset_detail' => $this->basic_model->fetch_all_records(TBL_ASSETS, ['ref_id' => $ref_id])[0],
            'accounts' => $this->basic_model->fetch_asset_accounts()
        ];
        $this->user_nav_lib->run_page('setup/assets/new_asset', $data, 'Edit Assets | ' . BUSINESS_NAME);
    }

    public function delete_asset($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(TBL_ASSETS, ['asset_id' => $id])) {
            $this->user_auth_lib->log_user_action(" deleted asset with id : $id", 300);
            notify('success', 'Asset deleted successfully');
        } else {
            notify('error', 'Unable to delete asset at moment, pls try again');
        }
        redirect(site_url('/setup/fixed_assets'));
    }

    public function opening_balance($account_chart_id) {
        $this->user_auth_lib->check_login();
        if (!is_numeric($account_chart_id)) {
            notify('error', 'Invalid account selected');
        }

        if (request_is_post()) {
            if ($this->s_model->saveOpeningBalance(request_post_data())) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Oops! Unable to complete your request at moment, please try again later.');
            }
            redirect(site_url("setup/opening_balance/{$account_chart_id}"));
        }

        $pageData = [
            'balances' => $this->s_model->fetchAccountBalance($account_chart_id),
            'account_info' => $this->basic_model->fetch_all_records(TBL_ACCOUNT_CHART, ['account_chart_id' => $account_chart_id]),
            'account_chart_id' => $account_chart_id
        ];

        $this->user_nav_lib->run_page('setup/account/balances', $pageData, 'Opening/Closing Balance | ' . BUSINESS_NAME);
    }

    public function deleteAccountBalance($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(TBL_ACCOUNT_BALANCE, ['account_balance_id' => $id])) {
            notify('success', 'Account Opening/Closing Balance Deleted Successfully');
        } else {
            notify('error', 'Unable to delete account balance, please try again');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function editAccountBalance($id, $account_chart_id) {
        if (request_is_post()) {
            if ($this->basic_model->update(TBL_ACCOUNT_BALANCE, request_post_data(), ['account_balance_id' => $id])) {
                $this->user_auth_lib->log_user_action(' updated opening/closing account', 101);
                notify('success', 'Account balance updated successfully');
            } else {
                notify('error', 'Unable to update account opening/closing balance at moment, pls try again');
            }
            redirect(site_url('/setup/opening_balance/' . $account_chart_id));
        }

        $data = array(
            'account_chart_id' => $account_chart_id,
            'account_bal' => $this->basic_model->fetch_all_records(TBL_ACCOUNT_BALANCE, ['account_balance_id' => $id])[0]
        );

        $this->load->view('setup/account/edit_balance', $data);
    }
    
}
