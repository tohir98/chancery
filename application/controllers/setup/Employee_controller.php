<?php

/**
 * Description of Employee_controller
 *
 * @author tohir
 * @property Setup_model $s_model Description
 */
require_once 'Setup_controller.php';

class Employee_controller extends Setup_controller {

    public function employees() {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->log_user_action(" viewed employees page", 120);
        $data = [
            'employees' => $this->emp_model->fetchAllEmployees(null, null, \employees\Employee::class),
            'statuses' => Employee_model::getStatuses(),
            'genders' => Employee_model::getGenders(),
            'job_types' => Employee_model::getJobTypes(),
        ];

        $this->user_nav_lib->run_page('setup/employee/list', $data, "Employees | " . BUSINESS_NAME);
    }

    public function add_employee() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->emp_model->add_employee(request_post_data())) {
                notify('success', "Employee added successfully");
            } else {
                notify('error', "Unable to add employee at moment, pls try again");
            }
            redirect(site_url('/setup/employees'));
        }

        $data = [
            'states' => $this->basic_model->fetch_all_records('states'),
            'banks' => $this->basic_model->fetch_all_records('banks'),
            'relationships' => $this->basic_model->fetch_all_records('relationship'),
            'job_types' => Employee_model::getJobTypes(),
        ];

        $this->user_nav_lib->run_page('setup/employee/add_employee', $data, "Add Employee | " . BUSINESS_NAME);
    }

    public function deleteEmployee($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(TBL_EMPLOYEES, ['employee_id' => $id])) {
            notify('success', "Employee deleted successfully");
        } else {
            notify('error', "Ops! An error occured while deleting employee, pls try again");
        }

        redirect(site_url('/setup/employees'));
    }

    public function edit_status() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->emp_model->updateEmployee(request_post_data())) {
                notify('success', "Employee status updated successfully");
            } else {
                notify('error', "Unable to update employee status, pls try again");
            }
            redirect(site_url('/setup/employees'));
        }
    }

    public function editEmployee($id) {
        $this->user_auth_lib->check_login();
        $redirect_url = site_url('/setup/employees');
        if (!isset($id) || !is_numeric($id)) {
            notify('error', "Invalid employee ID was passed.", $redirect_url);
        }

        if (request_is_post()) {
            if ($this->emp_model->update_employee($id, request_post_data())) {
                notify('success', 'Employee record was updated successfully', $redirect_url);
            } else {
                notify('error', 'Ops! An error occured while updating Employee\"s record, pls try again', $redirect_url);
            }
        }

        $data = [
            'states' => $this->basic_model->fetch_all_records('states'),
            'banks' => $this->basic_model->fetch_all_records('banks'),
            'relationships' => $this->basic_model->fetch_all_records('relationship'),
            'job_types' => Employee_model::getJobTypes(),
            'emp_info' => $this->emp_model->fetchEmp($id),
            'title' => 'Edit Employee Information'
        ];

        $this->user_nav_lib->run_page('setup/employee/add_employee', $data, "Edit Employee | " . BUSINESS_NAME);
    }

    public function view_record($employee_id) {
        $this->user_auth_lib->check_login();

        $profile = $this->emp_model->fetchAllEmployees($employee_id);
        
        $profile_content = $this->load->view('setup/employee/profile_page_contents/profile', array(
            'record' => $profile[0]
                ), true);

        $contact_details_content = $this->load->view('setup/employee/profile_page_contents/contact_details', array(
            'contact_details' => $profile[0]
                ), true);
        
        $bank_details_content = $this->load->view('setup/employee/profile_page_contents/bank_details', array(
            'bank_details' => array(
                'Bank' => $profile[0]->name,
                'Account Name' => $profile[0]->account_name,
                'Account Number' => $profile[0]->account_no,
            )
                ), true);
        
        $emergency_content = $this->load->view('setup/employee/profile_page_contents/emergency_details', array(
            'emergency_details' => array(
                'Emergency Contact' => $profile[0]->emergency_name,
                'Phone' => $profile[0]->emergency_phone,
                'Relationship' => $profile[0]->relationship,
            )
                ), true);

        $data = array(
            'profile_content' => $profile_content,
            'contact_details_content' => $contact_details_content,
            'bank_details_content' => $bank_details_content,
            'emergency_content' => $emergency_content,
            'profile_picture' => $profile[0]->profile_picture_url,
            'employees' => $this->emp_model->fetchAllEmployees(null, null, \employees\Employee::class),
        );
        $this->user_nav_lib->run_page('setup/employee/profile_page', $data, 'Applicant Record | ' . BUSINESS_NAME);
    }
    
    public function change_emp_picture($employee_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if (request_is_post()) {
                if ($this->emp_model->update_employee_picture($employee_id, $_FILES['userfile'])) {
                    notify('success', "Employee\"s picture updated successfully");
                } else {
                    notify('error', "Unable to update employee\"s image at moment, pls try again");
                }
                redirect(site_url('/setup/view_record/' . $employee_id));
            }
        }

        $data = [
            'profile_picture' => $this->emp_model->fetchAllEmployees($employee_id),
            'employee_id' => $employee_id
        ];

        $this->user_nav_lib->run_page('setup/employee/edit_profile_picture', $data, 'Change Profile Picture | ' . BUSINESS_NAME);
    }
    
    public function remove_emp_picture($employee_id) {
        $this->user_auth_lib->check_login();
        
        if ($this->emp_model->remove_profile_picture($employee_id)){
            notify('success', 'Operation was successful');
        }else{
            notify('error', 'Something went wrong while updating profile picture');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }
    

}
