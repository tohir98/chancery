<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Report_controller
 *
 * @author tohir
 * @property Setup_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property Report_model $r_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Report_lib $report_lib Description
 * @property Transaction_model $t_model Description
 */
class Report_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'report_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->helper('date_helper');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('setup/transaction_model', 't_model');
        $this->load->model('report/report_model', 'r_model');
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->load->model('basic_model', 'basic_model');
        
        $this->user_auth_lib->check_login();
    }

    public function income_report() {
        $this->user_auth_lib->check_login();
        $pageData = [
            'income_sources' => $this->basic_model->fetch_all_records('income_sources'), //income source
            'income_accounts' => $this->basic_model->fetch_income_accounts(),
            'income_types' => $this->user_auth_lib->get_income_types(),
            'bank_accounts' => $this->basic_model->fetch_cash_accounts(),
            'heading_types' => $this->basic_model->fetch_all_records(TBL_HEADING_TYPE),
        ];

        $this->user_nav_lib->run_page('report/income_report', $pageData, 'Income Report | ' . BUSINESS_NAME);
    }
    
    public function expense_report() {
        $this->user_auth_lib->check_login();
        $pageData = [
            'vendors' => $this->basic_model->fetch_all_records(TBL_VENDOR),
            'expense_accounts' => $this->basic_model->fetch_expense_accounts(),
            'expense_types' => $this->t_model->getexpenseTypes(),
            'bank_accounts' => $this->basic_model->fetch_cash_accounts(),
            'heading_types' => $this->basic_model->fetch_all_records(TBL_HEADING_TYPE),
        ];

        $this->user_nav_lib->run_page('report/expense_report', $pageData, 'Expense Report | ' . BUSINESS_NAME);
    }

    public function income_report_json() {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->r_model->fetch_incomes($_GET['source'], $_GET['account_chart'], $_GET['income_type'], $_GET['cash_account'], $_GET['heading_type'], $_GET['start_date'], $_GET['end_date'] )));
    }
    
    public function expense_report_json() {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->r_model->fetch_expenses($_GET['payee'], $_GET['account_chart'], $_GET['expense_type'], $_GET['cash_account'], $_GET['heading_type'], $_GET['start_date'], $_GET['end_date'] )));
    }
    
    public function payable_report_json() {
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->r_model->fetch_expenses(null, $_GET['account_chart'], null,null, null, $_GET['start_date'], $_GET['end_date'] )));
    }
    
    public function income_report_excel() {
        $this->report_lib->income_report_excel($_GET['source'], $_GET['account_chart'], $_GET['income_type'], $_GET['cash_account'], $_GET['heading_type'], $_GET['start_date'], $_GET['end_date']);
    }
    
    public function expense_report_excel() {
        $this->report_lib->expense_report_excel($_GET['payee'], $_GET['account_chart'], $_GET['expense_type'], $_GET['cash_account'], $_GET['heading_type'], $_GET['start_date'], $_GET['end_date']);
    }
    
    public function receivables() {
        $pageData = [
            'ar_accounts' => $this->basic_model->fetch_income_receivables()
        ];
        
        $this->user_nav_lib->run_page('report/receivables', $pageData, 'Receivables Report | ' . BUSINESS_NAME);
    }
    
    public function payables() {
        $pageData = [
            'ap_accounts' => $this->basic_model->fetch_liability_accounts(),
        ];
        
        $this->user_nav_lib->run_page('report/payables', $pageData, 'Payables Report | ' . BUSINESS_NAME);
    }
    
    public function paid_invoices() {
        $pageData = [
            'ap_accounts' => $this->basic_model->fetch_liability_accounts(),
        ];
        
        $this->user_nav_lib->run_page('report/paid_invoices', $pageData, 'Payable/Invoice Report | ' . BUSINESS_NAME);
    }
    
    public function banks() {
        $data = [
            'bank_accounts' => $this->s_model->fetchBankAccounts(),
            'account_types' => $this->basic_model->get_account_types()
        ];
        $this->user_nav_lib->run_page('report/banks', $data, 'Banks Report | ' . BUSINESS_NAME);
    }
    
    public function download_bank_xls() {
        $this->report_lib->bank_report_excel($this->basic_model->get_account_types(), $this->s_model->fetchBankAccounts());
    }
    
    public function payroll() {
        $pageData = [
            'records' => $this->p_model->fetchRecords()
        ];
        $this->user_nav_lib->run_page('report/payroll_record', $pageData, ' Payroll Records | ' . BUSINESS_NAME);
    }
    
    public function fixed_assets() {
        $data = [
            'assets' => $this->s_model->fetchAssets(),
        ];
        $this->user_nav_lib->run_page('report/fixed_assets', $data, 'Fixed Assets | ' . BUSINESS_NAME);
    }
    
    public function download_fixed_asset_xls() {
        $this->report_lib->asset_report_excel($this->s_model->fetchAssets());
    }
    
    public function download_liability_xls() {
        $this->report_lib->liability_report_excel($this->s_model->fetchLiabilities());
    }
    
    public function liabilities() {
        $data = [
            'liabilities' => $this->s_model->fetchLiabilities(),
        ];
        $this->user_nav_lib->run_page('report/liabilities', $data, 'Liabilities | ' . BUSINESS_NAME);
    }

}
