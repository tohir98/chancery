<?php

/**
 * Description of Personnel_controller
 *
 * @author tohir
 * @property Personnel_model $per_model Description
 * @property Basic_model $basic_model Description
 * @property CI_Loader $load Description
 * @property CI_Input $input Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Personnel_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(['user_nav_lib', 'cloudinary_lib', 'SmsAdapter', 'mailer']);

        $this->load->helper('user_nav_helper');
        $this->load->helper('date');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('personnel/personnel_model', 'per_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function profiles() {
        $this->user_auth_lib->check_login();
        $currentAss = [];
        $profiles = $this ->per_model->fetchProfiles();
        
        if (!empty($profiles)) {
            foreach ($profiles as $profile) {
                if ($profile->current_assignment) {
                    array_push($currentAss, $profile->current_assignment);
                }
            }
        }
        
        $data = [
            'profiles' => $profiles,
            'personnel_types' => $this->per_model->getPersonnelTypes(),
            'personnel_statuses' => Personnel_model::getPersonnelStatuses(),
            'current_assignments' => array_unique($currentAss)
        ];

        $this->user_nav_lib->run_page('personnel/list', $data, "Profile | " . BUSINESS_NAME);
    }

    public function add_profile() {

        if (request_is_post()) {
            $error = '';
            if ($this->per_model->savePersonnel(request_post_data(), $_FILES['userfile'], $error)) {
                notify('success', 'Information saved successfully');
            } else {
                notify('error', "Error ! {$error}");
            }
            redirect(site_url('/personnel/add_profile'));
        }

        $data = [
            'states' => $this->basic_model->fetch_all_records('states'),
            'positions' => $this->basic_model->fetch_all_records('positions'),
            'pastoral_positions' => $this->basic_model->fetch_all_records('pastoral_positions'),
            'current_positions' => $this->basic_model->fetch_all_records('current_positions'),
            'admin_offices' => $this->basic_model->fetch_all_records('admin_offices'),
            'female_congregs' => $this->basic_model->fetch_all_records('female_congregations'),
            'regions' => $this->basic_model->fetch_all_records('regions'),
            'incardination_types' => $this->basic_model->fetch_all_records('incardination_types'),
            'education_types' => $this->basic_model->fetch_all_records('education_types'),
            'honourary_titles' => $this->basic_model->fetch_all_records('honourary_titles'),
            'ordination_types' => $this->basic_model->fetch_all_records('ordination_types'),
            'relationships' => $this->basic_model->fetch_all_records('relationship'),
            'titles' => $this->basic_model->fetch_all_records('titles'),
            'incardinations' => $this->basic_model->fetch_all_records('incardinations'),
            'blood_groups' => $this->basic_model->fetch_all_records('blood_groups'),
            'genotypes' => $this->basic_model->fetch_all_records('genotypes'),
            'personnel_types' => $this->per_model->getPersonnelTypes()
        ];

        $this->load->view('templates/header', ['page_title' => 'Personnel Form']);
        $this->load->view('personnel/add_profile', $data);
        $this->load->view('templates/footer', []);
    }

    public function get_state_lga($state_id) {

        $lgas = $this->basic_model->fetch_all_records('lga', ['state_id' => $state_id]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$lgas) {
            $data[] = ['id' => '-1', 'name' => "[LGAs not found]"];
        } else {
            foreach ($lgas as $lg) {
                $data[] = ['id' => $lg->id_lga, 'name' => ucfirst($lg->lga)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function get_region_cong($incardination_type_id) {

        $lgas = $this->basic_model->fetch_all_records('congregations', ['incardination_type_id' => $incardination_type_id]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$lgas) {
            $data[] = ['id' => '-1', 'name' => "[Congregation not found]"];
        } else {
            foreach ($lgas as $lg) {
                $data[] = ['id' => $lg->congregation_id, 'name' => ucfirst($lg->congregation)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function get_region_deanary($region_id) {

        $lgas = $this->basic_model->fetch_all_records('deanery', ['region_id' => $region_id]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$lgas) {
            $data[] = ['id' => '-1', 'name' => "[Deanary not found]"];
        } else {
            foreach ($lgas as $lg) {
                $data[] = ['id' => $lg->deanery_id, 'name' => ucfirst($lg->deanery)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function deletePersonnel($personnel_id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL, ['personnel_id' => $personnel_id])) {
            notify('success', 'Personnel deleted successfully');
        } else {
            notify('error', 'Unable to delete personnel at moment, pls try again');
        }
        redirect(site_url('/personnel/profiles'));
    }

    public function chaangeStatus() {
        if (request_is_post()) {

            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL, ['status' => request_post_data()['status_id']], ['personnel_id' => request_post_data()['personnel_id']])) {
                notify('success', 'Personnel status updated successfully');
            } else {
                notify('error', 'Unable to update personnel status at moment, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') ? : site_url('/personnel/profiles'));
        }
    }

    public function view_record($personnel_id) {
        $this->user_auth_lib->check_login();

        $profile = $this->per_model->fetchProfiles($personnel_id);


        $profile_content = $this->load->view('personnel/profile_page_contents/profile', array(
            'record' => $profile[0]
                ), true);

        $current_residence_content = $this->load->view('personnel/profile_page_contents/current_residence', array(
            'record' => $this->per_model->fetchPersonnelResidence($personnel_id)
                ), true);

        //Home parish
        $home_parish_content = $this->load->view('personnel/profile_page_contents/home_parish_content', array(
            'record' => $this->per_model->fetchPersonnelHomeParish($personnel_id)
                ), true);

        $family_contact_content = $this->load->view('personnel/profile_page_contents/family_contact', array(
            'record' => $this->per_model->fetchPersonnelFamilyContact($personnel_id)
                ), true);
        $apostolate_content = $this->load->view('personnel/profile_page_contents/apostolates', array(
            'apostolates' => $this->per_model->fetchPersonnelApostolates($personnel_id)
                ), true);
        $education_content = $this->load->view('personnel/profile_page_contents/education', array(
            'educations' => $this->per_model->fetchPersonnelEducations($personnel_id),
            'education_types' => $this->basic_model->fetch_all_records('education_types'),
                ), true);
        $health_info_content = $this->load->view('personnel/profile_page_contents/health_info', array(
            'health_info' => $this->per_model->fetchPersonnelHealthInfo($personnel_id),
            'blood_groups' => $this->basic_model->fetch_all_records('blood_groups'),
            'genotypes' => $this->basic_model->fetch_all_records('genotypes'),
                ), true);

        $ordination_content = $this->load->view('personnel/profile_page_contents/ordinations', array(
            'ordinations' => $this->per_model->fetchPersonnelOrdinations($personnel_id),
            'ordination_types' => $this->basic_model->fetch_all_records('ordination_types'),
                ), true);

        $honorary_title_content = $this->load->view('personnel/profile_page_contents/honorary_title_content', array(
            'Honorarytitles' => $this->per_model->fetchPersonnelHonoraryTitles($personnel_id),
            'honourary_titles' => $this->basic_model->fetch_all_records('honourary_titles'),
                ), true);

        $current_assignment_content = $this->load->view('personnel/profile_page_contents/current_assignment_content', array(
            'record' => $profile[0],
            'current_positions' => $this->basic_model->fetch_all_records('current_positions'),
                ), true);
        $pastoral_assignment_content = $this->load->view('personnel/profile_page_contents/pastoral_assignment_content', array(
            'pastoral_assignments' => $this->per_model->fetchPastoralAssignments($personnel_id),
            'pastoral_positions' => $this->basic_model->fetch_all_records('pastoral_positions'),
                ), true);

        $admin_office_content = $this->load->view('personnel/profile_page_contents/admin_office_content', array(
            'secondary_assignments' => $this->per_model->fetchSecondaryAssignments($personnel_id),
            'admin_offices' => $this->basic_model->fetch_all_records('admin_offices'),
                ), true);

        $data = array(
            'profile_content' => $profile_content,
            'current_residence_content' => $current_residence_content,
            'home_parish_content' => $home_parish_content,
            'family_contact_content' => $family_contact_content,
            'apostolate_content' => $apostolate_content,
            'education_content' => $education_content,
            'health_info_content' => $health_info_content,
            'ordination_content' => $ordination_content,
            'honorary_title_content' => $honorary_title_content,
            'current_assignment_content' => $current_assignment_content,
            'pastoral_assignment_content' => $pastoral_assignment_content,
            'admin_office_content' => $admin_office_content,
            'emergency_content' => [],
            'profile_picture' => $profile[0]->picture_url,
            'personnels' => $this->per_model->fetchProfiles(),
            'personnel_type' => $profile[0]->personnel_type
        );

        $this->user_nav_lib->run_page('personnel/profile_page', $data, 'Personnel Record | ' . BUSINESS_NAME);
    }

    public function get_profile_json($status = 0, $personnel_type = 0, $current_assignment = '') {
        $results = $this->per_model->fetchProfiles(null, $status, $personnel_type, $current_assignment, personnel\Personnel::class);
        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($results));
    }

    public function sendSms() {
        if (request_is_post()) {
            $personnels = $this->per_model->fetchPersonnelGroup(explode(',', request_post_data()['personnel_ids']));
            // check is message is not blank
            if (trim(request_post_data()['sms_message']) == '') {
                notify('error', 'Error! Message can not be blank.');
                redirect(site_url('/personnel/profiles'));
            }

            if (!empty($personnels)) {
                foreach ($personnels as $p) {
                    SmsAdapter::processViaSMSRoute($p->phone_number_1, request_post_data()['sms_message'], request_post_data()['sender_id']);
                }

                notify('success', 'Message sent successfully');
            } else {
                notify('error', 'Unable to send message, please try again.');
            }

            redirect(site_url('/personnel/profiles'));
        }
    }

    public function change_emp_picture($personnel_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if (request_is_post()) {
                if ($this->per_model->update_employee_picture($personnel_id, $_FILES['userfile'])) {
                    notify('success', "Picture updated successfully");
                } else {
                    notify('error', "Unable to update picture at moment, pls try again");
                }
                redirect(site_url("/personnel/{$personnel_id}/view_record"));
            }
        }

        $data = [
            'profile_picture' => $this->per_model->fetchProfiles($personnel_id),
            'personnel_id' => $personnel_id
        ];

        $this->user_nav_lib->run_page('personnel/edit_profile_picture', $data, 'Change Profile Picture | ' . BUSINESS_NAME);
    }

    public function remove_emp_picture($personnel_id) {
        $this->user_auth_lib->check_login();

        if ($this->per_model->remove_profile_picture($personnel_id)) {
            notify('success', 'Operation was successful');
        } else {
            notify('error', 'Something went wrong while updating profile picture');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function edit_profile($personnel_id) {
        $this->user_auth_lib->check_login();

        $pageData = [
            'profile' => $this->per_model->fetchProfiles($personnel_id)[0],
            'states' => $this->basic_model->fetch_all_records('states'),
            'female_congregs' => $this->basic_model->fetch_all_records('female_congregations'),
            'titles' => $this->basic_model->fetch_all_records('titles'),
            'regions' => $this->basic_model->fetch_all_records('regions'),
            'incardination_types' => $this->basic_model->fetch_all_records('incardination_types'),
        ];

        if (request_is_post()) {
            $data = request_post_data();
            $data['dob'] = convert_date($data['dob']);
            $data['lga'] = $data['lga_id'];
            unset($data['lga_id']);
            if (array_key_exists('dateof_reli_prof', $data)) {
                $data['dateof_reli_prof'] = date('Y-m-d', strtotime($data['dateof_reli_prof']));
            }
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL, $data, ['personnel_id' => $personnel_id])) {
                notify('success', "Profile updated successfully");
            } else {
                notify('error', "Oops! Unable to update profile at moment, please try again later");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record"));
        }

        $this->user_nav_lib->run_page('personnel/edit/profile', $pageData, 'Edit Profile | ' . BUSINESS_NAME);
    }

    public function edit_residence_info($personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'residence' => $this->per_model->fetchPersonnelResidence($personnel_id),
            'states' => $this->basic_model->fetch_all_records('states'),
            'regions' => $this->basic_model->fetch_all_records('regions'),
            'profile' => $this->per_model->fetchProfiles($personnel_id)[0],
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_RESIDENCE_INFO, request_post_data(), ['personnel_id' => $personnel_id])) {
                notify('success', "Residence Info updated successfully");
            } else {
                notify('error', "Oops! Unable to update residence info at moment, please try again later");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#current_residence_container');
        }

        $this->user_nav_lib->run_page('personnel/edit/residence_info', $pageData, 'Edit Profile | ' . BUSINESS_NAME);
    }

    public function deleteApostolate($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_APOSTOLATE, ['personnel_apostolate_id' => $id])) {
            notify('success', 'Apostolate deleted successfully');
        } else {
            notify('error', 'Oops! Something went wrong while deleting the selected apostolate, Pls try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#apostolate_container');
    }

    public function deleteEducation($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_EDUCATION_BACKGROUND, ['personnel_education_background_id' => $id])) {
            notify('success', 'Educational background deleted successfully');
        } else {
            notify('error', 'Oops! Something went wrong while deleting the selected educational background, Please try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#education_container');
    }

    public function deleteHonoraryTitle($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_HONORARY_TITLE, ['personnel_honorary_title_id' => $id])) {
            notify('success', 'Honorary title removed successfully');
        } else {
            notify('error', 'Oops! Something went wrong while removing the selected honorary title, Please try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#honorary_title_container');
    }

    public function deleteOrdination($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_ORDINATION, ['personnel_ordination_id' => $id])) {
            notify('success', 'Ordination removed successfully');
        } else {
            notify('error', 'Oops! Something went wrong while removing the selected ordination, Please try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#ordination_container');
    }

    public function editApostolate($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'apostolate' => $this->per_model->fetchPersonnelApostolates($personnel_id, $id)[0],
            'apostolate_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_APOSTOLATE, request_post_data(), ['personnel_apostolate_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update apostolate record, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#apostolate_container');
        }

        $this->load->view('personnel/edit/apostolate', $pageData);
    }

    public function editEducation($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'education' => $this->per_model->fetchPersonnelEducations($personnel_id, $id)[0],
            'education_types' => $this->basic_model->fetch_all_records('education_types'),
            'education_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_EDUCATION_BACKGROUND, request_post_data(), ['personnel_education_background_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update educational background, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#education_container');
        }

        $this->load->view('personnel/edit/education', $pageData);
    }

    public function editHonoraryTitle($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'hTitle' => $this->per_model->fetchPersonnelHonoraryTitles($personnel_id, $id)[0],
            'honourary_titles' => $this->basic_model->fetch_all_records('honourary_titles'),
            'honorary_title_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            $data = request_post_data();
            $data['date'] = convert_date($data['date']);
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_HONORARY_TITLE, $data, ['personnel_honorary_title_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update honorary title, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#honorary_title_container');
        }

        $this->load->view('personnel/edit/honorary_title', $pageData);
    }

    public function editOrdination($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'ordination' => $this->per_model->fetchPersonnelOrdinations($personnel_id, $id)[0],
            'ordination_types' => $this->basic_model->fetch_all_records('ordination_types'),
            'ordination_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            $data = request_post_data();
            $data['date'] = convert_date($data['date']);
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_ORDINATION, $data, ['personnel_ordination_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update ordination, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#ordination_container');
        }

        $this->load->view('personnel/edit/ordination', $pageData);
    }

    public function edit_family_contact($personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'contact' => $this->per_model->fetchPersonnelFamilyContact($personnel_id),
            'relationships' => $this->basic_model->fetch_all_records('relationship'),
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_EMERGENCY_CONTACT, request_post_data(), ['personnel_id' => $personnel_id])) {
                notify('success', "Family contact updated successfully");
            } else {
                notify('error', "Oops! Unable to update family contact, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#family_contact_container');
        }

        $this->load->view('personnel/edit/family_contact', $pageData);
    }

    public function edit_health_info($personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'health_info' => $this->per_model->fetchPersonnelHealthInfo($personnel_id),
            'blood_groups' => $this->basic_model->fetch_all_records('blood_groups'),
            'genotypes' => $this->basic_model->fetch_all_records('genotypes'),
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_HEALTH_INFO, request_post_data(), ['personnel_id' => $personnel_id])) {
                notify('success', "Health info updated successfully");
            } else {
                notify('error', "Oops! Unable to update health info, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#health_info_container');
        }

        $this->load->view('personnel/edit/health_info', $pageData);
    }

    public function edit_home_parish($personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'home_parish' => $this->per_model->fetchPersonnelHomeParish($personnel_id),
            'states' => $this->basic_model->fetch_all_records('states'),
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_HOME_PARISH, request_post_data(), ['personnel_id' => $personnel_id])) {
                notify('success', "Home parish updated successfully");
            } else {
                notify('error', "Oops! Unable to update home parish info, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#home_parish_container');
        }

        $this->load->view('personnel/edit/home_parish', $pageData);
    }

    public function addEducation() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->insert(Personnel_model::TBL_PERSONNEL_EDUCATION_BACKGROUND, request_post_data())) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Oops! Unable to save educational background, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') . '#education_container');
        }
    }

    public function addHonoraryTitle() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            $data = request_post_data();
            $data['date'] = convert_date($data['date']);
            if ($this->basic_model->insert(Personnel_model::TBL_PERSONNEL_HONORARY_TITLE, $data)) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Oops! Unable to save honorary title, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') . '#honorary_title_container');
        }
    }

    public function addOrdination() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            $data = request_post_data();

            $data['date'] = convert_date($data['date']);
            if ($this->basic_model->insert(Personnel_model::TBL_PERSONNEL_ORDINATION, $data)) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Oops! Unable to save ordination, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') . '#ordination_container');
        }
    }

    public function addSecondaryAssignment() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->insert(Personnel_model::TBL_PERSONNEL_SECONDARY_ASSIGNMENTS, request_post_data())) {
                notify('success', 'Administrative office successfully added');
            } else {
                notify('error', 'Oops! Unable to save administrative office, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') . '#admin_office_container');
        }
    }

    public function addPaAssignment() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->insert(Personnel_model::TBL_PERSONNEL_PRIMARY_ASSIGNMENT, request_post_data())) {
                notify('success', 'Pastoral assignment successfully added');
            } else {
                notify('error', 'Oops! Unable to save pastoral assignment, please try again');
            }
            redirect($this->input->server('HTTP_REFERER') . '#pastoral_assignment_container');
        }
    }

    public function deleteSeAssignment($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_SECONDARY_ASSIGNMENTS, ['personnel_secondary_assignment_id' => $id])) {
            notify('success', 'Administrative office deleted successfully');
        } else {
            notify('error', 'Oops! Something went wrong while deleting the selected administrative office, Please try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#admin_office_container');
    }

    public function deletePaAssignment($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Personnel_model::TBL_PERSONNEL_PRIMARY_ASSIGNMENT, ['personnel_primary_assignment_id' => $id])) {
            notify('success', 'Pastoral assignment deleted successfully');
        } else {
            notify('error', 'Oops! Something went wrong while deleting the selected Pastoral assignment, Please try again');
        }
        redirect($this->input->server('HTTP_REFERER') . '#pastoral_assignment_container');
    }

    public function editSeAssignment($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'assignment' => $this->per_model->fetchSecondaryAssignments($personnel_id, $id)[0],
            'admin_offices' => $this->basic_model->fetch_all_records('admin_offices'),
            'assignment_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_SECONDARY_ASSIGNMENTS, request_post_data(), ['personnel_secondary_assignment_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update administrative office, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#admin_office_container');
        }

        $this->load->view('personnel/edit/edit_admin_office', $pageData);
    }

    public function editPaAssignment($id, $personnel_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'assignment' => $this->per_model->fetchPastoralAssignments($personnel_id, $id)[0],
            'pastoral_positions' => $this->basic_model->fetch_all_records('pastoral_positions'),
            'assignment_id' => $id,
            'personnel_id' => $personnel_id,
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL_PRIMARY_ASSIGNMENT, request_post_data(), ['personnel_primary_assignment_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Oops! Unable to update pastoral assignment, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#pastoral_assignment_container');
        }

        $this->load->view('personnel/edit/edit_pastoral_assignment', $pageData);
    }

    public function editCurrentAssignment() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $personnel_id = request_post_data()['personnel_id'];
            if ($this->basic_model->update(Personnel_model::TBL_PERSONNEL, request_post_data(), ['personnel_id' => request_post_data()['personnel_id']])) {
                notify('success', "Current assignment updated successful");
            } else {
                notify('error', "Oops! Unable to update current assignment, please try again");
            }
            redirect(site_url("/personnel/{$personnel_id}/view_record") . '#current_assignment_container');
        }
    }

    public function compose_email() {
        $this->user_auth_lib->check_login();

        $recipients = rtrim($this->input->get('r'), '|');
        $personnels = $this->per_model->getPersonnelByArray(explode('|', $recipients));


        if (request_is_post()) {
            $error = '';
            if ($this->per_model->saveEmail(request_post_data(), $personnels, $error)) {
                notify('success', "Email has been queue and would be sent shortly");
            } else {
                notify('error', "Oops! {$error}");
            }
            redirect(site_url('/personnel/profiles'));
        }


        $data = [
            'personnels' => $personnels
        ];


        $this->user_nav_lib->run_page('personnel/compose', $data, "Profile | " . BUSINESS_NAME);
    }
    
    public function print_pdf($personnel_id = 0) {
        $this->user_auth_lib->check_login();

        if (!isset($personnel_id) || (int) $personnel_id < 1) {
            notify('error', "Operation could not be completed");
            redirect($this->input->server('HTTP_REFERER') ? : site_url('/personnel/profiles'));
        }

        $pageData = [
            'profile' => $this->per_model->fetchProfiles($personnel_id)[0],
            'residenceInfo' => $this->per_model->fetchPersonnelResidence($personnel_id),
            'homeParishInfo' => $this->per_model->fetchPersonnelHomeParish($personnel_id),
            'familyContantInfo' => $this->per_model->fetchPersonnelFamilyContact($personnel_id),
            'apostolateContent' => $this->per_model->fetchPersonnelApostolates($personnel_id),
            'educations' => $this->per_model->fetchPersonnelEducations($personnel_id),
            'healthInfo' => $this->per_model->fetchPersonnelHealthInfo($personnel_id),
            'ordinations' => $this->per_model->fetchPersonnelOrdinations($personnel_id),
            'honoraryTitles' => $this->per_model->fetchPersonnelHonoraryTitles($personnel_id),
            'pastoralAssignments' => $this->per_model->fetchPastoralAssignments($personnel_id),
            'admin_offices' => $this->per_model->fetchSecondaryAssignments($personnel_id),
        ];


//        $this->load->view('personnel/personnel_profile_pdf', $pageData);
        
        $buffer = $this->load->view('personnel/personnel_profile_pdf', $pageData, true);
        
        $this->load->library('pdf_lib');
        $file_name = random_string('alnum', 10);
        $this->pdf_lib->output_pdf(
                array(
                    'header' => ' ',
                    'footer' => $pageData['profile']->first_name . ' ' . $pageData['profile']->other_names . '. Personnel Profile. | {PAGENO} | ' . BUSINESS_NAME,
                    'html' => $buffer,
                    'file_name' => $file_name
        ));
    }

}
