<?php

/**
 * Description of Payroll_controller
 *
 * @author tohir
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Payroll_model $p_model Description
 * @property Employee_model $emp_model Description
 * @property Basic_model $basic_model Description
 * @property CI_Loader $load Description
 * @property CI_Input $input Description
 * @property Payroll_lib $payroll_lib Description
 */
class Payroll_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'payroll_lib',
        ));
        $this->load->helper(['user_nav_helper', 'date_helper']);
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('setup/employee_model', 'emp_model');
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function index() {
        
    }

    public function compensation() {
        $this->user_auth_lib->check_login();

        $data = [
            'paid_employees' => $this->p_model->fetchPaidEmployees()
        ];

        $this->user_nav_lib->run_page('payroll/compensation', $data, 'Staff Compensation ' . BUSINESS_NAME);
    }

    public function pay_grades() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->p_model->createPayGrade(request_post_data())) {
                notify('success', "Pay grade created successfully");
            } else {
                notify('error', "Ops! Unable to create pay grade at moment, Pls try again later");
            }

            redirect(site_url('/payroll/pay_grades'));
        }

        $data = [
            'pay_grades' => $this->p_model->fetchPayGrades(),
            'statuses' => $this->p_model->getStatus()
        ];

        $this->user_nav_lib->run_page('payroll/pay_grades', $data, 'Staff Compensation ' . BUSINESS_NAME);
    }

    public function delete_paygrade($ref_id) {
        $this->user_auth_lib->check_login();

        if ($this->p_model->deletePayGrade($ref_id)) {
            notify('success', 'Pay grade deleted successfully');
        } else {
            notify('error', 'Ops! Unable to delete pay grade at moment, pls try again');
        }

        redirect($this->input->server('HTTP_REFERER'));
    }

    public function unpaid_staffs() {
        $this->user_auth_lib->check_login();
        $data = [
            'employees' => $this->p_model->fetchUnPaidEmployees(),
            'statuses' => Employee_model::getStatuses()
        ];

        $this->user_nav_lib->run_page('payroll/unpaid_staffs', $data, 'Unpaid Staffs | ' . BUSINESS_NAME);
    }

    public function settings() {
        $this->user_auth_lib->check_login();

        $data = [
            'allowances' => $this->p_model->get(Payroll_model::TBL_ALLOWANCE),
            'deductions' => $this->p_model->get(Payroll_model::TBL_DEDUCTION),
        ];

        $this->user_nav_lib->run_page('payroll/settings/pay_types', $data, 'Payroll Settings | ' . BUSINESS_NAME);
    }

    public function add_allowance() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->p_model->savePayType(Payroll_model::TBL_ALLOWANCE, request_post_data())) {
                notify('success', "Allowance created successfully");
            } else {
                notify('error', "Unable to add allowance at moment, pls try again");
            }

            redirect(site_url('/payroll/settings'));
        }
    }

    public function add_deduction() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->p_model->savePayType(Payroll_model::TBL_DEDUCTION, request_post_data())) {
                notify('success', "Deduction created successfully");
            } else {
                notify('error', "Unable to add deduction at moment, pls try again");
            }

            redirect(site_url('/payroll/settings'));
        }
    }

    public function delete_allowance($ref_id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Payroll_model::TBL_ALLOWANCE, array('ref_id' => $ref_id))) {
            notify('success', 'Allowance deleted successfully');
        } else {
            notify('error', 'Something went wrong while deleting the selected allowance, Pls try again');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function delete_deduction($ref_id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Payroll_model::TBL_DEDUCTION, array('ref_id' => $ref_id))) {
            notify('success', 'Deduction deleted successfully');
        } else {
            notify('error', 'Something went wrong while deleting the selected deduction, Pls try again');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function edit_allowance($ref_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update(Payroll_model::TBL_ALLOWANCE, request_post_data(), ['ref_id' => $ref_id])) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Ops! Unable to complete your request at moment, pls try again');
            }
            redirect($this->input->server('HTTP_REFERER'));
        }

        $page_data = [
            'allowance' => $this->p_model->get(Payroll_model::TBL_ALLOWANCE, ['ref_id' => $ref_id])[0]
        ];

        $this->load->view('payroll/settings/edit_allowance', $page_data);
    }

    public function edit_deduction($ref_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->update(Payroll_model::TBL_DEDUCTION, request_post_data(), ['ref_id' => $ref_id])) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Ops! Unable to complete your request at moment, pls try again');
            }
            redirect($this->input->server('HTTP_REFERER'));
        }

        $page_data = [
            'deduction' => $this->p_model->get(Payroll_model::TBL_DEDUCTION, ['ref_id' => $ref_id])[0]
        ];

        $this->load->view('payroll/settings/edit_deduction', $page_data);
    }

    public function add_compensation($employee_id, $full_name) {
        $this->user_auth_lib->check_login();
        $compensation = $this->p_model->fetchEmployeeCompensation($employee_id);

        $page_data = [
            'employee_info' => $this->emp_model->fetchAllEmployees($employee_id),
            'allowances' => $this->p_model->get(Payroll_model::TBL_ALLOWANCE),
            'deductions' => $this->p_model->get(Payroll_model::TBL_DEDUCTION),
            'compensation' => $compensation,
            'pay_schedules' => $this->p_model->fetchPaySchedules(),
            'pay_periods' => $this->p_model->get_pay_period(),
            'schedule_period' => $this->p_model->get_schedule_period($compensation[0]->schedule_id),
        ];

        $this->user_nav_lib->run_page('payroll/add_compensation', $page_data, 'Add Compensation ' . BUSINESS_NAME);
    }

    public function add_grosspay($employee_id, $full_name) {
        if (request_is_post()) {
            if ($this->p_model->saveGrosspayComponent($employee_id, request_post_data())) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to complete your request at moment, please try again later");
            }
            redirect(site_url("/payroll/add_compensation/{$employee_id}/{$full_name}"));
        }
    }

    public function add_deductions($employee_id, $full_name) {
        if (request_is_post()) {
            if ($this->p_model->saveDeductionComponent($employee_id, request_post_data())) {
                notify('success', "Deduction added successfully");
            } else {
                notify('error', "Unable to complete add deduction, please try again later");
            }
            redirect(site_url("/payroll/add_compensation/{$employee_id}/{$full_name}"));
        }
    }

    public function payment_schedule() {
        $this->user_auth_lib->check_login();

        $pageData = [
            'pay_schedules' => $this->p_model->fetchPaySchedules(),
            'pay_periods' => $this->p_model->get_pay_period()
        ];

        $this->user_nav_lib->run_page('payroll/settings/payment_schedule', $pageData, 'Payment Schedule | ' . BUSINESS_NAME);
    }

    public function add_new_schedule() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->p_model->createPaySchedule(request_post_data())) {
                notify('success', 'Payment schedule created successfully');
            }
            redirect(site_url('/payroll/payment_schedule'));
        }

        $pageData = [
            'pay_periods' => $this->p_model->get_pay_period()
        ];

        $this->user_nav_lib->run_page('payroll/settings/add_payment_schedule', $pageData, 'Add Payment Schedule | ' . BUSINESS_NAME);
    }

    public function delete_schedule($id) {
        $this->user_auth_lib->check_login();
        if ($this->basic_model->delete(Payroll_model::TBL_PAY_SCHEDULE, ['schedule_id' => $id])) {
            notify('success', "Payment schedule deleted successfully");
        } else {
            notify('error', "Unable to delete payment schedule, please try again later");
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function add_payment_schedule($employee_id, $full_name) {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->p_model->addPaymentSchedule($employee_id, request_post_data())) {
                notify('success', 'Payment schedule added successfully');
            } else {
                notify('error', 'Oops! Unable to add payment scehdule.');
            }
            redirect(site_url("/payroll/add_compensation/{$employee_id}/{$full_name}"));
        }
    }

    public function compensation_payslip($employee_id) {
        $this->user_auth_lib->check_login();
        if (!isset($employee_id) || !is_numeric($employee_id)) {
            trigger_error('Compensation ID not set', E_USER_ERROR);
            exit;
        }

        $pageData = [
            'compensation' => $this->p_model->fetchEmployeeCompensation($employee_id)[0]
        ];

        $this->user_nav_lib->run_page('/payroll/compensation/payslip', $pageData, 'Payslip | ' . BUSINESS_NAME);
    }

    public function run_payroll() {
        $this->user_auth_lib->check_login();
        $pageData = [
            'pay_schedules' => $this->p_model->fetchPaySchedules(),
            'run_schedules' => $this->p_model->fetchRunSchedules()
        ];

        $this->user_nav_lib->run_page('/payroll/run_payroll/manage_payroll', $pageData, 'Run Payroll | ' . BUSINESS_NAME);
    }

    public function run_pay($step, $ref) {
        $this->user_auth_lib->check_login();
        if ($step == 'step1') {
            $this->_run_pay_step1($ref);
        } elseif ($step == 'step2') {
            $this->_run_pay_step2($ref);
        } elseif ($step == 'step3') {
            $this->_run_pay_step3($ref);
        }
    }

    private function _run_pay_step1($ref) {
        if ($ref == '') {
            notify('error', 'Invalid schedule reference');
            redirect(site_url('/payroll/run_payroll'));
        }

        if (request_is_post()) {
            if ($new_ref = $this->p_model->process_step1(request_post_data())) {
                notify('success', 'Operation was successful');
                redirect(site_url('/payroll/run_pay/step2/' . $new_ref));
            } else {
                notify('error', 'Oops! Something went wrong while processing your request.');
                redirect(site_url('/payroll/run_pay/step1/' . $ref));
            }
        }

        $pageData = [
            'upcoming_periods' => $this->p_model->fetch_upcoming_periods($ref)
        ];

        $this->user_nav_lib->run_page('/payroll/run_payroll/step1', $pageData, 'Select Pay Period | ' . BUSINESS_NAME);
    }

    public function _run_pay_step2($ref) {
        if ($ref == '') {
            notify('error', 'Invalid pay reference');
            redirect(site_url('/payroll/run_payroll'));
        }

        if (request_is_post()) {
            if ($this->p_model->approvePayroll($ref, request_post_data())) {
                notify('success', "Payroll approved successfully");
            } else {
                notify('error', "Unable to approve payroll at moment, pls try again later");
            }
            redirect(site_url('/payroll/pay_records'));
        }

        $pageData = [
            'paid_employees' => $this->p_model->fetchPaidEmployees()
        ];

        $this->user_nav_lib->run_page('/payroll/run_payroll/step2', $pageData, 'Review Employee Pay | ' . BUSINESS_NAME);
    }

    public function cancel_payrun($ref_id) {
        $this->user_auth_lib->check_login();

        if ($this->payroll_lib->cancel_payrun($ref_id)) {
            notify('success', 'Operation was successful');
        } else {
            notify('error', 'Unable to cancel payrun, please try again');
        }

        redirect(site_url('/payroll/run_payroll'));
    }

    public function pay_records() {
        $this->user_auth_lib->check_login();
        $pageData = [
            'records' => $this->p_model->fetchRecords()
        ];
        $this->user_nav_lib->run_page('payroll/records/list', $pageData, ' Payroll Records | ' . BUSINESS_NAME);
    }
    
    public function view_record($ref) {
        $this->user_auth_lib->check_login();
        
         $pageData = [
            'schedule' => $this->p_model->fetchRecords($ref)[0],
            'records' => $this->p_model->fetchArchiveRecords($ref),
        ];
        $this->user_nav_lib->run_page('payroll/records/view_record', $pageData, ' Payroll Records | ' . BUSINESS_NAME);
    }
    
    public function excel_download($ref) {
        $this->user_auth_lib->check_login();
        $this->payroll_lib->download_excel($ref);
    }
    
    public function view_payslip($ref_id, $employee_id) {
        $this->user_auth_lib->check_login();
        $pageData = [
            'schedule' => $this->p_model->fetchRecords($ref_id)[0],
            'records' => $this->p_model->fetchArchiveRecords($ref_id, $employee_id)[0],
        ];
        $this->user_nav_lib->run_page('payroll/records/payslip', $pageData, ' Payslip | ' . BUSINESS_NAME);
    }

}
