<?php

/**
 * Description of Personnel_model
 *
 * @author tohir
 * @property CI_DB_query_builder $db
 * @property Cloudinary_lib $cloudinary_lib Description
 * @property Mailer $mailer Description
 */
class Personnel_model extends CI_Model {

    private $personnel_type;

    const TBL_PERSONNEL = "personnel";
    const TBL_PERSONNEL_EDUCATION_BACKGROUND = "personnel_education_background";
    const TBL_PERSONNEL_EMERGENCY_CONTACT = "personnel_emergency_contact";
    const TBL_PERSONNEL_HEALTH_INFO = "personnel_health_info";
    const TBL_PERSONNEL_HOME_PARISH = "personnel_home_parish";
    const TBL_PERSONNEL_HONORARY_TITLE = "personnel_honorary_title";
    const TBL_PERSONNEL_ORDINATION = "personnel_ordination";
    const TBL_PERSONNEL_PRIMARY_ASSIGNMENT = "personnel_primary_assignment";
    const TBL_PERSONNEL_APOSTOLATE = "personnel_apostolate";
    const TBL_PERSONNEL_SEM_ASSIGNMENT = "personnel_sem_assignments";
    const TBL_PERSONNEL_RESIDENCE_INFO = "personnel_residence_info";
    const TBL_PERSONNEL_SECONDARY_ASSIGNMENTS = "personnel_secondary_assignments";

    protected static $personnelStatus = [];

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->personnel_type = [PERSONNEL_PRIST => 'Priests', PERSONNEL_REV_SISTER => 'Female Religious', PERSONNEL_SEMINARIANS => 'Seminarians'];

        self::$personnelStatus = [
            PERSONNEL_ACTIVE => 'Active',
            PERSONNEL_INACTIVE => 'Inactive',
            PERSONNEL_EXPELLED => 'Expelled',
        ];
    }

    public function getPersonnelTypes() {
        return $this->personnel_type;
    }

    public static function getPersonnelStatuses() {
        return static::$personnelStatus;
    }

    public function savePersonnel($data, $file, &$error) {

        if (empty($data)) {
            $error = "Please ensure all form entries are filled properly";
            return FALSE;
        }

        // check if email already exist
        if (!$this->_personnel_email_exist($data['email'])) {
            $error = 'Sorry, the email you supplied has been used';
            return FALSE;
        }

        $this->db->trans_start();

        //Personnel Info
        $personnel_data = [];
        $personnel_data['personnel_type'] = $data['personnel_type'];

        if ($data['personnel_type'] == PERSONNEL_PRIST) {
            $personnel_data['title_id'] = $data['title_id'];
        } elseif ($data['personnel_type'] == PERSONNEL_REV_SISTER) {
            $personnel_data['title'] = $data['title'];
        }

        //process image
        if (!empty($file)) {
            $upload = $this->cloudinary_lib->doUpload($file['tmp_name']);
            $personnel_data['picture_url'] = $upload['url'];
        }

        $personnel_data['surname'] = $data['surname'];
        $personnel_data['other_names'] = $data['other_names'];
        $personnel_data['dob'] = convert_date($data['dob']);
        $personnel_data['place_of_birth'] = $data['placeof_birth'];
        $personnel_data['nationality'] = $data['nationality'];
        $personnel_data['state_id'] = $data['state_id'];
        $personnel_data['lga'] = $data['lga_id'];
        $personnel_data['phone_number_1'] = $data['phone'];
        $personnel_data['phone_number_2'] = $data['phone2'];
        $personnel_data['email'] = $data['email'];


        if ($data['personnel_type'] != PERSONNEL_PRIST) {
            $personnel_data['dateof_reli_prof'] = date('Y-m-d', strtotime($data['dateof_reli_prof']));
        }
        if ($data['personnel_type'] == PERSONNEL_REV_SISTER) {
            $personnel_data['female_congregation_id'] = $data['female_congregation_id'];
        }

        if ($data['personnel_type'] == PERSONNEL_PRIST) {
            $personnel_data['incardination_type_id'] = $data['incardination_type_id'];
            $personnel_data['current_assignment'] = preg_replace('/[^A-Za-z0-9\-]/', ' ', $data['current_assignment']);
            $personnel_data['current_position_id'] = $data['current_position_id'];
            $personnel_data['congregation_id'] = $data['congregation_id'];
        }

        $this->db->insert(self::TBL_PERSONNEL, $personnel_data);
        $personnel_id = $this->db->insert_id();

        $residence_data = [];
        $residence_data['place_of_residence	'] = $data['residence'];
        $residence_data['address'] = $data['residence_address'];
        $residence_data['city'] = $data['residence_city'];
        $residence_data['state_id'] = $data['residence_state_id'];
        if ($data['personnel_type'] != PERSONNEL_SEMINARIANS) {
            $residence_data['region_id'] = $data['region_id'];
            $residence_data['deanery_id'] = $data['deanery_id'];
        }
        $residence_data['personnel_id'] = $personnel_id;
        $this->db->insert(self::TBL_PERSONNEL_RESIDENCE_INFO, $residence_data);

        if ($data['personnel_type'] != PERSONNEL_REV_SISTER) {
            $home_parish = [];
            $home_parish['parish'] = $data['home_parish'];
            $home_parish['city'] = $data['home_parish_city'];
            $home_parish['state_id'] = $data['home_parish_state_id'];
            $home_parish['personnel_id'] = $personnel_id;
            if ($data['personnel_type'] == PERSONNEL_SEMINARIANS) {
                $home_parish['region_id'] = $data['region_id'];
                $home_parish['deanery_id'] = $data['deanery_id'];
            }
            $this->db->insert(self::TBL_PERSONNEL_HOME_PARISH, $home_parish);
        }

        if ($data['personnel_type'] != PERSONNEL_REV_SISTER) {
            $emergency_contact = [];
            $emergency_contact['first_name'] = $data['contact_first_name'];
            $emergency_contact['last_name'] = $data['contact_last_name'];
            $emergency_contact['relationship_id'] = $data['relationship_id'];
            $emergency_contact['phone_number'] = $data['emergencyphone_number'];
            $emergency_contact['personnel_id'] = $personnel_id;
            $this->db->insert(self::TBL_PERSONNEL_EMERGENCY_CONTACT, $emergency_contact);
        }

        if (isset($data['genotype_id']) && !empty($data['genotype_id'])) {
            $health_info = [];
            $health_info['genotype_id'] = $data['genotype_id'];
            $health_info['blood_group_id'] = $data['blood_group_id'];
            $health_info['health_history'] = $data['health_history'];
            $health_info['personnel_id'] = $personnel_id;
            $this->db->insert(self::TBL_PERSONNEL_HEALTH_INFO, $health_info);
        }

        $education_data = [];
        if (!empty($data['education'])) {
            for ($j = 0; $j < count($data['education']); $j++) {
                $education_data[$j] = array(
                    'education_type_id' => $data['education'][$j]['education_type_id'],
                    'institution_name' => $data['education'][$j]['institution'],
                    'year_from' => $data['education'][$j]['from'],
                    'year_to' => $data['education'][$j]['to'],
                    'degree' => $data['education'][$j]['degree'],
                    'personnel_id' => $personnel_id,
                );
            }

            $this->db->insert_batch(self::TBL_PERSONNEL_EDUCATION_BACKGROUND, $education_data);
        }

        $primary_assignment = [];
        if (!empty($data['primary'])) {

            for ($j = 0; $j < count($data['primary']); $j++) {
                $primary_assignment[$j] = array(
                    'name_of_assignment' => $data['primary'][$j]['nameof_assignment'],
                    'pastoral_position_id' => $data['primary'][$j]['position_primary'],
                    'year_from' => $data['primary'][$j]['from'],
                    'year_to' => $data['primary'][$j]['to'],
                    'personnel_id' => $personnel_id,
                );
            }
            $this->db->insert_batch(self::TBL_PERSONNEL_PRIMARY_ASSIGNMENT, $primary_assignment);
        }

        $apostolate = [];
        if (!empty($data['apostolate'])) {

            for ($j = 0; $j < count($data['apostolate']); $j++) {
                if (trim($data['apostolate'][$j]['nameof_assignment']) !== '') {
                    $apostolate[$j] = array(
                        'name_of_assignment' => $data['apostolate'][$j]['nameof_assignment'],
                        'position' => $data['apostolate'][$j]['position'],
                        'year_from' => $data['apostolate'][$j]['from'],
                        'year_to' => $data['apostolate'][$j]['to'],
                        'personnel_id' => $personnel_id,
                    );
                }
            }
            !empty($apostolate) ? $this->db->insert_batch(self::TBL_PERSONNEL_APOSTOLATE, $apostolate) : '';
        }

        $seminaryAss = [];
        if (!empty($data['seminary'])) {
            for ($j = 0; $j < count($data['seminary']); $j++) {
                if ($data['seminary'][$j]['nameof_assignment'] !== '') {
                    $seminaryAss[$j] = array(
                        'name_of_assignment' => $data['seminary'][$j]['nameof_assignment'],
                        'parish_priest' => $data['seminary'][$j]['parish_priest'],
                        'year_from' => $data['seminary'][$j]['from'],
                        'year_to' => $data['seminary'][$j]['to'],
                        'personnel_id' => $personnel_id,
                    );
                }
            }
            !empty($seminaryAss) ? $this->db->insert_batch(self::TBL_PERSONNEL_SEM_ASSIGNMENT, $seminaryAss) : '';
        }

        $secondary_assignment = [];
        if (!empty($data['secondary'])) {
            for ($j = 0; $j < count($data['secondary']); $j++) {
                if ($data['secondary'][$j]['position_secondary'] !== '' && trim($data['secondary'][$j]['nameof_secondary_assignment']) !== '') {
                    $secondary_assignment[$j] = array(
                        'name_of_secondary_assignment' => $data['secondary'][$j]['nameof_secondary_assignment'],
                        'admin_office_id' => $data['secondary'][$j]['position_secondary'],
                        'year_from' => $data['secondary'][$j]['from'],
                        'year_to' => $data['secondary'][$j]['to'],
                        'personnel_id' => $personnel_id,
                    );
                }
            }
            !empty($secondary_assignment) ? $this->db->insert_batch(self::TBL_PERSONNEL_SECONDARY_ASSIGNMENTS, $secondary_assignment) : '';
        }

        if (!empty($data['ordination'])) {
            $ordination = [];
            for ($j = 0; $j < count($data['ordination']); $j++) {
                if ($data['ordination'][$j]['ordination_type_id'] !== '') {
                    $ordination[$j] = array(
                        'ordination_type_id' => $data['ordination'][$j]['ordination_type_id'],
                        'place' => $data['ordination'][$j]['place_episcopal'],
                        'date' => convert_date($data['ordination'][$j]['date_episcopal']),
                        'ordaining_prelate' => $data['ordination'][$j]['ordaining_episcopal'],
                        'personnel_id' => $personnel_id,
                    );
                }
            }
            !empty($ordination) ? $this->db->insert_batch(self::TBL_PERSONNEL_ORDINATION, $ordination) : '';
        }

        if (!empty($data['honorary'])) {
            $honorary = [];
            for ($j = 0; $j < count($data['honorary']); $j++) {
                if ($data['honorary'][$j]['honorary_title_id'] !== '') {
                    $honorary[$j] = array(
                        'honorary_title_id' => $data['honorary'][$j]['honorary_title_id'],
                        'place' => $data['honorary'][$j]['honorary_place'],
                        'date' => convert_date($data['honorary'][$j]['honorary_date']),
                        'prelate' => $data['honorary'][$j]['honorary_prelate'],
                        'personnel_id' => $personnel_id,
                    );
                }
            }
            !empty($honorary) ? $this->db->insert_batch(self::TBL_PERSONNEL_HONORARY_TITLE, $honorary) : '';
        }

        $this->db->trans_complete();
        return TRUE;
    }

    private function _personnel_email_exist($email) {
        if ($this->db->get_where(self::TBL_PERSONNEL, ['email' => trim($email)])->row()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function fetchPersonnelGroup($param) {
        if (!empty($param)) {
            $this->db->where_in('p.personnel_id', $param);
        }
        return $this->db->select('p.*, t.title as title_name, states.state, lga.lga as lgovt')
                        ->from(self::TBL_PERSONNEL . ' as p')
                        ->join('titles as t', 't.title_id=p.title_id', 'left')
                        ->join('states', 'states.state_id=p.state_id', 'left')
                        ->join('lga ', 'lga.id_lga=p.lga', 'left')
                        ->get()->result();
    }

    public function fetchProfiles($personnel_id = null, $status = 0, $personnel_type = 0, $current_assignment='', $rowClass = stdClass::class) {
//        echo urldecode($current_assignment); exit;
        if ($personnel_id) {
            $this->db->where('p.personnel_id', $personnel_id);
        }
        if ($status && (int) $status > 0) {
            $this->db->where('p.status', $status);
        }
        if ($personnel_type && (int) $personnel_type > 0) {
            $this->db->where('p.personnel_type', $personnel_type);
        }
        if ($current_assignment && strlen($current_assignment) > 0) {
            $this->db->like('p.current_assignment', urldecode($current_assignment));
        }

        return $this->db->select('p.*, t.title as title_name, states.state, lga.lga as lgovt, inc.incardination_type as incardination, con.congregation, cp.position current_position')
                        ->from(self::TBL_PERSONNEL . ' as p')
                        ->join('titles as t', 't.title_id=p.title_id', 'left')
                        ->join('states', 'states.state_id=p.state_id', 'left')
                        ->join('lga ', 'lga.id_lga=p.lga', 'left')
                        ->join('incardination_types inc', 'inc.incardination_type_id=p.incardination_type_id', 'left')
                        ->join('congregations con', 'con.congregation_id=p.congregation_id', 'left')
                        ->join('current_positions cp', 'cp.current_position_id=p.current_position_id', 'left')
                        ->get()->result($rowClass);
    }

    public function fetchPersonnelResidence($personnel_id) {
        return $this->db->select('pr.*, states.state, r.region, d.deanery')
                        ->from(self::TBL_PERSONNEL_RESIDENCE_INFO . ' as pr')
                        ->join('regions r', 'r.region_id=pr.region_id', 'left')
                        ->join('deanery d', 'd.deanery_id=pr.deanery_id', 'left')
                        ->join('states', 'states.state_id=pr.state_id', 'left')
                        ->where('pr.personnel_id', $personnel_id)
                        ->get()->row();
    }

    public function fetchPersonnelFamilyContact($personnel_id) {
        return $this->db->select('pc.*, r.relationship')
                        ->from(self::TBL_PERSONNEL_EMERGENCY_CONTACT . ' as pc')
                        ->join('relationship r', 'r.relationship_id=pc.relationship_id', 'left')
                        ->where('pc.personnel_id', $personnel_id)
                        ->get()->row();
    }

    public function fetchPersonnelEducations($personnel_id, $personnel_education_background_id = null) {
        if ($personnel_education_background_id) {
            $this->db->where('ped.personnel_education_background_id', $personnel_education_background_id);
        }
        return $this->db->select('ped.*, et.education_type')
                        ->from(self::TBL_PERSONNEL_EDUCATION_BACKGROUND . ' as ped')
                        ->join('education_types et', 'ped.education_type_id=et.education_type_id')
                        ->where('ped.personnel_id', $personnel_id)
                        ->get()->result();
    }

    public function fetchPersonnelOrdinations($personnel_id, $personnel_ordination_id = null) {
        if ($personnel_ordination_id) {
            $this->db->where('po.personnel_ordination_id', $personnel_ordination_id);
        }
        return $this->db->select('po.*, ot.ordination_type')
                        ->from(self::TBL_PERSONNEL_ORDINATION . ' as po')
                        ->join('ordination_types ot', 'po.ordination_type_id=ot.ordination_type_id')
                        ->where('po.personnel_id', $personnel_id)
                        ->get()->result();
    }

    public function fetchPersonnelHonoraryTitles($personnel_id, $personnel_honorary_title_id = null) {
        if ($personnel_honorary_title_id) {
            $this->db->where('pht.personnel_honorary_title_id', $personnel_honorary_title_id);
        }
        return $this->db->select('pht.*, ht.honourary_title')
                        ->from(self::TBL_PERSONNEL_HONORARY_TITLE . ' as pht')
                        ->join('honourary_titles ht', 'pht.honorary_title_id=ht.honorary_title_id')
                        ->where('pht.personnel_id', $personnel_id)
                        ->get()->result();
    }

    /**
     * 
     * @param type $personnel_id
     * @param type $personnel_apostolate_id
     * @return type
     */
    public function fetchPersonnelApostolates($personnel_id, $personnel_apostolate_id = null) {
        if ($personnel_apostolate_id) {
            $where = [
                'personnel_id' => $personnel_id,
                'personnel_apostolate_id' => $personnel_apostolate_id,
            ];
        } else {
            $where = [
                'personnel_id' => $personnel_id,
            ];
        }
        return $this->db
                        ->get_where(self::TBL_PERSONNEL_APOSTOLATE, $where)
                        ->result();
    }

    public function fetchPersonnelHealthInfo($personnel_id) {
        return $this->db
                        ->select('hi.*, bg.blood_group, g.genotype')
                        ->from(self::TBL_PERSONNEL_HEALTH_INFO . ' hi')
                        ->join('blood_groups bg', 'bg.blood_group_id=hi.blood_group_id', 'left')
                        ->join('genotypes g', 'g.genotype_id=hi.genotype_id', 'left')
                        ->where('hi.personnel_id', $personnel_id)
                        ->get()->row();
    }

    public function update_employee_picture($personnel_id, $file) {

        if (!empty($file) && $file['name'] !== '') {

            if (!$upload = $this->cloudinary_lib->doUpload($file['tmp_name'])) {
                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data_img['picture_url'] = $upload['url'];

            $this->db->where('personnel_id', $personnel_id)
                    ->update(self::TBL_PERSONNEL, $data_img);
            return $this->db->affected_rows() > 0;
        }
    }

    public function remove_profile_picture($personnel_id) {
        $this->db->where('personnel_id', $personnel_id)
                ->update(self::TBL_PERSONNEL, ['picture_url' => null]);
        return $this->db->affected_rows() > 0;
    }

    public function fetchPastoralAssignments($personnel_id, $personnel_primary_assignment_id = null) {
        if ($personnel_primary_assignment_id) {
            $this->db->where('pa.personnel_primary_assignment_id', $personnel_primary_assignment_id);
        }
        return $this->db->select('pa.*, pp.pastoral_position')
                        ->from(self::TBL_PERSONNEL_PRIMARY_ASSIGNMENT . ' pa')
                        ->where('pa.personnel_id', $personnel_id)
                        ->join('pastoral_positions pp', 'pp.pastoral_position_id=pa.pastoral_position_id')
                        ->get()->result();
    }

    public function fetchSecondaryAssignments($personnel_id, $personnel_secondary_assignment_id = null) {
        if ($personnel_secondary_assignment_id) {
            $this->db->where('sa.personnel_secondary_assignment_id', $personnel_secondary_assignment_id);
        }
        return $this->db->select('sa.*, ao.admin_office')
                        ->from(self::TBL_PERSONNEL_SECONDARY_ASSIGNMENTS . ' sa')
                        ->where('sa.personnel_id', $personnel_id)
                        ->join('admin_offices ao', 'ao.admin_office_id=sa.admin_office_id')
                        ->get()->result();
    }

    public function fetchPersonnelHomeParish($personnel_id) {
        return $this->db->select('php.*, s.state')
                        ->from(self::TBL_PERSONNEL_HOME_PARISH . ' php')
                        ->join('states s', 's.state_id=php.state_id')
                        ->where('php.personnel_id', $personnel_id)
                        ->get()->row();
    }

    public function getPersonnelByArray($pids) {
        if (empty($pids)) {
            return FALSE;
        }
        return $this->db->from(self::TBL_PERSONNEL)
                        ->where_in('personnel_id', $pids)
                        ->get()->result();
    }
    
    public function saveEmail($params, $personnels, &$error) {
        if (empty($params)) {
            $error = "Message content not found";
            return FALSE;
        }
        
        if (empty($personnels)) {
            $error = "Email recipients not found";
            return FALSE;
        }
        
        // log inside email queue
        foreach ($personnels as $person) {
            $email_params = [
                'to' => $person->email,
                'module_id' => PERSONNEL_MODULE,
                'sender' => REPLY_EMAIL,
                'sender_name' => BUSINESS_NAME,
                'email_data' => json_encode(['message' => $params['email_message'], 'fullname' => $person->surname . ' ' . $person->other_names]),
                'email_template' => 'email_templates/default',
                'subject' => $params['email_subject'],
                ];
            $this->mailer->add_queue($email_params);
        }
        
        return TRUE;
        
    }

}
