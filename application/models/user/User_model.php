<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of user_model
 *
 * @author TOHIR
 */
class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function last_staff_id($school_id) {
        $sql = "SELECT staff_id FROM employees WHERE school_id = '" . $school_id . "' order by staff_id desc limit 1 ";

        $result = $this->db->query($sql)->result_array();

        if (!$result) {
            return '';
        }

        return $result[0]['staff_id'];
    }

    /**
     * Fetch user account by array values from users table.
     * Return false if record not exists.
     * 
     * EXAMPLE 1:
     * 
     * $query_fields = array(
     *                  'email' => 'user@example.com', 
     *                  'password' => 'user_password'
     *                 );
     * 
     * This will create query with email and password fields.
     * 
     * EXAMPLE: 2
     * 
     * $query_fields = array(
     *                   'id_user' => 55
     *                 );
     * 
     * This will create query with id only.
     * 
     * This approach gives us possiblity to get user record with many different 
     * criterias instead of creating many functions like:
     * 
     * fetch_account_by_email_password()
     * fetch_account_by_id()
     * 
     * @access public
     * @param array $query_fields
     * @return mixed (bool | array)
     */
    public function fetch_account(array $query_fields) {

        if (empty($query_fields)) {
            trigger_error('query fields cannot be empty!', E_USER_WARNING);
        }

        $sql = "SELECT 
					u.*,
                    e.*,
					s.school_name, 
					s.string_id as school_id_string,
					s.status as school_status,
					s.cdn_container as cdn_container,
					s.logo_path
				FROM 
					users u, 
                    employees e,
					schools s 
				WHERE 
					s.school_id = u.school_id 
                    AND e.user_id = u.user_id ";

        foreach ($query_fields as $field => $value) {

            if (substr($field, 0, 2) == 'u.') {
                $sql .= "AND " . $field . " = '" . $value . "' ";
            } else {
                $sql .= "AND u." . $field . " = '" . $value . "' ";
            }
        }

        return $this->db->query($sql)->result();
    }

    public function fetch_user_groups_modules_perms($user_id) {

        $sql = "SELECT
	                m.module_id,
                    m.subject AS module_subject,
                    m.id_string AS module_id_string,
	                p.subject AS perm_subject,
                    p.perm_id,
	                p.id_string AS perm_id_string,
	                p.in_menu,
	                u.user_id
                    FROM user_perms u
                    LEFT JOIN module_perms p ON p.perm_id = u.perm_id
                    AND p.in_menu = 1
                    AND p.status = 1
                    LEFT JOIN modules m ON m.module_id = p.module_id
                    WHERE u.user_id = " . $user_id . "
                    GROUP BY u.perm_id
                    order by m.menu_order, m.subject, p.menu_order, p.subject
                    ";

        $result = $this->db->query($sql)->result_array();
        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function fetch_admin_account($params) {
        if (!is_array($params)) {
            return FALSE;
        }

        return $this->db->get_where(TBL_SUPER_ADMIN)->row();
    }

    public function fetchaccount($params) {
        if (!is_array($params)) {
            return FALSE;
        }

        return $this->db->get_where(TBL_USERS, $params)->row();
    }

    public function fetchUnassignedPerms($user_id) {
        $sql = "SELECT * FROM module_perms WHERE perm_id not IN (select perm_id FROM user_perms WHERE user_id = {$user_id} )";
        return $this->db->query($sql)->result();
    }

    public function assignAllPerm($user_id) {
        $allPerms = $this->fetchUnassignedPerms($user_id);
        if (empty($allPerms)) {
            return;
        }

        $datadb = [];
        foreach ($allPerms as $perm) {
            $datadb[] = array(
                'user_id' => $user_id,
                'perm_id' => $perm->perm_id,
                'module_id' => $perm->module_id,
            );
        }

        return $this->db->insert_batch(TBL_USER_PERMS, $datadb);
    }

    public function assignDefaultPermissions($user_id, $default_perms) {

        //check if have basic default permission
        $batch = [];
        foreach ($default_perms as $id_module => $perm) {

            foreach ($perm as $key => $id_perm) {

                if (!$id_perm) {
                    $permObj = $this->db->get_where('module_perms', ['module_id' => $id_module, 'id_string' => $key], 1)
                            ->result()[0];
                    if (!$permObj) {
                        throw new RuntimeException("Permision with module_id: $id_module AND id_string: $key");
                    }

                    $id_perm = $permObj->id_perm;
                }

                $check_perm = $this->db->get_where('user_perms', array('perm_id' => $id_perm, 'module_id' => $id_module, 'user_id' => $user_id))->result();

                if (empty($check_perm)) {

                    $perm_data = array(
                        'user_id' => $user_id,
                        'module_id' => $id_module,
                        'perm_id' => $id_perm
                    );

                    $batch[] = $perm_data;
                }
            }
        }

        if (!empty($batch)) {
            $this->db->insert_batch('user_perms', $batch);
        }
    }

    /**
     * Save user data.
     * If id_user exists in array, than it will call update function.
     * Else, will call insert function.
     * 
     * @access public
     * @param array $data 
     * @return bool
     */
    final public function save(array $data, $where = null) {

        if (!is_null($where)) {
            return $this->update($data, $where);
        } else {
            return $this->db->insert(TBL_USERS, $data);
        }
    }

    final protected function update(array $data, $where) {
        $this->db->update(TBL_USERS, $data, $where);
    }

    public function log_db($data) {
        return $this->db->insert('user_logs', $data);
    }

    public function fetch_user_logs() {
        return $this->db->select('u.*, u2.first_name, u2.last_name')
                ->from('user_logs u')
                ->join('users u2', 'u.user_id=u2.user_id')
                ->order_by('u.id', 'desc')
                ->get()->result();
    }

}
