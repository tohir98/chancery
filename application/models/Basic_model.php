<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_query_builder $db Description
 */
class Basic_model extends CI_Model {

    private $account_types;
    private $statuses;

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->account_types = [1 => 'Saving', 2 => 'Current', 3 => 'Fixed Deposit'];
        $this->statuses = ['Inactive', 'Active'];
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function insert($table, $data) {
        if (empty($data)) {
            return FALSE;
        }
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update any table
     * @param type $table Table name
     * @param type $data update data
     * @param type $where update condition
     * @return boolean
     */
    public function update($table, $data, $where) {
        $this->db->where($where);
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Class Delete anything
     * 
     * @access public
     * @return void
     */
    public function delete($table, $array) {
        $this->db->where($array);
        return $this->db->delete($table);
    }

    public function fetchBankAccounts() {
        return $this->db
                        ->select('ba.*, b.name as bank_name')
                        ->from(TBL_BANK_ACCOUNT . ' as ba')
                        ->join(TBL_BANKS . ' as b', 'ba.bank_id=b.bank_id')
                        ->get()->result();
    }

    public function fetch_income_accounts() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.heading_type_id', INCOME)
                        ->get()->result();
    }

    public function fetch_cash_accounts() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.account_sub_category_id', ACCOUNT_BANK)
                        ->get()->result();
    }

    public function fetch_income_receivables() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.heading_type_id', RECEIVABLES)
                        ->get()->result();
    }

    public function fetch_asset_accounts() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.heading_type_id', OTHER_ASSETS)
                        ->get()->result();
    }

    public function fetch_liability_accounts() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.heading_type_id', LIABILITY)
                        ->or_where('ac.heading_type_id', EQUITY)
                        ->or_where('ac.heading_type_id', RE)
                        ->or_where('ac.heading_type_id', PYFB)
                        ->get()->result();
    }

    public function fetch_expense_accounts() {
        return $this->db
                        ->select('ac.*')
                        ->from(TBL_ACCOUNT_CHART . ' as ac')
                        ->where('ac.heading_type_id', EXPENSE)
                        ->get()->result();
    }

    public function get_account_types() {
        return $this->account_types;
    }

    public function get_statuses() {
        return $this->statuses;
    }

    public function fetchPersonnelUpcomingBirthdays($startDate, $endDate) {
        $sql = "SELECT
                surname,
                other_names,
                dob, SUBSTRING(dob, 6 ,5) as ddb, personnel_type, picture_url, phone_number_1
        FROM
                personnel
        where SUBSTRING(dob, 6 ,5) >= '" . substr(date('Y-m-d', strtotime($startDate)), 5, 5) . "'  and SUBSTRING(dob, 6 ,5) <= '" . substr(date('Y-m-d', strtotime($endDate)), 5, 5) . "' 
        order by ddb";
        
        return $this->db->query($sql)->result();
    }
    
    public function add($table, $param) {
        return $this->db->insert($table, $param);
    }

}
