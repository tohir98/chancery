<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of branch_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_active_record $db 
 * @property CI_Loader $load loader
 */
class Transaction_model extends CI_Model {

    private static $expense_types;

    public function __construct() {
        parent::__construct();
        $this->load->database();

        static::$expense_types = [
            EXPENSE_PAYABLE => 'Invoices/Payables',
            EXPENSE_PAID_INVOICES => 'Paid Invoices',
            EXPENSE_DISBURSEMENT => 'Disbursements',
        ];
    }

    public function saveTransaction($table, $data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        $data['ref_id'] = strtoupper(random_string('alnum', 10));
        $data['date_created'] = date('Y-m-d h:i:s');
        if ($table == TBL_EXPENDITURE) {
            $data['incured_date'] = date('Y-m-d', strtotime($data['incured_date']));
            $data['due_date'] = date('Y-m-d', strtotime($data['due_date']));
        } elseif ($table == TBL_INCOME) {
            $data['income_date'] = date('Y-m-d', strtotime($data['income_date']));
        } else {
            $data['date_acquired'] = date('Y-m-d', strtotime($data['date_acquired']));
        }
        $data['created_by'] = $this->user_auth_lib->get('user_id');

        return $this->db->insert($table, $data);
    }

    public function fetch_incomes() {
        return $this->db
                        ->select('i.*, is.source_name, ac.description as income_type, ac.code, ac.description')
                        ->from('incomes i')
                        ->join('income_sources is', 'i.income_source_id=is.income_source_id')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=i.account_chart_id')
                        ->order_by('i.income_date', 'desc')
                        ->get()->result();
    }

    public function fetch_assets() {
        return $this->db
                        ->select('a.*, ac.description as type, ac.code, b.name as bank_name')
                        ->from(TBL_ASSETS . ' as a')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=a.account_chart_id')
                        ->join(TBL_BANK_ACCOUNT . ' as ba', 'ba.bank_account_id=a.bank_account_id', 'left')
                        ->join(TBL_BANKS . ' as b', 'b.bank_id=ba.bank_id', 'left')
                        ->get()->result();
    }

    public function fetch_liabilities() {
        return $this->db
                        ->select('l.*, ac.description as type, ac.code, b.name as bank_name')
                        ->from(TBL_LIABILITY . ' as l')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=l.account_chart_id')
                        ->join(TBL_BANK_ACCOUNT . ' as ba', 'ba.bank_account_id=l.bank_account_id', 'left')
                        ->join(TBL_BANKS . ' as b', 'b.bank_id=ba.bank_id', 'left')
                        ->get()->result();
    }

    public function fetch_expenses() {
        return $this->db
                        ->select('e.*, ac.description as expense_type, ac.code as expense_code, v.vendor_name')
                        ->from(TBL_EXPENDITURE . ' as e')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=e.expense_type_id', 'left')
                        ->join(TBL_VENDOR . ' as v', 'v.vendor_id=e.vendor_id')
                        ->order_by('e.incured_date', 'DESC')
                        ->get()->result();
    }

    public function add_receivable($income_id, $data, $deduct = FALSE) {
        $amount = $data['amount'];
        if ($deduct) {
            $amount = -1 * floatval($data['amount']);
        }
        return $this->db->insert(TBL_RECEIVABLES, [
                    'account_chart_id' => $data['account_chart_id'],
                    'amount' => $amount,
                    'income_id' => $income_id,
                    'date_added' => date('Y-m-d h:i:s'),
                    'added_by' => $this->user_auth_lib->get('user_id'),
        ]);
    }

    public function process_deposit($data) {
        $this->db->trans_start();

        $this->db->insert(TBL_INCOME, array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'),
            'date_created' => date('Y-m-d h:i:s'),
            'income_date' => date('Y-m-d', strtotime($data['income_date']))]));

        $income_id = $this->db->insert_id();


        $this->add_cash($income_id, $data);

        $this->db->trans_complete();

        return true;
    }

    public function add_cash($income_id, $data) {
        $this->add_receivable($income_id, $data, TRUE);

        return $this->db->insert(TBL_CASH, [
                    'account_chart_id' => $data['account_chart_id'],
                    'amount' => $data['amount'],
                    'income_id' => $income_id,
                    'date_added' => date('Y-m-d h:i:s'),
                    'added_by' => $this->user_auth_lib->get('user_id'),
        ]);
    }

    public function getARBalance($account_chart_id, $income_soure_id) {
        return $this->db
                        ->select('r.account_chart_id, sum(r.amount) balance, a.description')
                        ->from(TBL_RECEIVABLES . ' as r')
                        ->join(TBL_INCOME . ' as i', 'i.income_id=r.income_id')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=r.account_chart_id')
                        ->where('r.account_chart_id', $account_chart_id)
                        ->where('i.income_source_id', $income_soure_id)
                        ->get()->row();
    }

    public function getAPBalance($ap_account_id, $vendor_id) {
        return $this->db
                        ->select('p.account_chart_id, sum(p.amount) balance, a.description')
                        ->from(TBL_PAYABLES . ' as p')
                        ->join(TBL_EXPENDITURE . ' as e', 'e.expense_id=p.expense_id')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=p.account_chart_id')
                        ->where('p.account_chart_id', $ap_account_id)
                        ->where('e.vendor_id', $vendor_id)
                        ->get()->row();
    }

    public function getCashBalance($account_chart_id, $income_source_id) {
        return $this->db
                        ->select('i.account_chart_id, sum(i.amount) balance, a.description')
                        ->from(TBL_INCOME . ' as i')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=i.account_chart_id')
                        ->where('i.account_chart_id', $account_chart_id)
                        ->where('i.income_source_id', $income_source_id)
                        ->get()->row();
    }

    public function getBankBalance($account_chart_id) {
        $income = $this->db
                        ->select('i.cash_account_id, sum(i.amount) balance, a.description')
                        ->from(TBL_INCOME . ' as i')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=i.cash_account_id')
                        ->where('i.cash_account_id', $account_chart_id)
                        ->get()->row();

        $expenses = $this->db
                        ->select('e.cash_account_id, sum(e.amount) balance')
                        ->from(TBL_EXPENDITURE . ' as e')
                        ->where('e.cash_account_id', $account_chart_id)
                        ->get()->row();

        return [
            'description' => $income->description,
            'balance' => floatval($income->balance) - floatval($expenses->balance)
        ];
    }

    public function getBankBalanceExp($account_chart_id) {
        return $this->db
                        ->select('e.cash_account_id, sum(e.amount) balance, a.description')
                        ->from(TBL_EXPENDITURE . ' as e')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=e.cash_account_id')
                        ->where('e.cash_account_id', $account_chart_id)
                        ->get()->row();
    }

    public function getPendingReceivables() {
        return $this->db
                        ->select('i.amount, i.due_date, s.source_name')
                        ->from(TBL_INCOME . ' as i')
                        ->join(TBL_INCOME_SOURCE . ' as s', 's.income_source_id=i.income_source_id')
                        ->where('i.income_type_id', INCOME_RECEIVABLE)
                        ->where('i.due_date >=', date('Y-m-d'))
                        ->get()->result();
    }

    /**
     * Add record to expense table
     * @param type $expense_type Invoice/Payables/Disbursement
     * @param type $data
     * @return boolean true on success
     */
    public function addExpense($expense_type, $data) {

        if (empty($data)) {
            return FALSE;
        }

        // start transaction
        $this->db->trans_start();

        $data['ref_id'] = strtoupper(random_string('alnum', 10));
        $data['incured_date'] = convert_date($data['incured_date']);
        $data['due_date'] = convert_date($data['due_date']);
        $data['date_created'] = date('Y-m-d h:i:s');
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['expense_type_id'] = $expense_type;

        $this->db->insert(TBL_EXPENDITURE, $data);
        $expense_id = $this->db->insert_id();

        if ($expense_type == EXPENSE_PAYABLE || $expense_type == EXPENSE_PAID_INVOICES) {
            $this->db->insert(TBL_PAYABLES, [
                'account_chart_id' => $data['ap_account_id'],
                'vendor_id' => $data['vendor_id'],
                'amount' => $expense_type == EXPENSE_PAID_INVOICES ? (-1 * floatval($data['amount'])) : $data['amount'],
                'date_added' => date('Y-m-d h:i:s'),
                'added_by' => $this->user_auth_lib->get('user_id'),
                'expense_id' => $expense_id
            ]);
        }

        if ($expense_type == EXPENSE_DISBURSEMENT) {
            $this->_add_disbursements($expense_id, $data);
        }

        $this->db->trans_complete();
        return TRUE;
    }

    private function _add_disbursements($expense_id, $data) {
        return $this->db->insert(TBL_DISBURSEMENT, [
                    'account_chart_id' => $data['expense_category_id'],
                    'vendor_id' => $data['vendor_id'],
                    'amount' => floatval($data['amount']),
                    'date_added' => date('Y-m-d h:i:s'),
                    'added_by' => $this->user_auth_lib->get('user_id'),
                    'expense_id' => $expense_id
        ]);
    }

    public function getexpenseTypes() {
        return static::$expense_types;
    }

}
