<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of agent_model
 *
 * @author TOHIR
 * @property CI_DB_query_builder $db Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Setup_model extends CI_Model {

    const TBL_EMP = 'employees';

    private static $subject = "Access Granted | Agent";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function deleteAccount($account_chart_id) {
        return $this->db->where('account_chart_id', $account_chart_id)
                        ->delete('account_chart');
    }

    private function _sendConfirmationEmail($user_id, $params) {
        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $params['first_name'],
            'username' => $params['email'],
            'password' => DEFAULT_PASSWORD,
            'verify_link' => site_url('verify_email/' . $user_id . '/' . $this->user_auth_lib->encrypt($params['email'] . $user_id) . '/acc'),
        );

        $msg = $this->load->view('email_templates/agent', $mail_data, true);

        return $this->mailer
                        ->sendMessage(static::$subject, $msg, $params['email']);
    }

    public function get_sub_groups($group_id) {
        return $this->db->get_where('sub_groups', ['group_id' => $group_id])->result_array();
    }

    public function fetchGroups() {
        $groups = $this->db->get('groups')->result();
        if (empty($groups)) {
            return FALSE;
        }

        $result = [];

        foreach ($groups as $group) {
            $result[] = array_merge((array) $group, ['sub_group' => $this->get_sub_groups($group->group_id)]);
        }
        return $result;
    }

    public function get_sub_categories($category_id) {
        return $this->db->get_where('sub_categories', ['category_id' => $category_id])->result_array();
    }

    public function fetchCategories() {
        $categories = $this->db->get('categories')->result();
        if (empty($categories)) {
            return FALSE;
        }

        $result = [];

        foreach ($categories as $category) {
            $result[] = array_merge((array) $category, ['sub_categories' => $this->get_sub_categories($category->category_id)]);
        }

        return $result;
    }

    public function add_sub_group($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        if (!is_array($data['sub_group']) || empty($data['sub_group'])) {
            return FALSE;
        }

        $data_db = [];
        foreach ($data['sub_group'] as $sub_group) {
            $data_db[] = ['group_id' => $data['group_id'], 'sub_group' => $sub_group, 'status' => 1, 'date_created' => date('Y-m-d :i:s')];
        }
        !empty($data_db) ? $this->db->insert_batch('sub_groups', $data_db) : '';

        return TRUE;
    }

    public function add_sub_category($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        if (!is_array($data['sub_category']) || empty($data['sub_category'])) {
            return FALSE;
        }

        $data_db = [];
        foreach ($data['sub_category'] as $sub_category) {
            $data_db[] = ['category_id' => $data['category_id'], 'sub_category' => $sub_category, 'status' => 1, 'date_created' => date('Y-m-d :i:s')];
        }
        !empty($data_db) ? $this->db->insert_batch('sub_categories', $data_db) : '';

        return TRUE;
    }

    public function saveAccount($data) {
        return $this->db->insert('account_chart', array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'), 'date_created' => date('Y-m-d h:i:s')]));
    }

    public function saveIncomeType($data) {
        return $this->db->insert('income_types', array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'), 'date_created' => date('Y-m-d h:i:s')]));
    }

    public function saveExpenseType($data) {
        return $this->db->insert('expense_types', array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'), 'date_created' => date('Y-m-d h:i:s')]));
    }

    public function saveIncomeSource($data) {
        return $this->db->insert('income_sources', array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'), 'date_created' => date('Y-m-d h:i:s')]));
    }

    public function fetch_all_accounts() {
        return $this->db
                        ->select('a.*, c.category, s.sub_category, h.heading, (
		SELECT
			opening_balance
		FROM
			account_balances
		WHERE
			account_chart_id = a.account_chart_id
		ORDER BY
			account_balance_id DESC
		LIMIT 1
	) as opening_balance,
	(
		SELECT
			closing_balance
		FROM
			account_balances
		WHERE
			account_chart_id = a.account_chart_id
		ORDER BY
			account_balance_id DESC
		LIMIT 1
	) as closing_balance')
                        ->from(TBL_ACCOUNT_CHART . ' as a')
                        ->join('account_category as c', 'a.account_category_id=c.account_category_id')
                        ->join('account_sub_category as s', 'a.account_sub_category_id=s.account_sub_category_id')
//                        ->join(TBL_ACCOUNT_BALANCE . ' as b', 'b.account_chart_id=a.account_chart_id', 'left')
                        ->join(TBL_HEADING_TYPE . ' as h', 'a.heading_type_id=h.heading_type_id')
                        ->get()->result();
    }

    public function fetchBankAccounts() {
        return $this->db
                        ->select('ba.*, b.name')
                        ->from(TBL_BANK_ACCOUNT . ' as ba')
                        ->join(TBL_BANKS . ' as b', 'ba.bank_id=b.bank_id')
                        ->get()->result();
    }

    public function saveVendor($data) {
        if (empty($data)) {
            return FALSE;
        }

        return $this->db->insert(TBL_VENDOR, array_merge($data, ['created_by' => $this->user_auth_lib->get('user_id'), 'date_created' => date('Y-m-d h:i:s'), 'vendor_since' => date('Y-m-d', strtotime($data['vendor_since']))]));
    }

    public function vendor_exits($vendor_name) {
        return $this->db->get_where(TBL_VENDOR, ['vendor_name' => $vendor_name])->row();
    }

    public function fetchAssets() {
        return $this->db
                        ->select('as.*, a.description, a.code')
                        ->from(TBL_ASSETS . ' as as')
                        ->join(TBL_ACCOUNT_CHART . ' as a', 'a.account_chart_id=as.account_chart_id')
                        ->get()->result();
    }

    public function fetchLiabilities() {
        return $this->db
                        ->select('a.*, asb.sub_category, ac.category')
                        ->from(TBL_ACCOUNT_CHART . ' as a')
                        ->join('account_category as ac', 'a.account_category_id=ac.account_category_id')
                        ->join('account_sub_category as asb', 'a.account_sub_category_id=asb.account_sub_category_id')
                        ->where_in('a.account_sub_category_id', [4, 5])
                        ->get()->result();
    }

    public function saveAsset($data) {
        $data['date_added'] = date('Y-m-d h:i:s');
        $data['ref_id'] = strtoupper(random_string('alnum', 10));
        $data['purchase_date'] = date('Y-m-d', strtotime($data['purchase_date']));
        $data['added_by'] = $this->user_auth_lib->get('user_id');

        $this->db->insert(TBL_ASSETS, $data);

        return $this->db->insert_id();
    }

    public function fetchAccountBalance($account_chart_id) {
        return $this->db->get_where(TBL_ACCOUNT_BALANCE, ['account_chart_id' => $account_chart_id])->result();
    }

    public function saveOpeningBalance($data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['added_by'] = $this->user_auth_lib->get('user_id');
        $data['date_added'] = date('Y-m-d h:i:s');

        return $this->db->insert(TBL_ACCOUNT_BALANCE, $data);
    }

}
