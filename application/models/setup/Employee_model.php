<?php

require_once 'Setup_model.php';

/**
 * Description of Employee_model
 *
 * @author tohir
 * @property CI_DB_query_builder $db Description
 */
class Employee_model extends Setup_model {

    static $statuses = ['Inactive', 'Active', 'Inactive', 'Terminated'];
    static $genders = [1 => 'Female', 2 => 'Male'];
    static $job_types = [1 => 'Full Time', 2 => 'Part Time'];

    public function add_employee($data) {
        if (empty($data)) {
            return false;
        }

        $data['dob'] = date('Y-m-d', strtotime($data['dob']));
        $data['employment_date'] = date('Y-m-d', strtotime($data['employment_date']));

        return $this->db->insert(TBL_EMPLOYEES, $data);
    }

    public function update_employee($employee_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['dob'] = date('Y-m-d', strtotime($data['dob']));
        $data['employment_date'] = date('Y-m-d', strtotime($data['employment_date']));

        $this->db->where('employee_id', $employee_id)
                ->update(TBL_EMPLOYEES, $data);

        return $this->db->affected_rows() > 0;
    }

    /**
     * Returns employee information
     * @param type int Employee id
     * @param type $rowClass
     * @return type
     */
    public function fetchAllEmployees($employee_id = null, $skipp = null, $rowClass = stdClass::class) {
        if (!is_null($employee_id)) {
            $this->db->where('e.employee_id', $employee_id);
        }
        if (!is_null($skipp)) {
            $this->db->where_not_in('e.employee_id', $skipp);
        }
        return $this->db
                        ->select('e.*, s.state, r.relationship, b.name')
                        ->from(self::TBL_EMP . ' as e')
                        ->join('states s', 's.state_id = e.state_id', 'left')
                        ->join('relationship r', 'r.relationship_id = e.relationship_id', 'left')
                        ->join('banks b', 'b.bank_id = e.bank_id', 'left')
                        ->get()->result($rowClass);
    }

    public static function getStatuses() {
        return static::$statuses;
    }

    public static function getGenders() {
        return static::$genders;
    }

    public static function getJobTypes() {
        return static::$job_types;
    }

    public function updateEmployee($data) {
        if (empty($data)) {
            return FALSE;
        }

        $data_db = array(
            'reason' => $data['reason'],
            'status' => $data['status_id'],
            'date_terminated' => date('Y-m-d', strtotime($data['date_termination'])),
        );

        $this->db->where('employee_id', $data['employee_id'])
                ->update(TBL_EMPLOYEES, $data_db);
        return $this->db->affected_rows() > 0;
    }

    public function fetchEmp($emp_id) {
        return $this->db->get_where(TBL_EMPLOYEES, ['employee_id' => $emp_id])->row();
    }

    public function update_employee_picture($employee_id, $file) {

        if (!empty($file) && $file['name'] !== '') {

            $ext = end((explode(".", $file['name'])));

            $config = array(
                'upload_path' => FILE_PATH_EMP_PIC,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "3000",
                'max_width' => "3000",
            );

            $picture_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $picture_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {

                $error = array('error' => $this->upload->display_errors());

                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data_img['profile_picture_name'] = $picture_name;
            $data_img['employee_id'] = $employee_id;
            $data_img['profile_picture_url'] = site_url('/files/employee_pictures/' . $picture_name);

            $this->db->where('employee_id', $employee_id)
                    ->update(TBL_EMPLOYEES, $data_img);
            return $this->db->affected_rows() > 0;
        }
    }

    public function remove_profile_picture($employee_id) {
        $this->db->where('employee_id', $employee_id)
                ->update(TBL_EMPLOYEES, ['profile_picture_name' => null, 'profile_picture_url' => null]);
        return $this->db->affected_rows() > 0;
    }

}
