<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payroll_model
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_query_builder $db Description
 * @property Payroll_lib $payroll_lib Description
 * @property Employee_model $emp_model Description
 */
class Payroll_model extends CI_Model {

    const TBL_PAYGRADE = 'payroll_paygrade';
    const TBL_ALLOWANCE = 'payroll_allowances';
    const TBL_DEDUCTION = 'payroll_deductions';
    const TBL_STAFF_ALLOWANCES = 'payroll_staff_allowances';
    const TBL_STAFF_DEDUCTIONS = 'payroll_staff_deductions';
    const TBL_COMPENSATION = 'payroll_compensation';
    const TBL_PAY_SCHEDULE = 'payroll_pay_schedule';
    const TBL_RUN_SCHEDULE = 'payroll_run_schedule';
    const TBL_SCHEDULE_PERIOD = 'payroll_schedule_period';
    const TBL_COMPENSATION_ARCHIVE = 'payroll_compensation_archive';
    const TBL_SCHEDULE_ARCHIVE = 'payroll_run_schedule_archive';
    const PAYGRADE_DRAFT = 1;
    const PAYGRADE_COMPLETED = 2;
    const PERIOD_UPCOMING = 'Upcoming';
    const PERIOD_COMPLETED = 'Completed';
    const PERIOD_ONGOING = 'Ongoing';

    private $status;
    private $pay_period;

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->status = [self::PAYGRADE_DRAFT => 'Draft', self::PAYGRADE_COMPLETED => 'Completed'];
        $this->pay_period = [PERIOD_MONTHLY => 'Monthly'];
    }

    public function fetchPayGrades() {
        return $this->db->get(self::TBL_PAYGRADE)->result();
    }

    public function createPayGrade($data) {
        if (empty($data)) {
            return FALSE;
        }

        return $this->db->insert(self::TBL_PAYGRADE, ['pay_grade' => $data['pay_grade'],
                    'date_created' => date('Y-m-d h:i:s'),
                    'status' => self::PAYGRADE_DRAFT,
                    'ref_id' => strtoupper(random_string('alnum', 10))
        ]);
    }

    public function deletePayGrade($ref_id) {
        if (!isset($ref_id) || $ref_id == '') {
            return FALSE;
        }

        $this->db->where('ref_id', $ref_id)
                ->delete(self::TBL_PAYGRADE);

        return $this->db->affected_rows() > 0;
    }

    public function getStatus() {
        return $this->status;
    }

    public function get($table_name, $where = null) {
        return $this->db->get_where($table_name, $where)->result();
    }

    public function savePayType($tablename, $data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['ref_id'] = strtoupper(random_string('alnum', 10));
        $data['date_added'] = date('Y-m-d h:i:s');
        $data['added_by'] = $this->user_auth_lib->get('user_id');

        return $this->db->insert($tablename, $data);
    }

    public function saveGrosspayComponent($employee_id, $data) {
        if (empty($data)) {
            return FALSE;
        }

        // if new component found, save it as new allowance
        if (array_key_exists('components', $data)) {
            foreach ($data['components'] as $allowance) {
                if (!$this->db->get_where(self::TBL_ALLOWANCE, ['title' => $allowance['title']])->result()) {
                    $this->savePayType(self::TBL_ALLOWANCE, ['title' => $allowance['title'], 'percentage' => $allowance['percentage']]);
                }
            }
        }

        $all_allowances = array_key_exists('components', $data) ? array_merge((array) $data['components'], (array) $data['allowance']) : $data['allowance'];

        if ($exist = $this->db->get_where(self::TBL_STAFF_ALLOWANCES, ['employee_id' => $employee_id])->result()) {
            $this->db->update(self::TBL_STAFF_ALLOWANCES, ['components' => json_encode($all_allowances), 'date_updated' => date('Y-m-d h:i:s')], ['employee_id' => $employee_id]);
            $last_id = $exist[0]->payroll_staff_allowance_id;
        } else {
            $this->db->insert(self::TBL_STAFF_ALLOWANCES, [
                'employee_id' => $employee_id,
                'grosspay' => $data['grosspay'],
                'components' => json_encode($all_allowances),
                'date_added' => date('Y-m-d h:i:s'),
                'added_by' => $this->user_auth_lib->get('user_id'),
            ]);
            $last_id = $this->db->insert_id();
        }

        // Insert into employee compensation
        return $this->_update_staff_compensation($employee_id, ['payroll_staff_allowance_id' => $last_id]);
    }

    private function _update_staff_compensation($employee_id, $params) {
        if ($this->db->get_where(self::TBL_COMPENSATION, ['employee_id' => $employee_id])->row()) {
            $params['date_updated'] = date('Y-m-d h:i:s');
            $this->db->update(self::TBL_COMPENSATION, $params, ['employee_id' => $employee_id]);
        } else {
            $params['employee_id'] = $employee_id;
            $params['date_added'] = date('Y-m-d h:i:s');
            $params['added_by'] = $this->user_auth_lib->get('user_id');
            $this->db->insert(self::TBL_COMPENSATION, $params);
        }

        return $this->db->affected_rows() > 0;
    }

    public function fetchEmployeeCompensation($employee_id, $rowClass = stdClass::class) {
        return $this->db
                        ->select('c.*, e.*, a.grosspay, a.components, d.components deductions, ps.*')
                        ->from(self::TBL_COMPENSATION . ' as c')
                        ->join(Employee_model::TBL_EMP . ' as e', 'e.employee_id=c.employee_id')
                        ->join(self::TBL_STAFF_ALLOWANCES . ' as a', 'c.payroll_staff_allowance_id=a.payroll_staff_allowance_id', 'left')
                        ->join(self::TBL_STAFF_DEDUCTIONS . ' as d', 'd.payroll_staff_deduction_id=c.payroll_staff_deduction_id', 'left')
                        ->join(self::TBL_PAY_SCHEDULE . ' as ps', 'ps.schedule_id=c.schedule_id', 'left')
                        ->where('c.employee_id', $employee_id)
                        ->get()->result($rowClass);
    }

    public function saveDeductionComponent($employee_id, $data) {
        if (empty($data)) {
            return false;
        }

        if (array_key_exists('components', $data)) {
            foreach ($data['components'] as $deduction) {
                if (!$this->db->get_where(self::TBL_DEDUCTION, ['deduction' => $deduction['title']])->result()) {
                    $this->savePayType(self::TBL_DEDUCTION, ['deduction' => $deduction['title'], 'amount' => $deduction['amount']]);
                }
            }
        }

        $all_deductions = array_key_exists('components', $data) ? array_merge((array) $data['components'], (array) $data['deduction']) : $data['deduction'];

        if ($exist = $this->db->get_where(self::TBL_STAFF_DEDUCTIONS, ['employee_id' => $employee_id])->result()) {
            $this->db->update(self::TBL_STAFF_DEDUCTIONS, ['components' => json_encode($all_deductions), 'date_updated' => date('Y-m-d h:i:s')], ['employee_id' => $employee_id]);
            $last_id = $exist[0]->payroll_staff_deduction_id;
        } else {
            $this->db->insert(self::TBL_STAFF_DEDUCTIONS, [
                'employee_id' => $employee_id,
                'components' => json_encode($all_deductions),
                'date_added' => date('Y-m-d h:i:s'),
                'added_by' => $this->user_auth_lib->get('user_id'),
            ]);
            $last_id = $this->db->insert_id();
        }


        return $this->_update_staff_compensation($employee_id, ['payroll_staff_deduction_id' => $last_id]);
    }

    public function get_pay_period() {
        return $this->pay_period;
    }

    public function createPaySchedule($data) {
        if (empty($data)) {
            notify('error', 'Unable to create pay schedule, Please try again');
            return FALSE;
        }

        // compute monthly period from library
        if ($this->getScheduleByName($data['schedule_name'])) {
            notify('error', 'Schedule name already exists, please choose another name');
            return FALSE;
        }

        $this->db->insert(self::TBL_PAY_SCHEDULE, [
            'schedule_name' => ucfirst($data['schedule_name']),
            'period_id' => $data['period_id'],
            'created_by' => $this->user_auth_lib->get('user_id'),
            'status' => ACTIVE,
            'ref_id' => strtoupper(random_string('alnum', 10))
        ]);

        return $this->payroll_lib->calculate_monthly($this->db->insert_id(), $data);
    }

    public function getScheduleByName($schedule_name) {
        return $this->db->get_where(self::TBL_PAY_SCHEDULE, ['schedule_name' => $schedule_name])->result();
    }

    public function fetchPaySchedules() {
        $sql = "SELECT
	pps.*, SUBSTRING_INDEX(
		GROUP_CONCAT(
			psp.`start_date`
			ORDER BY
				psp.`payroll_schedule_period_id`
		),
		',',
		1
	)AS `from`,
	SUBSTRING_INDEX(
		GROUP_CONCAT(
			psp.`end_date`
			ORDER BY
				psp.`payroll_schedule_period_id`
		),
		',',
		1
	)AS `to`,
	SUBSTRING_INDEX(
		GROUP_CONCAT(
			psp.`pay_date`
			ORDER BY
				psp.`payroll_schedule_period_id`
		),
		',',
		1
	)AS `pay_date`,
	SUBSTRING_INDEX(
		GROUP_CONCAT(
			psp.`status`
			ORDER BY
				psp.`payroll_schedule_period_id`
		),
		',',
		1
	)AS `status`
FROM
	payroll_pay_schedule AS pps
INNER JOIN payroll_schedule_period AS psp ON pps.`schedule_id` = psp.`schedule_id`
AND psp.`status` = 'Upcoming'
GROUP BY
	pps.`schedule_id`
ORDER BY
	psp.`payroll_schedule_period_id`;";

        return $this->db->query($sql)->result();
    }

    public function fetchRunSchedules() {

        return $this->db->select('rs.*, sp.start_date from, sp.end_date to, ps.schedule_name')
                        ->from(self::TBL_RUN_SCHEDULE . ' as rs')
                        ->join(self::TBL_SCHEDULE_PERIOD . ' as sp', 'rs.payroll_schedule_period_id=sp.payroll_schedule_period_id')
                        ->join(self::TBL_PAY_SCHEDULE . ' as ps', 'ps.schedule_id=sp.schedule_id')
                        ->where('rs.step <', 3)
                        ->get()->result();
    }

    public function addPaymentSchedule($employee_id, $data) {
        $this->db->where('employee_id', $employee_id)
                ->update(self::TBL_COMPENSATION, ['schedule_id' => $data['schedule_id']]);
        return $this->db->affected_rows() > 0;
    }

    public function get_schedule_period($schedule_id) {
        return $this->db
                        ->from(self::TBL_SCHEDULE_PERIOD)
                        ->where('schedule_id', $schedule_id)
                        ->where('status', self::PERIOD_UPCOMING)
                        ->limit(1)
                        ->get()->row();
    }

    public function fetchPaidEmployees($rowClass = stdClass::class) {
        return $this->db->select()
                        ->from(self::TBL_COMPENSATION . ' as c')
                        ->join(Employee_model::TBL_EMP . ' as e', 'e.employee_id=c.employee_id')
                        ->join(self::TBL_STAFF_ALLOWANCES . ' as al', 'al.payroll_staff_allowance_id=c.payroll_staff_allowance_id')
                        ->get()->result();
    }

    public function fetchUnPaidEmployees() {
        $paid_employees = $this->fetchPaidEmployees();
        $skipp = [];
        if (!empty($paid_employees)) {
            foreach ($paid_employees as $employee) {
                array_push($skipp, $employee->employee_id);
            }
        }
        return $this->emp_model->fetchAllEmployees(NULL, $skipp);
    }

    public function fetch_upcoming_periods($ref_id) {
        $sql = "SELECT
                *
            FROM
                    payroll_schedule_period
            WHERE
                    schedule_id =(
                            SELECT
                                    schedule_id
                            FROM
                                    payroll_pay_schedule
                            WHERE
                                    ref_id = '$ref_id'
                    ) and `status` = 'Upcoming'";
        return $this->db->query($sql)->result();
    }

    public function process_step1($data) {
        if (empty($data)) {
            return FALSE;
        }

        $ref = strtoupper(random_string('alnum', 10));
        // Insert into run schedule
        $data_db = [
            'ref_id' => $ref,
            'step' => 2,
            'payroll_schedule_period_id' => $data['period_id'],
            'pay_date' => date('Y-m-d', strtotime(convert_date($data['pay_date']))),
            'inserted_by' => $this->user_auth_lib->get('user_id'),
            'date_created' => date('Y-m-d h:i:s')
        ];

        // update schedule period
        $this->updateSchedulePeriod($data['period_id'], self::PERIOD_ONGOING);

        $this->db->insert(self::TBL_RUN_SCHEDULE, $data_db);

        if (!$this->db->insert_id()) {
            return false;
        }

        return $ref;
    }

    public function updateSchedulePeriod($payroll_schedule_period_id, $status) {
        $this->db->update(self::TBL_SCHEDULE_PERIOD, ['status' => $status], ['payroll_schedule_period_id' => $payroll_schedule_period_id]);
        return $this->db->affected_rows() > 0;
    }

    public function updateRunSchedule($ref_id) {
        $this->db->update(self::TBL_RUN_SCHEDULE, ['step' => 3], ['ref_id' => $ref_id]);
        return $this->db->affected_rows() > 0;
    }

    public function approvePayroll($ref, $data) {
        if (empty($data)) {
            return FALSE;
        }

        // schedule info
        $schedule = $this->getScheduleByRef($ref);
        $run_archive_data = [
            'payroll_run_schedule_id' => $schedule->payroll_run_schedule_id,
            'pay_date' => $schedule->pay_date,
            'inserted_by' => $schedule->inserted_by,
            'status' => $schedule->status,
            'date_created' => $schedule->date_created,
            'schedule_name' => $schedule->schedule_name,
            'ref_id' => $schedule->ref_id,
            'payroll_schedule_period_id' => $schedule->payroll_schedule_period_id,
            'step' => $schedule->step,
        ];

        // loop over emp;
        $data_archive = [];
        foreach ($data['employee_id'] as $employee_id) {

            $compensation = $this->fetchEmployeeCompensation($employee_id)[0];

            $t_deduction = $this->_getTotalDeduction(json_decode($compensation->deductions));
            $data_archive[] = [
                'employee_id' => $employee_id,
                'allowances' => $compensation->components,
                'deductions' => $compensation->deductions,
                'grosspay' => $compensation->grosspay,
                'total_deductions' => $t_deduction,
                'netpay' => $compensation->grosspay - $t_deduction,
                'period_id' => $schedule->payroll_schedule_period_id,
                'schedule_name' => $schedule->schedule_name,
                'payroll_run_schedule_id' => $schedule->payroll_run_schedule_id,
                'payroll_run_ref' => $ref,
                'date_added' => date('Y-m-d H:i:s'),
                'added_by' => $this->user_auth_lib->get('user_id')
            ];
        }

        if (!empty($data_archive)) {
             // update run_schedule as completed
            $this->updateRunSchedule($ref);
            
            $this->db->insert_batch(self::TBL_COMPENSATION_ARCHIVE, $data_archive);
            $this->db->insert(self::TBL_SCHEDULE_ARCHIVE, $run_archive_data);

            //update schedule period
            $this->updateSchedulePeriod($schedule->payroll_schedule_period_id, self::PERIOD_COMPLETED);

            return TRUE;
        }

        return FALSE;
    }

    private function _getTotalDeduction($deductions) {
        $total_deductions = 0;
        if (!empty($deductions)) {
            foreach ($deductions as $ded) {
                $total_deductions += $ded->amount;
            }
        }

        return $total_deductions;
    }

    public function getScheduleByRef($ref) {
        return $this->db->select('rs.*, sp.payroll_schedule_period_id, ps.schedule_name')
                        ->from(self::TBL_RUN_SCHEDULE . ' as rs')
                        ->join(self::TBL_SCHEDULE_PERIOD . ' as sp', 'rs.payroll_schedule_period_id=sp.payroll_schedule_period_id')
                        ->join(self::TBL_PAY_SCHEDULE . ' as ps', 'ps.schedule_id=ps.schedule_id')
                        ->where('rs.ref_id', $ref)
                        ->get()->row();
    }

    public function fetchRecords($ref=null) {
        if($ref){
            $this->db->where('prsa.ref_id', $ref);
        }
        return $this->db->select('prsa.*, psp.start_date, psp.end_date, (select count(1) from payroll_compensation_archive where payroll_run_ref = prsa.ref_id ) as emp_count')
                        ->from(self::TBL_SCHEDULE_ARCHIVE . ' as prsa')
                        ->join(self::TBL_SCHEDULE_PERIOD . ' as psp', 'psp.payroll_schedule_period_id = prsa.payroll_schedule_period_id')
                ->get()->result();
    }
    
    public function fetchArchiveRecords($ref, $employee_id = null) {
        if ($ref == ''){
            return FALSE;
        }
        
        if (!is_null($employee_id)){
            $this->db->where('pca.employee_id', $employee_id);
        }
        
        return $this->db->select('pca.*, e.first_name, e.last_name, e.account_name, e.account_no, b.name as bank_name, b.code as sort_code, e.employment_date')
                ->from(self::TBL_COMPENSATION_ARCHIVE . ' as pca')
                ->join(TBL_EMPLOYEES . ' as e', 'pca.employee_id = e.employee_id')
                ->join(TBL_BANKS . ' as b', 'b.bank_id = e.bank_id', 'left')
                ->where('pca.payroll_run_ref', $ref)
                ->get()->result();
    }

}
