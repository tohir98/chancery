<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Report_model
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_query_builder $db 
 * @property CI_Loader $load loader
 */
class Report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_incomes($income_source_id = null, $account_chart_id = null, $income_type_id = null, $cash_account_id = null, $heading_type_id = null, $start_date = null, $end_date = null) {
        
        if (!is_null($income_source_id) && (int) $income_source_id > 0) {
            $this->db->where('i.income_source_id', $income_source_id);
        }
        if (!is_null($account_chart_id) && (int) $account_chart_id > 0) {
            $this->db->where('i.account_chart_id', $account_chart_id);
        }
        if (!is_null($income_type_id) && (int) $income_type_id > 0) {
            $this->db->where('i.income_type_id', $income_type_id);
        }
        if (!is_null($cash_account_id) && (int) $cash_account_id > 0) {
            $this->db->where('i.cash_account_id', $cash_account_id);
        }
        if (!is_null($heading_type_id) && (int) $heading_type_id > 0) {
            $this->db->where('ac.heading_type_id', $heading_type_id);
        }
        if (!is_null($start_date) && $start_date !== '0') {
            $this->db->where('i.income_date >=', convert_date($start_date));
        }
        if (!is_null($end_date) && $end_date !== '0') {
            $this->db->where('i.income_date <=', convert_date($end_date));
        }
        return $this->db
                        ->select('i.*, is.source_name, ac.description as income_type, ac.code, ac.description')
                        ->from('incomes i')
                        ->join('income_sources is', 'i.income_source_id=is.income_source_id')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=i.account_chart_id')
                        ->order_by('i.income_date', 'desc')
                        ->get()->result(\reports\Income::class);

        //echo $this->db->last_query(); exit;
    }

    public function fetch_expenses($vendor_id = null, $account_chart_id = null, $expense_type_id = null, $cash_account_id = null, $heading_type_id = null, $start_date = null, $end_date = null) {
        
        if (!is_null($vendor_id) && (int) $vendor_id > 0) {
            $this->db->where('e.vendor_id', $vendor_id);
        }
        if (!is_null($account_chart_id) && (int) $account_chart_id > 0) {
            $this->db->where('e.ap_account_id', $account_chart_id);
        }
        if (!is_null($expense_type_id) && (int) $expense_type_id > 0) {
            $this->db->where('e.expense_type_id', $expense_type_id);
        }
        if (!is_null($cash_account_id) && (int) $cash_account_id > 0) {
            $this->db->where('e.cash_account_id', $cash_account_id);
        }
        if (!is_null($heading_type_id) && (int) $heading_type_id > 0) {
            $this->db->where('ac.heading_type_id', $heading_type_id);
        }
        if (!is_null($start_date) && $start_date !== '0') {
            $this->db->where('e.incured_date >=', convert_date($start_date));
        }
        if (!is_null($end_date) && $end_date !== '0') {
            $this->db->where('e.incured_date <=', convert_date($end_date));
        }
        return $this->db
                        ->select('e.*, ac.description as expense_type, ac.code as expense_code, v.vendor_name')
                        ->from(TBL_EXPENDITURE . ' as e')
                        ->join(TBL_ACCOUNT_CHART . ' as ac', 'ac.account_chart_id=e.ap_account_id', 'left')
                        ->join(TBL_VENDOR . ' as v', 'v.vendor_id=e.vendor_id')
                        ->order_by('e.incured_date', 'DESC')
                        ->get()->result(\reports\Expenses::class);

        //\reports\Income::class; exit;
    }

}
