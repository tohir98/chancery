<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
//$route['default_controller'] = 'users/Users_controller';
$route['login']  = 'users/users_controller/login';
$route['logout']  = 'users/users_controller/logout';
$route['verify_email']  = 'users/users_controller/verify_email';
$route['verify_email/(:num)']  = 'users/users_controller/verify_email/$1';
$route['verify_email/(:num)/(:any)']  = 'users/users_controller/verify_email/$1/$2';
$route['verify_email/(:num)/(:any)/(:any)']  = 'users/users_controller/verify_email/$1/$2/$3';
$route['admin']  = 'users/users_controller/super_admin';
$route['admin/dashboard']  = 'users/admin_controller/dashboard';
$route['account/dashboard']  = 'users/admin_controller/dashboard';
$route['account/edit_profile']  = 'users/users_controller/edit_profile';


$route['setup/employees']  = 'setup/employee_controller/employees';
$route['setup/edit_status']  = 'setup/employee_controller/edit_status';
$route['setup/add_employee']  = 'setup/employee_controller/add_employee';
$route['setup/deleteEmployee/(\d+)']  = 'setup/employee_controller/deleteEmployee/$1';
$route['setup/change_emp_picture/(\d+)']  = 'setup/employee_controller/change_emp_picture/$1';
$route['setup/remove_emp_picture/(\d+)']  = 'setup/employee_controller/remove_emp_picture/$1';
$route['setup/editEmployee/(\d+)']  = 'setup/employee_controller/editEmployee/$1';
$route['setup/view_record/(\d+)']  = 'setup/employee_controller/view_record/$1';
$route['setup/vendors/add_vendor']  = 'setup/setup_controller/add_vendor';
$route['setup/fixed_assets/new']  = 'setup/setup_controller/new_asset';
$route['setup/fixed_assets/edit/(:any)']  = 'setup/setup_controller/edit_asset/$1';
$route['setup']  = 'setup/setup_controller';
$route['setup/(:any)']  = 'setup/setup_controller/$1';
$route['setup/(:any)/(:any)']  = 'setup/setup_controller/$1/$2';
$route['setup/(:any)/(:any)/(:any)']  = 'setup/setup_controller/$1/$2/$3';
$route['setup/profile']  = 'setup/setup_controller/profile';
$route['setup/profile/add_profile']  = 'setup/setup_controller/add_profile';


$route['forgot_password']  = 'users/users_controller/forgot_password';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['administration']  = 'users/users_controller';
$route['administration/(:any)']  = 'users/users_controller/$1';
$route['administration/(:any)/(:any)']  = 'users/users_controller/$1/$2';

$route['transaction']  = 'transaction/transaction_controller';
$route['transaction/incomes']  = 'transaction/transaction_controller/incomes';
$route['transaction/incomes/add_income']  = 'transaction/transaction_controller/add_income';
$route['transaction/incomes/add_receivable']  = 'transaction/transaction_controller/add_receivable';
$route['transaction/incomes/add_cash_receipts']  = 'transaction/transaction_controller/add_cash_receipts';
$route['transaction/incomes/getARBalance/(:num)']  = 'transaction/transaction_controller/getARBalance/$1';
$route['transaction/incomes/getARBalance/(:num)/(:num)']  = 'transaction/transaction_controller/getARBalance/$1/$2';

$route['transaction/incomes/getCashBalance/(:any)']  = 'transaction/transaction_controller/getCashBalance/$1';
$route['transaction/incomes/getCashBalance/(:num)/(:num)']  = 'transaction/transaction_controller/getCashBalance/$1/$2';
$route['transaction/incomes/getBankBalance/(:any)']  = 'transaction/transaction_controller/getBankBalance/$1';
$route['transaction/incomes/add_deposit']  = 'transaction/transaction_controller/add_deposit';

// expenses
$route['transaction/expenses']  = 'transaction/expense_controller/index';
$route['transaction/expenses/add_payable']  = 'transaction/expense_controller/add_payable';
$route['transaction/expenses/add_disbursement']  = 'transaction/expense_controller/add_disbursement';
$route['transaction/expenses/add_paid_invoices']  = 'transaction/expense_controller/add_paid_invoices';

$route['transaction/expenses/getAPBalance/(:num)']  = 'transaction/expense_controller/getAPBalance/$1';
$route['transaction/expenses/getAPBalance/(:num)/(:num)']  = 'transaction/expense_controller/getAPBalance/$1/$2';

$route['transaction/expenses/getBankBalance/(:num)']  = 'transaction/expense_controller/getBankBalance/$1';

// payroll
$route['payroll']  = 'payroll/payroll_controller/index';
$route['payroll/(:any)']  = 'payroll/payroll_controller/$1';
$route['payroll/(:any)/(:any)']  = 'payroll/payroll_controller/$1/$2';
$route['payroll/(:any)/(:any)/(:any)']  = 'payroll/payroll_controller/$1/$2/$3';

// report
$route['report']  = 'report/report_controller/index';
$route['report/(:any)']  = 'report/report_controller/$1';
$route['report/(:any)/(:any)']  = 'report/report_controller/$1/$2';
$route['report/(:any)/(:any)/(:any)']  = 'report/report_controller/$1/$2/$3';

// report
$route['personnel']  = 'personnel/personnel_controller/index';
$route['personnel/(\d+)/view_record']  = 'personnel/personnel_controller/view_record/$1';
$route['personnel/(\d+)/print_pdf']  = 'personnel/personnel_controller/print_pdf/$1';
$route['personnel/(\d+)/edit_record']  = 'personnel/personnel_controller/edit_profile/$1';
$route['personnel/(\d+)/edit_residence_info']  = 'personnel/personnel_controller/edit_residence_info/$1';
$route['personnel/(\d+)/edit_health_info']  = 'personnel/personnel_controller/edit_health_info/$1';
$route['personnel/(\d+)/edit_home_parish']  = 'personnel/personnel_controller/edit_home_parish/$1';
$route['personnel/(\d+)/edit_family_contact']  = 'personnel/personnel_controller/edit_family_contact/$1';
//$route['personnel/(\d+)/editSeAssignment']  = 'personnel/personnel_controller/editSeAssignment/$1';
$route['personnel/(:any)']  = 'personnel/personnel_controller/$1';
$route['personnel/(:any)/(:any)']  = 'personnel/personnel_controller/$1/$2';
$route['personnel/(:any)/(:any)/(:any)']  = 'personnel/personnel_controller/$1/$2/$3';
$route['personnel/(:any)/(:any)/(:any)/(:any)']  = 'personnel/personnel_controller/$1/$2/$3/$4';