<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




/*
|--------------------------------------------------------------------------
| Account Types
|--------------------------------------------------------------------------
|
*/
define('ACCOUNT_TYPE_FREE_TRIAL', 1);
define('ACCOUNT_TYPE_PREMIUM', 2);

/*
|--------------------------------------------------------------------------
| User Types
|--------------------------------------------------------------------------
*/
define('USER_TYPE_SUPER_ADMIN', 1);
define('USER_TYPE_AGENT', 2);
define('USER_TYPE_ORDINRY', 2);
define('USER_TYPE_BRANCH', 3);

/*
|--------------------------------------------------------------------------
| User Status
|--------------------------------------------------------------------------
*/
define('USER_STATUS_ACTIVE', 1);
define('USER_STATUS_INACTIVE', 0);
define('USER_STATUS_SUSPENDED', 2);
define('USER_STATUS_TERMINATED', 3);

/*
|--------------------------------------------------------------------------
| Account Status
|--------------------------------------------------------------------------
*/
define('ACCOUNT_STATUS_ACTIVE', 1);
define('ACCOUNT_STATUS_INACTIVE', 0);

/*
|--------------------------------------------------------------------------
| Personnel Status
|--------------------------------------------------------------------------
*/
define('PERSONNEL_ACTIVE', 1);
define('PERSONNEL_INACTIVE', 0);
define('PERSONNEL_EXPELLED', 2);

// Businnes Name
define('BUSINESS_NAME', 'Chancery');
define('REPLY_EMAIL', 'no_reply@chancery.com');
define('DEFAULT_PASSWORD', 'password');

define('REG_CONFIRMATION', 'Registration Confirmation Email');

/*
  | ---------------------------------------------------
  |	Temp directory for storing files
  | ---------------------------------------------------
 */
define('FILE_PATH', FCPATH . 'files/');

if (!is_dir(FILE_PATH)) {
    @mkdir(FILE_PATH, 0777, true);
}

define('FILE_PATH_EMP_PIC', FILE_PATH . 'employee_pictures/');
if (!is_dir(FILE_PATH_EMP_PIC)) {
    @mkdir(FILE_PATH_EMP_PIC, 0777, true);
}


/*
|--------------------------------------------------------------------------
| DB Tables
|--------------------------------------------------------------------------
*/
define('TBL_USERS', 'users');
define('TBL_EMPLOYEES', 'employees');
define('TBL_SUPER_ADMIN', 'super_admin');
define('TBL_MODULE_PERMS', 'module_perms');
define('TBL_USER_PERMS', 'user_perms');
define('TBL_COUNTRIES', 'countries');
define('TBL_BANK_ACCOUNT', 'bank_accounts');
define('TBL_BANKS', 'banks');
define('TBL_ACCOUNT_CHART', 'account_chart');
define('TBL_ACCOUNT_BALANCE', 'account_balances');
define('TBL_HEADING_TYPE', 'heading_type');
define('TBL_ASSETS', 'assets');
define('TBL_LIABILITY', 'liabilities');
define('TBL_EXPENDITURE', 'expenses');
define('TBL_INCOME', 'incomes');
define('TBL_INCOME_SOURCE', 'income_sources');
define('TBL_VENDOR', 'vendors');
define('TBL_RECEIVABLES', 'receivables');
define('TBL_CASH', 'cash');
define('TBL_PAYABLES', 'payables');
define('TBL_DISBURSEMENT', 'disbursements');

// Heading Types
define('OTHER_ASSETS', 1);
define('CURRENT_ASSETS', 2);
define('RECEIVABLES', 3);
define('PREPAYMENT', 4);
define('CASH', 5);
define('LIABILITY', 6);
define('RE', 7);
define('PYFB', 8);
define('EQUITY', 9);
define('INCOME', 10);
define('EXPENSE', 11);

//Modules
define('SETUP_MODULE', 4);
define('PERSONNEL_MODULE', 5);

// Account Sub Categories
define('ACCOUNT_BANK', 3);

//Status
define('ACTIVE', 1);
define('INACTIVE', 0);

//Income Types
define('INCOME_RECEIVABLE', 1);
define('INCOME_CASH_RECEIPT', 2);
define('INCOME_DEPOSIT', 3);

//Expense Types
define('EXPENSE_PAYABLE', 1);
define('EXPENSE_DISBURSEMENT', 3);
define('EXPENSE_PAID_INVOICES', 2);

//Quarters
define('FIRST_QUARTER', 1);
define('SECOND_QUARTER', 2);
define('THIRD_QUARTER', 3);
define('FOURTH_QUARTER', 4);


// Pay periods
define('PERIOD_MONTHLY', 1);


define('PERSONNEL_PRIST', 1);
define('PERSONNEL_REV_SISTER', 2);
define('PERSONNEL_SEMINARIANS', 3);

//Defautl Birthday message
define('BIRTHDAY_MSG', "On this auspicious occasion of your Birthday, we felicitate with you and pray the good Lord to grant you wisdom, spiritual insight, physical well-being and many more fruitful years with the grace of final perseverance. Have a healthy, happy and holy Birthday! +Alfred Adewale Martins {From the Chancery}");

//Modules
define('MODULE_PERSONNEL', 5);


define('SMS_LENGTH', 140);

