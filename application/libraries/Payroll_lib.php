<?php

require_once 'Lib.php';

/**
 * Description of Payroll_lib
 *
 * @author tohir
 * @property Payroll_model $p_model Description
 * @property Excel_lib $excel_lib Description
 * @property CI_Loader $load Description
 */
class Payroll_lib extends Lib {

    public function __construct() {
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->load->library('excel_lib');
    }

    public function calculate_monthly($schedule_id, $params) {

        $dates = array(
            'monthly_from' => $params['date_from'],
            'monthly_to' => $params['date_to'],
            'monthly_pay_date' => $params['pay_date']
        );

        $result = array();


        $monthlyFrom = strtotime($dates['monthly_from']);
        $monthlyTo = strtotime($dates['monthly_to']);
        $monthlyPayDate = strtotime($dates['monthly_pay_date']);


        $result[] = array(
            'start_date' => date('Y-m-d', $monthlyFrom),
            'end_date' => date('Y-m-d', $monthlyTo),
            'pay_date' => date('Y-m-d', $monthlyPayDate),
            'status' => 'Upcoming',
            'period_id' => 1,
            'schedule_id' => $schedule_id
        );

        $startMonth = date('n', $monthlyFrom);
        $payDayOfMonth = date('j', $monthlyPayDate);
        $startYear = date('Y', $monthlyFrom);

        for ($i = 1; $i < 12; $i++) {
            $currentMonth = ($startMonth + $i) % 12;
            $currentYear = $startYear + floor(($startMonth + $i - 1) / 12);

            $firstDayOfMonth = mktime(0, 0, 0, $currentMonth, 1, $currentYear);
            $daysInMonth = date('t', $firstDayOfMonth);
            $payDayThisMonth = $payDayOfMonth > $daysInMonth ? $daysInMonth : $payDayOfMonth;

            $result[$i] = array(
                'start_date' => date('Y-m-d', $firstDayOfMonth),
                'end_date' => date('Y-m-t', $firstDayOfMonth),
                'pay_date' => date('Y-m-d', mktime(0, 0, 0, $currentMonth, $payDayThisMonth, $currentYear)),
                'status' => 'Upcoming',
                'period_id' => $i + 1,
                'schedule_id' => $schedule_id
            );
        }

        return $this->db->insert_batch(Payroll_model::TBL_SCHEDULE_PERIOD, $result);
    }

    public function cancel_payrun($ref_id) {
        if (empty($ref_id)) {
            return FALSE;
        }

        $run_info = $this->db->get_where(Payroll_model::TBL_RUN_SCHEDULE, ['ref_id' => $ref_id])->row();
        $this->p_model->updateSchedulePeriod($run_info->payroll_schedule_period_id, Payroll_model::PERIOD_UPCOMING);

        $this->db->delete(Payroll_model::TBL_RUN_SCHEDULE, ['ref_id' => $ref_id]);

        return $this->db->affected_rows() > 0;
    }

    public function download_excel($ref) {
        $schedule = $this->p_model->fetchRecords($ref)[0];
        
        $records = $this->p_model->fetchArchiveRecords($ref);

        $intro_data = array();
        $intro_data[] = array('Company Name', BUSINESS_NAME);
        $intro_data[] = array('Payroll Schedule', $schedule->schedule_name);
        $intro_data[] = array('Period', date('d-M-Y', strtotime($schedule->start_date)) . ' to ' . date('d-M-Y', strtotime($schedule->end_date)));
        $intro_data[] = array('Pay Date', date('d-M-Y', strtotime($schedule->pay_date)));
        $intro_data[] = array('No. of Employees', $schedule->emp_count);
        $intro_data[] = array('', '');
        $intro_data[] = array('Approved By', '');
        
        $payroll_detail_data[] = array('Staff ID', 'First Name', 'Last Name', 'Basic Salary', 'Housing', 'Transport', 'Allowances', 'Regular Pay', 'Gross Salary', 'Deductions', 'Take Home Pay');
        
        $summary_data = [];
        $summary_data[] = array('Staff ID', 'Fname', 'Lname', 'Take Home Pay', 'Account Name', 'Bank', 'Account Number', 'Sort Code');
        
        foreach ($records as $record) {

            $components = $this->_fetch_components($record->allowances, $record->deductions);

            $payroll_detail_data[] = array($record->employee_id,
                $record->first_name,
                $record->last_name,
                number_format($components['basic'], 2),
                number_format($components['housing'], 2),
                number_format($components['transport'], 2),
                number_format($components['other_all'], 2),
                number_format($components['regular_pay'], 2),
                number_format($record->grosspay, 2),
                number_format($record->total_deductions, 2),
                number_format($record->netpay, 2)
            );
            
            $summary_data[] = [$record->employee_id, $record->first_name, $record->last_name, number_format($record->netpay, 2), $record->account_name, $record->bank_name, $record->account_no, $record->sort_code];
        }
        
        $from = date('dMY', strtotime($schedule->start_date));
        $to = date('dMY', strtotime($schedule->end_date));
        $download = BUSINESS_NAME . "_Payroll_" . $from . 'to' . $to;
        
        $this->excel_lib->payroll_report($intro_data, $payroll_detail_data, $summary_data, $download);
        
    }
    
    private function _fetch_components($allowance_json, $deduction_json){
        $transport = 0;
        $basic = 0;
        $housing = 0;
        $other_all = 0;
        $total_deduction = 0;

        $allowances = json_decode($allowance_json);
        $deductions = json_decode($deduction_json);

        foreach ($allowances as $allowance) {

            if (strtolower($allowance->title) == 'basic') {
                $basic = $allowance->amount;
            } elseif (strtolower($allowance->title) == 'housing') {
                $housing = $allowance->amount;
            } elseif (strtolower($allowance->title) == 'transport') {
                $transport = $allowance->amount;
            } else {
                $other_all += $allowance->amount;
            }
        }
        
        if (!empty($deductions)) {
            foreach ($deductions as $deduction) {
                $total_deduction += $deduction->amount;
            }
        }

        return [
            'basic' => $basic,
            'housing' => $housing,
            'transport' => $transport,
            'other_all' => $other_all,
            'total_deduction' => $total_deduction,
            'regular_pay' => $basic + $housing + $transport,
        ];

    }

}
