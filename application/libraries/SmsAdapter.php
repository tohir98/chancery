<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Lib.php';

use sms\Sms_lib;

//require 'sms/smslib.php';

/**
 * Description of SmsAdapter
 *
 * @author chuksolloh
 */
class SmsAdapter extends Lib {

    private $CI; //CI instance

    const TBL_SMS = 'sms_messages';

    //put your code here
    protected static $mobiles;
    protected static $message;

    public function __construct() {
        parent::__construct();
        $this->CI = get_instance();
        $this->CI->load->database();
    }

    public static function processViaSMSRoute($mobiles, $message, $sender_id = '') {
        if ($mobiles != '' && $message != '') {

            self::$mobiles = $mobiles;
            self::$message = $message;

            // log inside sms table
            $CI = &get_instance();
            $CI->load->database();
            $CI->db->insert(self::TBL_SMS, [
                'module_id' => MODULE_PERSONNEL,
                'sender_id' => $sender_id ? : 'Catholic',
                'msg_type' => 'text',
                'message' => $message,
                'date' => date('Y-m-d'),
                'time' => time('H:i:s'),
                'units' => strlen($message) / SMS_LENGTH,
                'status' => 0,
                'phone_number' => $mobiles,
            ]);

            return Sms_lib::sendSms(self::$mobiles, self::$message, $sender_id);
        }
    }

}
