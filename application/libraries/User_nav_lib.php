<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Page_nav.php';

/**
 * Eduportal
 * Company User nav lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage User_nav
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright Â© 2015 EduPortal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property User_auth_lib $user_auth_lib Description
 */
class User_nav_lib extends Page_nav {

    protected $CI;
    private $user_id;
    private $account_type;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('user_auth_lib');
        $this->CI->load->helper('user_nav_helper');

        // Load model
        $this->CI->load->model('user/user_model');

        $this->user_id = $this->CI->user_auth_lib->get("user_id");
        $this->account_type = $this->CI->user_auth_lib->get("access_level");
    }

    public function get_user_link() {
        $user_type = $this->CI->user_auth_lib->get('access_level');

        if ($user_type == USER_TYPE_SUPER_ADMIN) {
            $user_link = 'admin';
        } elseif ($user_type == USER_TYPE_AGENT) {
            $user_link = 'agent';
        } elseif ($user_type == USER_TYPE_BRANCH) {
            $user_link = 'branch';
        } else {
            log_message('error', 'Unknown user type');
            $user_link = '';
        }

        return $user_link;
    }

    public function get_assoc() {
        if ((!$this->account_type) or ( !$this->user_id)) {
            return false;
        }

        // Fetch user permissions
        $result = $this->CI->user_model->fetch_user_groups_modules_perms($this->user_id);
       
        if (!$result) {
            return false;
        }

        $a = array();

        foreach ($result as $row) {

            if ($row['module_subject'] != '') {
                $a[$row['module_subject']]['subject'] = $row['module_subject'];
                $a[$row['module_subject']]['items'][] = $row;
                $a[$row['module_subject']]['id_string'] = $row['module_id_string'];
            }
        }

        return $a;
    }

    public function get_top_menu() {
        return array(
            'dashboard_url' => '/' . $this->get_user_link() . '/dashboard',
            'logout_url' => site_url('/logout'),
            'display_name' => $this->CI->user_auth_lib->get('display_name')
        );
    }

    public function company_name() {
        return BUSINESS_NAME;
    }

}
