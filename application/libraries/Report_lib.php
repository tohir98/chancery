<?php

require_once 'Lib.php';

/**
 * Description of Report_lib
 *
 * @author tohir
 * @property Report_model $r_model Description
 * @property Excel_lib $excel_lib Description
 * 
 */
class Report_lib extends Lib {

    public function __construct() {
        $this->load->library('excel_lib');
    }

    public function income_report_excel($income_source_id, $account_chart_id, $income_type_id, $cash_account_id, $heading_type_id, $start_date, $end_date) {
        $incomes = $this->r_model->fetch_incomes($income_source_id, $account_chart_id, $income_type_id, $cash_account_id, $heading_type_id, $start_date, $end_date);

        $incomes = json_decode(json_encode($incomes));

        $income_data[] = array('Date', 'Income Type', 'Source', 'Pay Item', 'Amount', 'Memo');

        foreach ($incomes as $income) {

            $income_data[] = array(
                $income->income_date,
                $income->income_type,
                $income->income_source,
                $income->pay_item,
                $income->amount,
                $income->memo
            );
        }

        $download = BUSINESS_NAME . "_Income_Report_" . time();



        $this->excel_lib->income_report($income_data, $download);
    }

    public function expense_report_excel($vendor_id, $account_chart_id, $expense_type_id, $cash_account_id, $heading_type_id, $start_date, $end_date) {
        $expenses = $this->r_model->fetch_expenses($vendor_id, $account_chart_id, $expense_type_id, $cash_account_id, $heading_type_id, $start_date, $end_date);

        $expenses = json_decode(json_encode($expenses));

        $expense_data[] = array('Date', 'Expense Type', 'Payee', 'Pay Item', 'Amount', 'Memo');

        foreach ($expenses as $expense) {

            $expense_data[] = array(
                $expense->incured_date,
                $expense->expense_type,
                $expense->vendor,
                $expense->pay_item,
                $expense->amount,
                $expense->memo
            );
        }

        $download = BUSINESS_NAME . "_Expense_Report_" . time();

        $this->excel_lib->expense_report($expense_data, $download);
    }

    public function bank_report_excel($account_types, $bank_data) {


        $bank_details[] = array('SN', 'Bank Name', 'Account Name', 'Account Number', 'Account Type');

        $j = 0;
        foreach ($bank_data as $bank) {

            $bank_details[] = array(
                ++$j,
                $bank->name,
                $bank->account_name,
                $bank->account_number,
                $account_types[$bank->account_type_id]
            );
        }

        $download = BUSINESS_NAME . "_Bank_Report";

        $this->excel_lib->bank_report($bank_details, $download, "Bank Report");
    }

    public function asset_report_excel($assets) {


        $asset_details[] = array('SN', 'Asset Name', 'Asset No', 'Account Category', 'Price', 'Purchase Date');

        $j = 0;
        foreach ($assets as $asset) {

            $asset_details[] = array(
                ++$j,
                $asset->asset_name,
                $asset->reg_no,
                $asset->description . "( {$asset->code} )",
                number_format($asset->purchase_price, 2),
                date('d-M-Y', strtotime($asset->purchase_date))
            );
        }

        $download = BUSINESS_NAME . "_Fixed_Asset_Report";

        $this->excel_lib->bank_report($asset_details, $download, "Asset Report");
    }

    public function liability_report_excel($liabilities) {


        $details[] = array('SN', 'Item', 'Code', 'Account Category', 'Sub Category');

        $j = 0;
        foreach ($liabilities as $liability) {

            $details[] = array(
                ++$j,
                ucwords($liability->description),
                $liability->code,
                $liability->category,
                $liability->sub_category
            );
        }

        $download = BUSINESS_NAME . "_Liability_Report";

        $this->excel_lib->bank_report($details, $download, "Liability Report");
    }

}
