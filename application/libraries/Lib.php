<?php

/**
 * Description of Lib
 *
 * @author tohir
 * @property CI_DB_query_builder $db Description
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_Loader $load Description
 */
abstract class Lib {
    
    public function __construct() {
        $this->load->database();
    }

    //put your code here

    public function __get($name) {
        return get_instance()->$name;
    }
}
