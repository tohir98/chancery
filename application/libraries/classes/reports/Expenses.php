<?php

namespace reports;

/**
 * Description of Expenses
 *
 * @author tohir
 */
class Expenses extends \db\Entity {
    public function __construct() {
        if (!$this->user_auth_lib) {
            $this->load->library('user_auth_lib');
        }
    }
    public function jsonSerialize() {
        return [
            'incured_date' => date('d-M-Y', strtotime($this->incured_date)),
            'expense_type' => $this->user_auth_lib->get_expense_types()[$this->expense_type_id],
            'amount' => number_format($this->amount, 2),
            'memo' => $this->memo,
            'incured_date' => $this->incured_date,
            'vendor' => $this->vendor_name,
            'pay_item' => $this->expense_type,
            'code' => $this->expense_code,
        ];
    }
}
