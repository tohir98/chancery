<?php

namespace reports;

/**
 * Description of Income
 *
 * @author tohir
 * @property \CI_Loader $load Description
 */
class Income extends \db\Entity {
    public function __construct() {
        if (!$this->user_auth_lib) {
            $this->load->library('user_auth_lib');
        }
    }
    public function jsonSerialize() {
        return [
            'amount' => number_format($this->amount, 2),
            'income_source' => $this->source_name,
            'memo' => $this->memo,
            'pay_item' => $this->description,
            'income_type' => $this->user_auth_lib->get_income_types()[$this->income_type_id],
            'code' => $this->code,
            'date_created' => date('d-M-Y', strtotime($this->date_created)),
            'income_date' => date('d-M-Y', strtotime($this->income_date)),
        ];
    }
}
