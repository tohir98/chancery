<?php

namespace personnel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personnel
 *
 * @author tohir
 */
class Personnel extends \db\Entity {
    public function jsonSerialize() {
        return [
            'personnel_id' => $this->personnel_id,
            'personnel_type' => $this->personnel_type,
            'title' => $this->title,
            'surname' => strtoupper($this->surname),
            'other_names' => ucwords($this->other_names),
            'dob' => $this->dob !== '0000-00-00 00:00:00' ? date('d-M-Y', strtotime($this->dob)) : 'N/A',
            'place_of_birth' => $this->place_of_birth,
            'nationality' => $this->nationality,
            'phone_number_1' => $this->phone_number_1,
            'phone_number_2' => $this->phone_number_2,
            'email' => $this->email,
            'picture_url' => $this->picture_url,
            'status' => $this->status,
            'dateof_reli_prof' => $this->dateof_reli_prof,
            'congregation_name' => $this->congregation_name,
            'current_assignment' => $this->current_assignment,
            'current_position_id' => $this->current_position_id,
            'title_name' => $this->title_name,
            'state' => $this->state,
            'lgovt' => $this->lgovt,
        ];
    }
}
