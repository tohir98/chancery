<?php

namespace employees;

/**
 * Description of Employee
 *
 * @author tohir
 */
class Employee extends \db\Entity {
    public function jsonSerialize() {
        return [
            'first_name' => ucfirst($this->first_name),
            'last_name' => ucfirst($this->last_name),
            'email' => $this->email
        ];
    }
}
