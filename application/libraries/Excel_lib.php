<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Excel_lib
 *
 * @author tohir
 */
class Excel_lib {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;
    private $col_letters = array(
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U',
        21 => 'V',
        22 => 'W',
        23 => 'X',
        24 => 'Y',
        25 => 'Z',
        26 => 'AA',
        27 => 'AB',
        28 => 'AC',
        29 => 'AD',
        30 => 'AE',
        31 => 'AF',
        32 => 'AG',
        33 => 'AH',
        34 => 'AI',
        35 => 'AJ',
        36 => 'AK',
        37 => 'AL',
        38 => 'AM',
        39 => 'AN',
        40 => 'AO',
        41 => 'AP',
        42 => 'AQ',
        43 => 'AR',
        44 => 'AS',
        45 => 'AT',
        46 => 'AU',
        47 => 'AV',
        48 => 'AW',
        49 => 'AX',
        50 => 'AY',
        51 => 'AZ',
        52 => 'BA',
        53 => 'BB',
        54 => 'BC',
        55 => 'BD',
        56 => 'BE',
        57 => 'BF',
        58 => 'BG',
        59 => 'BH',
        60 => 'BI',
        61 => 'BJ',
        62 => 'BK',
        63 => 'BL',
        64 => 'BM',
        65 => 'BN',
        66 => 'BO',
        67 => 'BP',
        68 => 'BQ',
        69 => 'BR',
        70 => 'BS',
    );

    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
    }

    public function payroll_report($pay_detail, $payroll_detail_data, $summary_data, $name) {

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Zend Solution");
        $objPHPExcel->getProperties()->setLastModifiedBy("Zend Solution");
        $objPHPExcel->getProperties()->setTitle("Payroll Report");
        $objPHPExcel->getProperties()->setSubject("Payroll Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Intro');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F14')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('G4:G14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $rowCount = 4;
        foreach ($pay_detail as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[1]);
            $rowCount++;
        }

        //Payroll detail
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Payroll Detail');
        $objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells('K2:M2');
        $objPHPExcel->getActiveSheet()->SetCellValue('K2', 'PAYE Allowances');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $count = sizeof($payroll_detail_data[0]);
        for ($k = 0; $k <= $count - 1; ++$k) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$k + 1])->setWidth(15);
        }

        $rowCount = 3;
        foreach ($payroll_detail_data as $dat) {
            for ($k = 0; $k <= $count - 1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k + 1] . $rowCount, $dat[$k]);
            }
            $rowCount++;
        }

        // Compensation
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(2);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Payroll Summary');
        $rowCount = 3;
        $objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Payroll Summary');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
         $count = sizeof($summary_data[0]);
        for ($k = 0; $k <= $count - 1; ++$k) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$k + 1])->setWidth(15);
        }


        $objPHPExcel->getActiveSheet()->getStyle('H1:H500')
                ->getNumberFormat()
                ->setFormatCode(
                        '0000000000'
        );
        $objPHPExcel->getActiveSheet()->getStyle('I1:I500')
                ->getNumberFormat()
                ->setFormatCode(
                        '0000000000'
        );
        
        $objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFont()->setBold(true);
        $grandTotal = 0;
        
        foreach ($summary_data as $dat) {
            for ($k = 0; $k <= $count - 1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k + 1] . $rowCount, $dat[$k]);
            }
            $grandTotal += number_unformat($dat[3]);
            $rowCount++;
        }

        
        // Grand total of take home pay
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowCount . ':N' . $rowCount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Total');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($grandTotal, 2));

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function income_report($income_data, $download) {
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Zend Solution");
        $objPHPExcel->getProperties()->setLastModifiedBy("Zend Solution");
        $objPHPExcel->getProperties()->setTitle("Income Report");
        $objPHPExcel->getProperties()->setSubject("Income Report");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Income Report');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F500')->getFont()->setBold(true);
 
        $rowCount = 3;
        $count = sizeof($income_data[0]);
        $objPHPExcel->getActiveSheet()->getStyle('B3:G3')->getFont()->setBold(true);
        for ($k = 0; $k <= $count - 1; ++$k) {
            
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$k + 1])->setWidth(15);
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        
        foreach ($income_data as $dat) {
            for ($k = 0; $k <= $count - 1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k + 1] . $rowCount, $dat[$k]);
            }
            $rowCount++;
        }
        
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $download . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function expense_report($expense_data, $download) {
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Zend Solution");
        $objPHPExcel->getProperties()->setLastModifiedBy("Zend Solution");
        $objPHPExcel->getProperties()->setTitle("Expense Report");
        $objPHPExcel->getProperties()->setSubject("Expense Report");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Expense Report');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F500')->getFont()->setBold(true);
 
        $rowCount = 3;
        $count = sizeof($expense_data[0]);
        $objPHPExcel->getActiveSheet()->getStyle('B3:G3')->getFont()->setBold(true);
        for ($k = 0; $k <= $count - 1; ++$k) {
            
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$k + 1])->setWidth(15);
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        
        foreach ($expense_data as $dat) {
            for ($k = 0; $k <= $count - 1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k + 1] . $rowCount, $dat[$k]);
            }
            $rowCount++;
        }
        
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $download . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function bank_report($bank_data, $download, $reportTitle) {
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Zend Solution");
        $objPHPExcel->getProperties()->setLastModifiedBy("Zend Solution");
        $objPHPExcel->getProperties()->setTitle($reportTitle);
        $objPHPExcel->getProperties()->setSubject($reportTitle);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($reportTitle);
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F500')->getFont()->setBold(true);
 
        $rowCount = 3;
        $count = sizeof($bank_data[0]);
        $objPHPExcel->getActiveSheet()->getStyle('B3:G3')->getFont()->setBold(true);
        for ($k = 0; $k <= $count - 1; ++$k) {
            
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$k + 1])->setWidth(15);
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        
        foreach ($bank_data as $dat) {
            for ($k = 0; $k <= $count - 1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k + 1] . $rowCount, $dat[$k]);
            }
            $rowCount++;
        }
        
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $download . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

}
