<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Notification helper
 * display error, success or notification errors
 */
if (!function_exists('show_notification')) {

    function show_notification() {
        //Display error/success message 
        $CI = & get_instance();  //get instance, access the CI superobject

        if ($CI->session->flashdata('type') != NULL) {
            ?>
            <div class="alert alert-<?php echo $CI->session->flashdata('type') ?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $CI->session->flashdata('msg'); ?>
            </div>
            <?php
        }
    }

}


if (!function_exists('notify')) {

    function notify($type, $message, $redirect_url = null) {
        if (empty($type) || empty($message)) {
            return false;
        }

        $CI = & get_instance();

        $CI->session->set_flashdata('type', $type);
        $CI->session->set_flashdata('msg', $message);
        if (!empty($redirect_url)) {
            redirect($redirect_url, 'refresh');
        }
    }

}



if (!function_exists('show_no_data')) {

    function show_no_data($descriptive_text = null) {
        $msg = '<div class=\"direct-chat-msg\">
                      <img class="direct-chat-img" src="/img/chan_logo.png" alt="message user image">
                      <div class="direct-chat-text"><b>'.BUSINESS_NAME.'</b><br>
                        <p>' . $descriptive_text . '</p>
                      </div>
                    </div>';

        return $msg;
    }

}