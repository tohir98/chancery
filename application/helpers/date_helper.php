<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('conver_date')) {

    /**
     * Convert date into Y/m/d from d/m/Y
     * @param type $date
     * @return boolean
     */
    function convert_date($date = '') {
        if ($date == '') {
            return FALSE;
        }

        $split = explode('/', $date);

        return $split[2] . '-' . $split[1] . '-' . $split[0];
    }

}