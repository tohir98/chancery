<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Navigation helper
 */
if (!function_exists('build_top_nav_part')) {

    function build_top_nav_part() {
        
    }

}

if (!function_exists('build_sidebar_nav_part')) {

    function build_sidebar_nav_part($nav_menu, $path) {
        $buffer = '';


        $buffer .= '<ul class="sidebar-menu">';
        if (!empty($nav_menu)) {
            foreach ($nav_menu as $module => $item) {
                $active = strstr($path, $item['id_string']) ? 'active' : '';
                $buffer .= '<li class="' . $active . '">
                <a href="#">
                <i class="fa fa-' . nav_icon($item['id_string']) . '"></i> <span>' . $module . '</span> <i class="fa fa-angle-left pull-right"></i>
              </a>';
                if (count($item) > 0) {

                    $buffer .= '<ul class="treeview-menu">';
                    foreach ($item['items'] as $sub) {

                        $buffer .= '<li>'
                            . '<a href="' . site_url($sub['module_id_string'] . '/' . $sub['perm_id_string']) . '"><i class="fa fa-arrow-right"></i> ' . $sub['perm_subject'] . '</a></li>';
                    }
                    $buffer .= '</ul>';
                }
                $buffer .= '</li>';
            }
        }
        $buffer .= '</ul>';
        return $buffer;
    }

}

if (!function_exists('user_type_status')) {

    function user_type_status($access_level) {
        $arr = array(
            USER_TYPE_SUPER_ADMIN => "Super Admin",
            USER_TYPE_BRANCH => "Branch Manager",
            USER_TYPE_ORDINRY => "Member",
        );

        return $arr[$access_level];
    }

}

if (!function_exists('nav_icon')) {

    function nav_icon($module) {
        $icon = '';
        if ($module === 'administration') {
            $icon = 'users';
        } elseif ($module === 'setup') {
            $icon = 'cogs';
        } else{
            $icon = 'envelope';
        }

        return $icon;
    }

}

