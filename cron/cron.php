<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (isset($_SERVER['REQUEST_URI'])) {
    die('Access Denied');
}

define('APPROOT', realpath(__DIR__ . '/..') . DIRECTORY_SEPARATOR);

require_once __DIR__ . '/../vendor/autoload.php';

$environment = getenv('CRM_ENV') ? : 'production';
define('ENVIRONMENT', $environment);


$jobby = new Jobby\Jobby();

//Birthday reminders
$jobby->add('birthDayReminder', array(
    'command' => 'php ' . APPROOT . 'index.php cron_controller/personnelBirthdayNotification',
    'schedule' => '0 6 * * *',
    'output' => 'logs/bithdayReminder.log',
    'enabled' => true,
));

//Birthday reminders
$jobby->add('emailQueue', array(
    'command' => 'php ' . APPROOT . 'index.php cron_controller/push_emails_on_queue',
    'schedule' => '*/10 * * * *',
    'output' => 'logs/emailQueue.log',
    'enabled' => true,
));

//load modules 

//$scripts = glob(__DIR__ . '/_*.php');
//
//foreach ($scripts as $cronScript) {
//    include_once $cronScript;
//}

//run
$jobby->run();
