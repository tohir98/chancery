var profileApp = angular.module('personnel', ['app.Chancery', 'cgBusy', 'datatables', 'datatables.tabletools', 'datatables.scroller', 'datatables.fixedcolumns']);

profileApp.controller('profilesCtrl', function ($scope, $http, $filter, SiteUrl, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.noSelect = false;
    $scope.personnel_type_id = 0;
    $scope.current_assignment = '';
    $scope.status = 0;


    $scope.personnelPromise = $http.get(SiteUrl.get('personnel/get_profile_json'))
            .success(function (data) {
                $scope.personnels = data;
            });

    $scope.toggleAll = function () {
        angular.forEach($scope.personnels, function (emp) {
            emp.is_selected = !emp.is_selected;
        });
    };

    $scope.selectedPersonnels = [];

    $scope.bulkAction = function (employees, modal) {

        $scope.selectedPersonnels = [];
        $scope.noSelect = false;
        if (employees) {
            if (employees instanceof Array) {
                $scope.selectedPersonnels = employees;
            } else {
                $scope.selectedPersonnels.push(employees);
            }
        } else {
            $scope.selectedPersonnels = $filter('filter')($scope.personnels, {is_selected: true});
        }

        if ($scope.selectedPersonnels.length < 1) {
            toastr["error"]("Please select at least one personnel.");
            $scope.noSelect = true;
            return;
        }

        $(modal).modal();
    };
    
    $scope.bulkActionEmail = function (employees) {
        
        $scope.selectedPersonnels = [];
        $scope.noSelect = false;
        if (employees) {
            if (employees instanceof Array) {
                $scope.selectedPersonnels = employees;
            } else {
                $scope.selectedPersonnels.push(employees);
            }
        } else {
            $scope.selectedPersonnels = $filter('filter')($scope.personnels, {is_selected: true});
        }

        if ($scope.selectedPersonnels.length < 1) {
            toastr["error"]("Please select at least one personnel.");
            $scope.noSelect = true;
            return;
        }
        
        var link = '';
        for (j=0; j < $scope.selectedPersonnels.length; ++j) {
            link = link + $scope.selectedPersonnels[j].personnel_id + '|';
        }
        
        console.log(link);
        self.location = SiteUrl.get('personnel/compose_email?r=' + link);
    };

    $scope.filterPersonnel = function () {
        $scope.personnelPromise = $http.get(SiteUrl.get('personnel/get_profile_json/' + $scope.status + '/' + $scope.personnel_type_id + '/' + $scope.current_assignment))
                .success(function (data) {
                    $scope.personnels = data;
                });
    };

    $scope.buildIdList = function (employees, field, delim) {
        field = field || 'personnel_id';
        delim = delim || ',';
        var list = [];
        angular.forEach(employees, function (e) {
            list.push(e[field]);
        });
        return list.join(delim);
    };


    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('bFilter', true)
//            .withTableTools(SiteUrl.get('/js/plugins/datatable/swf/copy_csv_xls_pdf.swf'))
            .withTableToolsButtons([
                'copy',
                'print', {
                    'sExtends': 'collection',
                    'sButtonText': 'Save',
                    'aButtons': ['csv', 'xls', 'pdf']
                }
            ]);
//    .withBootstrap();

    $scope.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(0).notSortable(), DTColumnDefBuilder.newColumnDef(1).notSortable()];
});

profileApp.controller('personnelCtrl', function ($scope, $http, SiteUrl) {

    $scope.personal = {state_id: '', lgaStatus: 'Select state first', lgas: [], congStatus: 'Select Incardination Type first', congregations: [], deanaryStatus: 'Select region first', deanarys: []};
    $scope.personnel_type = 0;
    $scope.honouraries = [];
    $scope.ordinations = [];
    $scope.educations = [];
    $scope.passignments = [];
    $scope.sassignments = [];

    $scope.selected_region = region_id_;
    $scope.selected_deanery = selected_deanery_;
    $scope.selected_state = selected_state_;
    $scope.selected_lga = lga_;
    $scope.selected_incardination = incardination_type_id_;
    $scope.selected_congregation = selected_congregation_id_;


    $scope.loadLgas = function (state_id_) {
        if (!$scope.personal.state_id) {
            $scope.personal.state_id = state_id_;
        }
        if (!$scope.personal.state_id) {
            console.log('state not set');
            return;
        }

        $scope.personal.lgaStatus = 'Loading....';
        $http.get(SiteUrl.get('personnel/get_state_lga/' + $scope.personal.state_id))
                .success(function (data) {
                    $scope.personal.lgaStatus = '';
                    $scope.personal.lgas = data;
                })
                .error(function () {
                    $scope.personal.lgaStatus = 'Unable to load LGA';
                });
    };

    $scope.loadCongregation = function (incardination_type_id_) {
        if (!$scope.personal.incardination_type_id) {
            $scope.personal.incardination_type_id = incardination_type_id_;
        }
        if (!$scope.personal.incardination_type_id) {
            return;
        }

        $scope.personal.congStatus = 'Loading....';
        $http.get(SiteUrl.get('personnel/get_region_cong/' + $scope.personal.incardination_type_id))
                .success(function (data) {
                    $scope.personal.congStatus = '';
                    $scope.personal.congregations = data;
                })
                .error(function () {
                    $scope.personal.congStatus = 'Unable to load congregations';
                });
    };

    $scope.loadDeanary = function (region_id_) {
        if (!$scope.personal.region_id) {
            $scope.personal.region_id = region_id_;
        }
        if (!$scope.personal.region_id) {
            console.log('region not found');
            return;
        }

        $scope.personal.deanaryStatus = 'Loading....';
        $http.get(SiteUrl.get('personnel/get_region_deanary/' + $scope.personal.region_id))
                .success(function (data) {
                    $scope.personal.deanaryStatus = '';
                    $scope.personal.deanarys = data;
                })
                .error(function () {
                    $scope.personal.deanaryStatus = 'Unable to load congregations';
                });
    };

    $scope.blankHonour = {honorary_title_id: '', honorary_place: '', honorary_date: '', honorary_prelate: ''};
    $scope.blank = {};

    $scope.addNewHonour = function () {
        $scope.honouraries.push(angular.copy($scope.blankHonour));
    };

    $scope.removeHonour = function (idx) {
        $scope.honouraries.splice(idx, 1);
        return $scope.checkHonour();
    };

    $scope.checkHonour = function () {
        if ($scope.honouraries.length < 1) {
            $scope.addNewHonour();
        }
    };

    $scope.addNewPrimaryAssignment = function () {
        $scope.passignments.push(angular.copy($scope.blank));
    };

    $scope.removePrimaryAssignment = function (idx) {
        $scope.passignments.splice(idx, 1);
        return $scope.checkPrimaryAssignment();
    };

    $scope.checkPrimaryAssignment = function () {
        if ($scope.passignments.length < 1) {
            $scope.addNewPrimaryAssignment();
        }
    };

    $scope.addNewSecondaryAssignment = function () {
        $scope.sassignments.push(angular.copy($scope.blank));
    };

    $scope.removeSecondaryAssignment = function (idx) {
        $scope.sassignments.splice(idx, 1);
        return $scope.checkSecondaryAssignment();
    };

    $scope.checkSecondaryAssignment = function () {
        if ($scope.sassignments.length < 1) {
            $scope.addNewSecondaryAssignment();
        }
    };

    $scope.addNewOrdination = function () {
        $scope.ordinations.push(angular.copy($scope.blank));
    };

    $scope.addNewEducation = function () {
        $scope.educations.push(angular.copy($scope.blank));
    };

    $scope.removeOrdination = function (idx) {
        $scope.ordinations.splice(idx, 1);
        return $scope.checkOrdination();
    };

    $scope.checkOrdination = function () {
        if ($scope.ordinations.length < 1) {
            $scope.addNewOrdination();
        }
    };

    $scope.removeEducation = function (idx) {
        $scope.educations.splice(idx, 1);
        return $scope.checkEducation();
    };

    $scope.checkEducation = function () {
        if ($scope.educations.length < 1) {
            $scope.addNewEducation();
        }
    };


    if (parseInt($scope.selected_region) > 0) {
        $scope.loadDeanary($scope.selected_region);
    }

    if (parseInt($scope.selected_state) > 0) {
        $scope.loadLgas($scope.selected_state);
    }

    if (parseInt($scope.selected_incardination) > 0) {
        $scope.loadCongregation($scope.selected_incardination);
    }

    $scope.checkHonour();
    $scope.checkOrdination();
    $scope.checkEducation();
    $scope.checkPrimaryAssignment();
    $scope.checkSecondaryAssignment();


});

$('body').delegate('.change_status', 'click', function (e) {
    e.preventDefault();
    $("#personnel_id").val($(this).data('personnel_id'));
    $("#modal-edit-status").modal();

});

$('body').delegate('.deletePersonnel', 'click', function (e) {
    e.preventDefault();
    var h = this.href;
    var message = 'Are you sure you want to delete this personnel?';
    OaaStudy.doConfirm({
        title: 'Confirm Delete',
        cancelText: 'No',
        acceptText: 'Yes',
        message: message,
        onAccept: function () {
            window.location = h;
        }
    });
});
