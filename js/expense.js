
var expenseApp = angular.module('expense', []);

expenseApp.controller('expenseCtrl', function ($scope, $http) {
    $scope.ap_account_id = '';
    $scope.vendor_id = '';
    $scope.cash_account_id = '';
    $scope.ap_info = $scope.bank = [];
    $scope.amount = 0;

    $scope.getAPBalance = function () {
        if (!$scope.ap_account_id) {
            return;
        }
        var url = 'getAPBalance/' + $scope.ap_account_id + '/' + $scope.vendor_id;
        $http.get(url)
                .success(function (data) {
                    $scope.ap_info = data;
                    $scope.validate();
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };

    $scope.getCashBalance = function () {
        if (!$scope.account_chart_id) {
            return;
        }
        var url = 'getCashBalance/' + $scope.account_chart_id + '/' + $scope.income_source_id;
        $http.get(url)
                .success(function (data) {
                    $scope.ar_info = data;
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };
    
    $scope.getBankBalance = function () {
        if (!$scope.cash_account_id) {
            return;
        }
        var url = 'getBankBalance/' + $scope.cash_account_id;
        $http.get(url) 
                .success(function (data) {
                    $scope.bank = data;
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };

    $scope.form_submit = function () {
        $("#frm").submit();
    };
    
    $scope.validate = function(){
        if (!$scope.ap_info.balance){
            return true;
        }
        return parseFloat($scope.amount) > parseFloat($scope.ap_info.balance);
    };



});


