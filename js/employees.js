var employeeApp = angular.module('employees', []);

employeeApp.controller('statusCtrl', function ($scope, $http) {
    
});


 $(function () {
        $('.groupToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this sub group, you will not be able to assign it, Are you sure you want to continue? ' : 'Are you sure you want to enable selected sub group ?';
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });

        $('body').delegate('.deleteEmployee', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this employee?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                cancelText: 'No',
                acceptText: 'Yes',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

//    $("#status_id").change(function () {
//        var status_id_ = $(this).val();
//        console.log(status_id_);
//
//        if (parseInt(status_id_) === 3) {
//            $("#dv_termination").show();
//        } else {
//            $("#dv_termination").hide();
//        }
//    });

    $('body').delegate('.edit_account', 'click', function (evt) {
        evt.preventDefault();
        var emp_id_ = $(this).data('employee_id');
        $("#employee_id").val(emp_id_);

        $('#edit_status_modal').modal('show');
        return false;
    });