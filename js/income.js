
var incomeApp = angular.module('income', []);

incomeApp.controller('incomeCtrl', function ($scope, $http) {
    $scope.account_chart_id = '';
    $scope.income_source_id = '';
    $scope.cash_account_id = '';
    $scope.amount = 0;
    $scope.ar_info = {balance: 0};
    $scope.bank = [];

    $scope.getARBalance = function () {
        if (!$scope.account_chart_id) {
            return;
        }

        var url = 'getARBalance/' + $scope.account_chart_id + '/' + $scope.income_source_id;
        $http.get(url)
                .success(function (data) {
                    $scope.ar_info = data;
                    $scope.validate();
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };

    $scope.getCashBalance = function () {
        if (!$scope.account_chart_id) {
            return;
        }
        var url = 'getCashBalance/' + $scope.account_chart_id + '/' + $scope.income_source_id;
        $http.get(url)
                .success(function (data) {
                    $scope.ar_info = data;
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };

    $scope.getBankBalance = function () {
        if (!$scope.cash_account_id) {
            return;
        }
        var url = 'getBankBalance/' + $scope.cash_account_id;
        $http.get(url)
                .success(function (data) {
                    $scope.bank = data;
                })
                .error(function () {
                    $("#err_modal").modal();
                });
    };

    $scope.form_submit = function () {
        $("#frm").submit();
    };

    $scope.validate = function () {
        if (!$scope.ar_info.balance) {
            return true;
        }
        return parseFloat($scope.amount) > parseFloat($scope.ar_info.balance);
    };

    $scope.formatNum = function () {
//
//        if ($scope.amount.length == 4) {
//            var my_val = $scope.amount;
//            $scope.amount = Number(my_val).toLocaleString('en');
//        }

//        if (evt.which != 110) {//not a fullstop
//            var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
//            $(this).val(n.toLocaleString());
//        }

//            if ($(this).val().length == 4) {
//                var my_val = $(this).val();
//                $(this).val(Number(my_val).toLocaleString('en'));
//            }
    };



});

//$("#amount").on('keyup', function (evt) {
//    if (evt.which != 110) {//not a fullstop
//        var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
//        $(this).val(n.toLocaleString());
//    }

//$('#amount').keyup(function (event) {
//
//    // skip for arrow keys
//    if (event.which >= 37 && event.which <= 40) {
//        event.preventDefault();
//    }
//
//    $(this).val(function (index, value) {
//        return value
//                .replace(/\D/g, "")
//                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
//                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
//                ;
//    });
//});

