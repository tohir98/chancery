$(function () {
    $('.confirm_cancel').click(function (e) {
        e.preventDefault();
        var h = this.href;
        var message = 'Are you sure you want to cancel this pay run';
        OaaStudy.doConfirm({
            title: 'Confirm',
            message: message,
            onAccept: function () {
                window.location = h;
            }
        });
    });
});

function toggle(source) {
    checkboxes = document.getElementsByName('employee_id[]');
    for (var i in checkboxes)
        checkboxes[i].checked = source.checked;
}