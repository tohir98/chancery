var OaaStudy = OaaStudy || {};

OaaStudy.doConfirm = function (params) {
    params = params || {};
    var defaults = {
        title: 'Confirm',
        message: 'Are you sure?',
        cancelText: 'Cancel',
        acceptText: 'OK',
        onAccept: null,
        onCancel: null,
        closeOnConfirm: true,
        fade: true
    };
    var mParams = jQuery.extend({}, defaults, params);

    var html = "<div class=\"modal\" id='edConfirmModal'><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"edCancelBtn close\" data-dismiss='modal' aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\"></h4></div><div class=\"modal-body\"></div><div class=\"modal-footer\"><a class=\"btn btn-danger  btn-flat btn-sm edCancelBtn\" data-dismiss='modal'></a><a class=\"btn btn-primary btn-flat btn-sm edAcceptBtn\" ></a></div></div></div></div>";


    $('#edConfirmModal').remove();
    $('body').append(html);

    var $confirm = $('#edConfirmModal');
    //title
    $confirm.find('.modal-header h4').html(mParams.title);
    //body
    $confirm.find('.modal-body').html(mParams.message);
    //cancel button
    $confirm.find('.btn.edCancelBtn').text(mParams.cancelText);
    //accept button
    $confirm.find('.btn.edAcceptBtn').text(mParams.acceptText);

    if (typeof mParams.onAccept === 'function') {
        $confirm.find('.edAcceptBtn')
                .click(mParams.onAccept);
    }

    if (typeof mParams.onCancel === 'function') {
        $confirm.find('.edCancelBtn').click(mParams.onCancel);
    }

    if (mParams.closeOnConfirm) {
        $confirm.find('.edAcceptBtn').attr('data-dismiss', 'modal');
    }

    if (mParams.fade) {
        $confirm.addClass('fade');
    }

    $confirm.modal();
};

$(function () {
    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker({
            format: 'dd/mm/yyyy'
        });
        $(this).css('cursor', 'pointer')
    });
    $('.select2').select2();
});

if (angular) {
    var CHANCERY_APP = angular.module('app.Chancery', [])
            .config(function ($httpProvider) {
                // Use x-www-form-urlencoded Content-Type
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
                // Override $http service's default transformRequest
                $httpProvider.defaults.transformRequest = [function (data) {
                        return angular.isObject(data) && String(data) !== '[object File]' ? jQuery.param(data).replace(/\+/g, '%20') : data;
                    }];
            }).factory('SiteUrl', function () {

        return {
            get: function (path) {
                if (!window.SITE_URL) {
                    window.SITE_URL = '//' + window.location.host;
                    if (window.location.port !== '') {
                        window.SITE_URL += ":" + window.location.port;
                    }

                    window.SITE_URL += '/';
                }

                if (path.indexOf('/') === 0) {
                    path = path.substr(1);
                }

                return window.SITE_URL + path;
            }
        };
    }).filter('arrayFy', function () {
        return function (object, keyName) {
            var arr = [];
            keyName = keyName || '_key';

            angular.forEach(object, function (obj, key) {
                if (!angular.isObject(obj)) {
                    obj = {_val: obj};
                }
                obj[keyName] = key;
                arr.push(obj);
            });
            return arr;
        };
    }).directive('tbConfirm', function ($parse) {
        return {
            restrict: 'AC',
            link: function (scope, elem, attrs) {
                elem.click(function (e) {
                    e.preventDefault();
                    var options = {};
                    angular.forEach(attrs, function (val, name) {
                        if (name.match(/^confirm/)) {
                            name = name.replace(/^confirm/, '');
                            name = name[0].toLowerCase() + name.substr(1);
                            options[name] = val;
                        }
                    });

                    var callables = ['onAccept', 'onCancel'];
                    angular.forEach(callables, function (name) {
                        if (options.hasOwnProperty(name)) {
                            var k = $parse(options[name]);
                            options[name] = function () {
                                k(scope);
                                try {
                                    scope.$apply();
                                } catch (e) {

                                }
                            };
                        }
                    });


                    console.log(options);
                    CTM.doConfirm(options);
                });
            }
        };
    }).directive('chosenfy', function ($timeout) {

        return {
            restrict: 'A',
            scope: {
                resultProcess: '=process'
            },
            link: function (scope, element, attrs) {
                $timeout(function () {
                    var search = (element.attr("data-nosearch") === "true") ? true : false,
                            opt = {};
                    if (search) {
                        opt.disable_search_threshold = 9999999;
                    }
                    
                    if (attrs.chosenWidth){
                        opt.width = attrs.chosenWidth;
                    }


                    if (attrs.remoteUrl && attrs.remoteUrl !== '') {
                        element.ajaxChosen({
                            type: attrs.remoteUrlMethod || 'GET',
                            url: attrs.remoteUrl,
                            dataType: 'json',
                            minTermLength: 2
                        }, function (data) {
                            //console.log('Result Process: ', scope.resultProcess);
                            if (scope.resultProcess && (typeof scope.resultProcess === 'function')) {
                                return scope.resultProcess(data);
                            }

                            var results = [];
                            $.each(data, function (i, val) {
                                results.push({value: val.value, text: val.text});
                            });
                            return results;
                        }, opt);
                    } else {
                        element.chosen(opt);
                    }


                    setTimeout(function () {
                        if ($('.chzn-container').length > 0) {
                            element.attr('style', 'display:visible; position:absolute; clip:rect(0,0,0,0)');
                        }
                    }, 100);

                }, 100);
            }
        };
    }).directive('datepick', function ($timeout) {

        return {
            link: function (scope, element) {
                $timeout(function () {
                    element.datepicker();
                }, 50);
            }
        };
    }).directive('eatclick', function () {
        return {
            link: function (scope, element) {
                element.click(function (e) {
                    e.preventDefault();
                });
            }
        };
    })
    
    .directive('popover', function ($timeout) {
        return {
            link: function (scope, element) {
//                element.click(function (e) {
//                    e.preventDefault();
//                });
                
                $timeout(function(){
                    element.popover();
                }, 50);
            }
        };
    })
    
    ;


}


