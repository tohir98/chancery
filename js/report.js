var reportApp = angular.module('report', ['app.Chancery']);

reportApp.run(function ($rootScope, $http, SiteUrl) {
    $rootScope.start_date = window.start_date;
    $rootScope.end_date = window.end_date;

});

reportApp.controller('incomeCtrl', function ($scope, $http, SiteUrl) {
    $scope.income_source =0;
    $scope.income_account =0;
    $scope.income_type =0;
    $scope.cash_account =0;
    $scope.heading_type =0;
    
    $scope.filterResult = function(){
        $scope.incomePromise = $http.get(SiteUrl.get('report/income_report_json?source='+ $scope.income_source+'&account_chart='+ $scope.income_account+'&income_type='+ $scope.income_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date ))
            .success(function (data) {
                $scope.incomes = data;
            });
    };
    
    $scope.downloadExcel = function(){
        self.location = SiteUrl.get('report/income_report_excel?source='+ $scope.income_source+'&account_chart='+ $scope.income_account+'&income_type='+ $scope.income_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date );
    };
    
    $scope.filterResult();

});

reportApp.controller('expenseCtrl', function ($scope, $http, SiteUrl) {
    $scope.payee =0;
    $scope.expense_account =0;
    $scope.expense_type =0;
    $scope.cash_account =0;
    $scope.heading_type =0;
    
    $scope.filterResult = function(){
        $scope.expensePromise = $http.get(SiteUrl.get('report/expense_report_json?payee='+ $scope.payee+'&account_chart='+ $scope.expense_account+'&expense_type='+ $scope.expense_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date ))
            .success(function (data) {
                $scope.expenses = data;
            });
    };
    
    $scope.downloadExcel = function(){
        self.location = SiteUrl.get('report/expense_report_excel?payee='+ $scope.payee+'&account_chart='+ $scope.expense_account+'&expense_type='+ $scope.expense_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date );
    };
    
    $scope.filterResult();

});

reportApp.controller('receivableCtrl', function ($scope, $http, SiteUrl) {
    $scope.is_paid =0;
    $scope.ar_account =0;
    
    $scope.filterResult = function(){
        $scope.expensePromise = $http.get(SiteUrl.get('report/expense_report_json?payee='+ $scope.payee+'&account_chart='+ $scope.expense_account+'&expense_type='+ $scope.expense_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date ))
            .success(function (data) {
                $scope.expenses = data;
            });
    };
    
    $scope.downloadExcel = function(){
        self.location = SiteUrl.get('report/expense_report_excel?payee='+ $scope.payee+'&account_chart='+ $scope.expense_account+'&expense_type='+ $scope.expense_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date );
    };

});

reportApp.controller('payableCtrl', function ($scope, $http, SiteUrl) {
    $scope.ap_account =0;
    
    $scope.filterResult = function(){
        $scope.expensePromise = $http.get(SiteUrl.get('report/payable_report_json?account_chart='+ $scope.ap_account+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date ))
            .success(function (data) {
                $scope.payables = data;
            });
    };
    
    $scope.downloadExcel = function(){
        self.location = SiteUrl.get('report/expense_report_excel?payee='+ $scope.payee+'&account_chart='+ $scope.expense_account+'&expense_type='+ $scope.expense_type+'&cash_account='+ $scope.cash_account+'&heading_type='+ $scope.heading_type+'&start_date='+ $scope.start_date+'&end_date='+ $scope.end_date );
    };

});