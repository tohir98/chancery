var employeeApp = angular.module('compensation', []);

employeeApp.controller('grosspayCtrl', function ($scope, $http) {
    $scope.comp = {allowances:[], components:[], grosspay:0 };
    $scope.blankComponent = {id: null, percentage: 0, title: '', amount: 0};
    
    $scope.removeAllowance = function(idx){
        return $scope.comp.allowances.splice(idx, 1);
    };
    
    $scope.removeComponent = function(idx){
        return $scope.comp.components.splice(idx, 1);
    };
    
    $scope.getTotalPercent = function(){
        var sum_ = 0;
        if ($scope.comp.allowances.length > 0){
            for(j=0; j < $scope.comp.allowances.length; ++j){
                sum_ += parseFloat($scope.comp.allowances[j].percentage);
            }
        }
        if ($scope.comp.components.length > 0){
            for(k=0; k < $scope.comp.components.length; ++k){
                sum_ += parseFloat($scope.comp.components[k].percentage);
            }
        }
        return sum_;
    };
    
    $scope.getTotalAmount = function(){
        var amount_ = 0;
        if ($scope.comp.allowances.length > 0){
            for(j=0; j < $scope.comp.allowances.length; ++j){
                amount_ += parseFloat($scope.comp.allowances[j].percentage/100 * $scope.comp.grosspay);
            }
        }
        if ($scope.comp.components.length > 0){
            for(k=0; k < $scope.comp.components.length; ++k){
                amount_ += parseFloat($scope.comp.components[k].percentage/100 * $scope.comp.grosspay);
            }
        }
        return amount_;
    };
    
    $scope.addNewComponent = function () {
        $scope.comp.components.push(angular.copy($scope.blankComponent));
    };
    
});

employeeApp.controller('deductionCtrl', function ($scope, $http) {
    $scope.comp = {deductions:[], components:[] };
    $scope.blankComponent = {id: null, title: '', amount: 0};
    
    $scope.removeDeduction = function(idx){
        return $scope.comp.deductions.splice(idx, 1);
    };
    
    $scope.removeComponent = function(idx){
        return $scope.comp.components.splice(idx, 1);
    };
    
    $scope.getTotalDeduction = function(){
        var amount_ = 0;
        if ($scope.comp.deductions.length > 0){
            for(j=0; j < $scope.comp.deductions.length; ++j){
                amount_ += parseFloat($scope.comp.deductions[j].amount);
            }
        }
        if ($scope.comp.components.length > 0){
            for(k=0; k < $scope.comp.components.length; ++k){
                amount_ += parseFloat($scope.comp.components[k].amount);
            }
        }
        return amount_;
    };
    
    $scope.addNewComponent = function () {
        $scope.comp.components.push(angular.copy($scope.blankComponent));
    };
    
});
