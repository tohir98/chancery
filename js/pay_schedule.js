var scheduleApp = angular.module('schedule', []);

scheduleApp.controller('scheduleCtrl', function ($scope, $http) {
    $scope.periods = {1: 'Monthly'};
    $scope.period_id = 1;

});

$(function () {
    $('.delete_schedule').click(function (e) {
        e.preventDefault();
        var h = this.href;
        var message = 'Are you sure you want to delete this pay schedule';
        OaaStudy.doConfirm({
            title: 'Confirm Delete',
            message: message,
            onAccept: function () {
                window.location = h;
            }
        });
    });
});
