ALTER TABLE `personnel_primary_assignment` DROP `period_year`; ALTER
TABLE `personnel_secondary_assignments` DROP `period_year`;

ALTER TABLE `personnel_secondary_assignments` ADD `year_from` DATE NULL
DEFAULT NULL AFTER `position_id`, ADD `year_to` DATE NULL DEFAULT NULL
AFTER `year_from`;

ALTER TABLE `personnel_primary_assignment` ADD `year_from` DATE NULL
DEFAULT NULL AFTER `position_id`, ADD `year_to` DATE NULL DEFAULT NULL
AFTER `year_from`;

