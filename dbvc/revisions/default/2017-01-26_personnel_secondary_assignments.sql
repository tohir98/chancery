-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2017 at 12:02 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chancery`
--

-- --------------------------------------------------------

--
-- Table structure for table `personnel_secondary_assignments`
--

CREATE TABLE `personnel_secondary_assignments` (
  `personnel_secondary_assignment_id` int(10) UNSIGNED NOT NULL,
  `name_of_secondary_assignment` varchar(150) DEFAULT NULL,
  `position_id` int(10) DEFAULT NULL,
  `period_year` varchar(150) DEFAULT NULL,
  `personnel_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personnel_secondary_assignments`
--
ALTER TABLE `personnel_secondary_assignments`
  ADD PRIMARY KEY (`personnel_secondary_assignment_id`),
  ADD KEY `FK_personnel_secondary_assignments_position_id` (`position_id`),
  ADD KEY `FK_personnel_secondary_assignments_personnel_id` (`personnel_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personnel_secondary_assignments`
--
ALTER TABLE `personnel_secondary_assignments`
  MODIFY `personnel_secondary_assignment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `personnel_secondary_assignments`
--
ALTER TABLE `personnel_secondary_assignments`
  ADD CONSTRAINT `FK_personnel_secondary_assignments_personnel_id` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_personnel_secondary_assignments_position_id` FOREIGN KEY (`position_id`) REFERENCES `positions` (`position_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
