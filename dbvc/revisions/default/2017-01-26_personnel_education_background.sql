-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 11:59 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chancery`
--

-- --------------------------------------------------------

--
-- Table structure for table `personnel_education_background`
--

CREATE TABLE `personnel_education_background` (
  `personnel_education_background_id` int(10) UNSIGNED NOT NULL,
  `education_type_id` int(10) DEFAULT NULL,
  `institution_name` varchar(150) DEFAULT NULL,
  `year_from` varchar(150) DEFAULT NULL,
  `year_to` varchar(150) NOT NULL,
  `degree` varchar(150) DEFAULT NULL,
  `personnel_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personnel_education_background`
--
ALTER TABLE `personnel_education_background`
  ADD PRIMARY KEY (`personnel_education_background_id`),
  ADD KEY `FK_personnel_education_background_education_id` (`education_type_id`),
  ADD KEY `FK_personnel_education_background_personnel_id` (`personnel_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personnel_education_background`
--
ALTER TABLE `personnel_education_background`
  MODIFY `personnel_education_background_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `personnel_education_background`
--
ALTER TABLE `personnel_education_background`
  ADD CONSTRAINT `FK_personnel_education_background_education_id` FOREIGN KEY (`education_type_id`) REFERENCES `education_types` (`education_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_personnel_education_background_personnel_id` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
