-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2017 at 12:01 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chancery`
--

-- --------------------------------------------------------

--
-- Table structure for table `personnel_honorary_title`
--

CREATE TABLE `personnel_honorary_title` (
  `personnel_honorary_title_id` int(10) UNSIGNED NOT NULL,
  `honorary_title_id` int(11) DEFAULT NULL,
  `place` varchar(150) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `prelate` varchar(150) DEFAULT NULL,
  `personnel_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personnel_honorary_title`
--
ALTER TABLE `personnel_honorary_title`
  ADD PRIMARY KEY (`personnel_honorary_title_id`),
  ADD KEY `FK_personnel_honorary_title_honorary_title_id` (`honorary_title_id`),
  ADD KEY `FK_personnel_honorary_title_personnel_id` (`personnel_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personnel_honorary_title`
--
ALTER TABLE `personnel_honorary_title`
  MODIFY `personnel_honorary_title_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `personnel_honorary_title`
--
ALTER TABLE `personnel_honorary_title`
  ADD CONSTRAINT `FK_personnel_honorary_title_honorary_title_id` FOREIGN KEY (`honorary_title_id`) REFERENCES `honourary_titles` (`honorary_title_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_personnel_honorary_title_personnel_id` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
