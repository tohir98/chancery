CREATE TABLE IF NOT EXISTS `bank_accounts` (
  `bank_account_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `bank_id` INT UNSIGNED,
  `account_number` VARCHAR(45),
  `account_name` VARCHAR(150),
  `date_added` DATETIME,
  `date_updated` DATETIME,
  PRIMARY KEY (`bank_account_id`)
)
ENGINE = InnoDB;